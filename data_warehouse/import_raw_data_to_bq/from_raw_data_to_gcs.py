import datetime
from db_config import *
from google.cloud import storage
from bson import json_util

from outliers_removal.outliers_removal import OutliersProcessing

DATE = datetime.datetime.now().strftime("%d-%m-%Y")


class FromRawDataToGcs:

  @staticmethod
  def export_integration_json():
    outlier = OutliersProcessing()
    outlier.processing()
    results = outlier.select_not_with_outlier()

    with open(RESULT_JSON_FILE, 'w', encoding='utf-8') as outfile:
      for result in results:
        outfile.write(json_util.dumps(result, default=FromRawDataToGcs._default, ensure_ascii=False)+'\n')

    return len(results)

  @staticmethod
  def from_json_to_gcs():
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(BUCKET_NAME)

    blob = bucket.blob(DATE + "/" + RESULT_JSON_FILE)
    blob.upload_from_filename(RESULT_JSON_FILE)

    return f'Uploaded {RESULT_JSON_FILE} to "{BUCKET_NAME}" bucket.'

  @staticmethod
  def _default(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
      return o.isoformat()

  @staticmethod
  def from_raw_data_to_gcs():
    length_result = FromRawDataToGcs.export_integration_json()
    if length_result:
      FromRawDataToGcs.from_json_to_gcs()


