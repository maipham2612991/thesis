from import_raw_data_to_bq.from_raw_data_to_gcs import FromRawDataToGcs

if __name__ == '__main__':
  # raw data to bq
  length_results = FromRawDataToGcs.export_integration_json()
  if length_results:
    FromRawDataToGcs.from_json_to_gcs()
