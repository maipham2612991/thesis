-- preprocess null address and extract analytics field
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.analytics_data` AS
 SELECT
   GENERATE_UUID() as id,
   SPLIT(address, ',')[OFFSET(0)] as addr_province_city,
   SPLIT(address, ',')[OFFSET(1)] as addr_city_district,
   price,
   price_unit,
   area,
   property_type,
   transaction_type,
   project,
   year,
   month,
   week,
   day
 FROM
 (
  SELECT
   CONCAT(
     CASE IFNULL(addr_province, "")
       WHEN "" then ""
       ELSE addr_province || ","
     END,
     addr_city, ",",
     CASE IFNULL(addr_district, "")
       WHEN "" then ""
       ELSE addr_district
     END
   ) as address,
   price,
   price_unit,
   area,
   property_type,
   transaction_type,
   CASE project
   WHEN "" THEN null
   ELSE project
   END AS project,
   CAST(EXTRACT(YEAR FROM created_date) AS STRING) AS year,
   LPAD(CAST(EXTRACT(MONTH FROM created_date) AS STRING), 2, '0') AS month,
   LPAD(CAST(EXTRACT(WEEK FROM created_date) AS STRING), 2, '0') AS week,
   LPAD(CAST(EXTRACT(DAY FROM created_date) AS STRING), 2, '0') AS day
  FROM `real-estate-thesis-2021.real_estate.raw_real_estate_integrated`
)
WHERE
    address is NOT NULL AND
    (price_unit = "triệu/m²" OR price_unit = "triệu/m²/tháng");

-- ADDRESS including two field addr: province or city, district
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.ADDR_DIMENSION` AS
SELECT
  GENERATE_UUID() as id,
  *
FROM (
  SELECT
    DISTINCT
      addr_province_city,
      addr_city_district
  FROM `real-estate-thesis-2021.real_estate.analytics_data`
);

-- TIME including year, month, week, day
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.TIME_DIMENSION` AS
SELECT
  GENERATE_UUID() as id,
  *
FROM (
  SELECT
    DISTINCT
      year,
      month,
      week,
      day
  FROM `real-estate-thesis-2021.real_estate.analytics_data`
);

-- TRANSACTION including field transaction
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.TRANSACTION_DIMENSION` AS
SELECT
  GENERATE_UUID() as id,
  *
FROM (
  SELECT
    DISTINCT
      transaction_type
  FROM `real-estate-thesis-2021.real_estate.analytics_data`
);

-- PROPERTY including property_type
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.PROPERTY_DIMENSION` AS
SELECT
  GENERATE_UUID() as id,
  *
FROM (
  SELECT
    DISTINCT
      property_type
  FROM `real-estate-thesis-2021.real_estate.analytics_data`
  WHERE
    property_type != "" AND
    property_type is NOT NULL
);

-- PROJECT_DIMENSION
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.PROJECT_DIMENSION` AS
SELECT
  GENERATE_UUID() as id,
  *
FROM (
  SELECT
    DISTINCT
      project as project_name
  FROM `real-estate-thesis-2021.real_estate.analytics_data`
  WHERE
    project != "" AND
    project is NOT NULL
);

-- TRANS_PROP_ADDR_FACT
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.TRANS_PROP_ADDR_FACT` AS
SELECT
  time.id as time_id,
  addr.id as addr_id,
  trans.transaction_type as transaction_type,
  prop.property_type as property_type,
  count(all_data.id) as count,
  SUM(all_data.area) as total_area,
  AVG(all_data.price) average_price,
  all_data.price_unit as price_unit
FROM
  `real-estate-thesis-2021.real_estate.analytics_data` as all_data,
  `real-estate-thesis-2021.real_estate.TIME_DIMENSION` as time,
  `real-estate-thesis-2021.real_estate.TRANSACTION_DIMENSION` as trans,
  `real-estate-thesis-2021.real_estate.PROPERTY_DIMENSION` as prop,
  `real-estate-thesis-2021.real_estate.ADDR_DIMENSION` as addr
WHERE
  all_data.year = time.year AND
  all_data.month = time.month AND
  all_data.week = time.week AND
  all_data.day = time.day AND
  all_data.transaction_type = trans.transaction_type AND
  all_data.property_type = prop.property_type AND
  all_data.addr_province_city = addr.addr_province_city AND
  all_data.addr_city_district = addr.addr_city_district AND
  price_unit != "" AND
  price_unit is not NULL
GROUP BY
  time_id,
  addr_id,
  transaction_type,
  property_type,
  price_unit;

-- AVERAGE_PRICE_PER_MONTH : line chart
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.AVERAGE_PRICE_PER_MONTH` AS
SELECT
  transaction_type,
  property_type,
  ARRAY_AGG(time) as time_to_month,
  ARRAY_AGG(average_price) as average_price,
  AVG(average_price) as average_total_price,
  price_unit
FROM (
  SELECT
    transaction_type,
    property_type,
    CONCAT(time.year, "/", time.month) as time,
    AVG(average_price) as average_price,
    price_unit
  FROM
    `real-estate-thesis-2021.real_estate.TRANS_PROP_ADDR_FACT` as fact,
    `real-estate-thesis-2021.real_estate.TIME_DIMENSION` as time
  WHERE
    fact.time_id = time.id
  GROUP BY
    transaction_type,
    property_type,
    time,
    price_unit
  ORDER BY time
)
GROUP BY
  price_unit,
  transaction_type,
  property_type
ORDER BY
  ARRAY_LENGTH(time_to_month) DESC;

-- HANOI and hcm average_price and total_area
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.AVERAGE_PRICE_TOTAL_AREA_METROPOLIS` AS
SELECT
  addr.addr_province_city as province_city,
  property_type,
  price_unit,
  transaction_type,
  AVG(fact.average_price) as average_price,
  SUM(fact.total_area) as total_area,
  SUM(fact.count) as province_city_count
FROM
  `real-estate-thesis-2021.real_estate.TRANS_PROP_ADDR_FACT` as fact,
  `real-estate-thesis-2021.real_estate.ADDR_DIMENSION` as addr
WHERE
  fact.addr_id = addr.id AND
  (addr.addr_province_city = "hà nội" OR
  addr.addr_province_city = "hồ chí minh" )
GROUP BY
  province_city,
  property_type,
  price_unit,
  transaction_type;

-- TRANS_PROJECT_ADDR_FACT
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.TRANS_PROJECT_ADDR_FACT` AS
SELECT
  time.id as time_id,
  addr.id as addr_id,
  project.project_name as project_name,
  trans.transaction_type as transaction_type,
  count(all_data.id) as count,
  AVG(all_data.price) average_price,
  all_data.price_unit as price_unit
FROM
  `real-estate-thesis-2021.real_estate.analytics_data` as all_data,
  `real-estate-thesis-2021.real_estate.TIME_DIMENSION` as time,
  `real-estate-thesis-2021.real_estate.TRANSACTION_DIMENSION` as trans,
  `real-estate-thesis-2021.real_estate.ADDR_DIMENSION` as addr,
  `real-estate-thesis-2021.real_estate.PROJECT_DIMENSION` as project
WHERE
  all_data.year = time.year AND
  all_data.month = time.month AND
  all_data.week = time.week AND
  all_data.day = time.day AND
  all_data.transaction_type = trans.transaction_type AND
  all_data.addr_province_city = addr.addr_province_city AND
  all_data.addr_city_district = addr.addr_city_district AND
  all_data.project = project.project_name AND
  price_unit != "" AND
  price_unit is not NULL
GROUP BY
  time_id,
  addr_id,
  transaction_type,
  project_name,
  price_unit;

-- PROVINCE_AVERAGE_PRICE_PER_MONTH
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.PROVINCE_AVERAGE_PRICE_PER_MONTH` AS
SELECT
  addr_province_city,
  transaction_type,
  property_type,
  ARRAY_AGG(time) as time_to_month,
  ARRAY_AGG(average_price) as average_price,
  AVG(average_price) as average_total_price,
  price_unit
FROM (
  SELECT
    addr_province_city,
    transaction_type,
    property_type,
    CONCAT(time.year, "/", time.month) as time,
    AVG(average_price) as average_price,
    price_unit
  FROM
    `real-estate-thesis-2021.real_estate.TRANS_PROP_ADDR_FACT` as fact,
    `real-estate-thesis-2021.real_estate.TIME_DIMENSION` as time,
    `real-estate-thesis-2021.real_estate.ADDR_DIMENSION` as addr
  WHERE
    fact.time_id = time.id AND
    fact.addr_id = addr.id
  GROUP BY
    addr_province_city,
    transaction_type,
    property_type,
    time,
    price_unit
  ORDER BY time
)
GROUP BY
  addr_province_city,
  price_unit,
  transaction_type,
  property_type
ORDER BY
  ARRAY_LENGTH(time_to_month) DESC;

-- ADDR_TRANS_PROP_PRICE
CREATE OR REPLACE TABLE `real-estate-thesis-2021.real_estate.ADDR_TRANS_PROP_PRICE` AS
SELECT
  fact.time_id,
  fact.transaction_type,
  fact.property_type,
  fact.count,
  fact.total_area,
  fact.average_price,
  fact.price_unit,
  addr.addr_province_city as province_city,
  addr.addr_city_district as city_district
FROM
  `real-estate-thesis-2021.real_estate.TRANS_PROP_ADDR_FACT` as fact,
  `real-estate-thesis-2021.real_estate.ADDR_DIMENSION` as addr
WHERE
  fact.addr_id = addr.id;