from abc import ABC

from sqlalchemy import create_engine

from db_config import *
from .utils import *
import pandas as pd

COLUMNS = (
  "price",
  "price_unit",
  "area",
  "transaction_type",
  "property_type",
  "addr_province",
  "addr_city",
  "addr_district",
  "addr_ward",
  "addr_street",
  "num_bedrooms",
  "num_bathrooms",
  "project",
  "project_size",
  "created_date",
  "expired_date",
  "num_floors",
  "floorth",
  "direction",
  "legal",
  "front",
  "alley",
  "url",
  "post_author",
  "phone_number",
  "surrounding",
  "surrounding_characteristics",
  "position",
  "internal_facility"
)


class OutliersProcessing(ABC):
    def __init__(self):
        self.engine = create_engine('postgresql://'
                                            + user 
                                            + ':' + password
                                            + '@' + host
                                            + ':' + port
                                            + '/' + database)
        
        self.sqlengine = self.engine
        self.connection = self.sqlengine.raw_connection()

    def processing(self):
        self.sell_processing()
        self.rent_processing()
        print('DONE mark outliers')

    def sell_processing(self):
      sql = f"""
            SELECT 
              price, url 
            FROM REAL_ESTATE_INTEGRATED
            WHERE 
              price is NOT NULL AND
              price_unit is NOT NULL AND
              transaction_type = 'bán'
        """
      df = pd.read_sql_query(sql, con = self.sqlengine)

      urls_list_rm = return_outliers(df)

      self.mark_outlier(urls_list_rm)
      print('DONE mark outliers of selling data')

    def rent_processing(self):
      sql = f"""
            SELECT 
              price, url 
            FROM REAL_ESTATE_INTEGRATED
            WHERE 
              price is NOT NULL AND
              price_unit is NOT NULL AND
              transaction_type = 'cho thuê'
        """
      df = pd.read_sql_query(sql, con = self.sqlengine)

      urls_list_rm = return_outliers(df)

      self.mark_outlier(urls_list_rm)
      print('DONE mark outliers of renting data')

    def mark_outlier(self, outlier_list):
      outliers = ", ".join([repr(i) for i in outlier_list])
      cursor = self.connection.cursor()

      sql = f"""
          UPDATE REAL_ESTATE_INTEGRATED 
          SET is_outlier = TRUE 
          WHERE url in ({outliers})
      """
      cursor.execute(sql)
      self.connection.commit()
    
    def select_not_with_outlier(self):
        cursor = self.connection.cursor()

        sql = f"""
            SELECT
              {", ".join(COLUMNS)}
            FROM REAL_ESTATE_INTEGRATED
            WHERE
              price is NOT NULL AND
              price_unit is NOT NULL AND
              is_outlier = FALSE AND
              is_loaded = FALSE LIMIT 20000;
        """

        # sql = f"""
        #     SELECT
        #       {", ".join(COLUMNS)}
        #     FROM REAL_ESTATE_INTEGRATED
        #     WHERE
        #       price is NOT NULL AND
        #       price_unit is NOT NULL AND
        #       is_outlier = FALSE ;
        # """

        cursor.execute(sql)
        results = []
        loaded_urls = []

        for row in cursor.fetchall():
            result = dict(zip(COLUMNS, row))
            results.append(result)
            loaded_urls.append(result.get("url"))

        loaded_list = ", ".join([repr(i) for i in loaded_urls])

        update_sql = f"""
          UPDATE REAL_ESTATE_INTEGRATED
          SET is_loaded = TRUE
          WHERE url in ({loaded_list})
        """

        cursor.execute(update_sql)
        self.connection.commit()

        return results

