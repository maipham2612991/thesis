import numpy as np

from scipy import stats


SCORE = 2.75


def return_outliers(df):
    df = df.dropna(subset=['price'])
    
    z_scores = stats.zscore(df['price'])
    df['absolute_score'] = np.abs(z_scores)
    
    outl_urls = df[df['absolute_score'] > SCORE]['url']
    
    outl_urls = list(outl_urls)
    print('Number of removed urls: ', len(outl_urls))
    return outl_urls
