#! /bin/bash
source /home/mai_phammaipham/venv/bin/activate
cd /home/mai_phammaipham/thesis/data_warehouse/
python etl_data_warehouse.py
deactivate
/snap/bin/bq load --source_format=NEWLINE_DELIMITED_JSON real_estate.raw_real_estate_integrated gs://raw_data_integration/"$(date +%d-%m-%Y)"/raw_real_estate_integrated.json /home/mai_phammaipham/thesis/data_warehouse/import_raw_data_to_bq/integration_schema.json
/snap/bin/bq query < /home/mai_phammaipham/thesis/data_warehouse/bq_to_star_schema/schema.sql --use_legacy_sql=false