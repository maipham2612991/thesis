"""
This program builds the real_estate_dev Postgresql database from the
real_estate_integrated.csv file.
"""
import googlemaps
import re
# import os
# import csv
# import googlemaps
# from importlib import resources
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine.url import URL
# from settings import *
from models import Base, Address, Author, Project, Property_type, Transaction_type, Post


# def get_real_estate_data(filepath):
#     """
#     This function gets the data from the csv file
#     """
#     with open(filepath, encoding='UTF-8') as csvfile:
#         csv_reader = csv.DictReader(csvfile, escapechar="'")
#         data = [row for row in csv_reader]
#         return data

# def normalize_address(address, prefix):
#     res = re.sub('(Huyện |Thành Phố |Thị Xã |Quận |Phường |Thị Trấn |Xã |Tỉnh )','',address.title())
#     if res.isnumeric():
#         return prefix+res
#     return res

def populate_database(session, real_estate_data):
    gmaps = googlemaps.Client(key='AIzaSyBjkeBscmwpPrZdcDwSCp1gPNb2sABjhlI')

    # insert the data
    i = 0
    for row in real_estate_data:
        print(row)
        if i > 1000:
            break
        i = i + 1
        author = (
            session.query(Author)
            .filter(Author.post_author == row["post_author"], Author.phone_number == row["phone_number"])
            .one_or_none()
        )
        if author is None:
            author = Author(
                post_author = row["post_author"], phone_number = row["phone_number"]
            )
            session.add(author)

        # row['addr_province'] = normalize_address(row['addr_province'], '')
        # row['addr_city'] = normalize_address(row['addr_city'],'Quận ')
        # row['addr_district'] = normalize_address(row['addr_district'],'Quận ')
        # row['addr_ward'] = normalize_address(row['addr_ward'],'Phường ')
        conv = lambda i : i or ''
        addr_province = re.sub('(huyện |thành phố |thị xã |quận |phường |thị trấn |xã |tỉnh )','',conv(row['addr_province']).lower())
        addr_city = re.sub('(huyện |thành phố |thị xã |quận |phường |thị trấn |xã |tỉnh )','',conv(row['addr_city']).lower())
        addr_district = re.sub('(huyện |thành phố |thị xã |quận |phường |thị trấn |xã |tỉnh )','',conv(row['addr_district']).lower())
        addr_ward = re.sub('(huyện |thành phố |thị xã |quận |phường |thị trấn |xã |tỉnh )','',conv(row['addr_ward']).lower())
        # if row['add_province'].isnumeric():

        address = (
            session.query(Address)
            .filter(
                Address.addr_province == addr_province, Address.addr_city == addr_city,
                Address.addr_district == addr_district, Address.addr_ward == addr_ward,
                Address.addr_street == row["addr_street"]
            )
            .one_or_none()
        )
        if address is None:
            # convert address to longitude and latitude
            filter_data = [row['addr_street'], row['addr_ward'], row['addr_district'], row['addr_city'], row['addr_province']]
            filter_data = [value for value in filter_data if value]
            temp = ', '.join(filter_data)
            longitude = None
            latitude = None
            if temp:
                try:
                    geocode_result = gmaps.geocode(temp)
                    dic = geocode_result[0]['geometry']['location']
                    longitude = dic['lng']
                    latitude = dic['lat']
                except:
                    print('!!!Failed converting!!!')
                    print(geocode_result)
                    print(temp)
                    print(filter_data)
                    print([row['addr_street'], row['addr_ward'], row['addr_district'], row['addr_city'], row['addr_province']])
            address = Address(
                addr_province = addr_province, addr_city = addr_city,
                addr_district = addr_district, addr_ward = addr_ward,
                addr_street = row["addr_street"], longitude = longitude, latitude = latitude
            )
            session.add(address)

        project = (
            session.query(Project)
            .filter(Project.project == row["project"])
            .one_or_none()
        )
        if project is None:
            project = Project(project = row["project"], project_size = row["project_size"])
            session.add(project)

        property_type = (
            session.query(Property_type)
            .filter(Property_type.property_type == row["property_type"])
            .one_or_none()
        )
        if property_type is None:
            property_type = Property_type(property_type = row["property_type"])
            session.add(property_type)

        transaction_type = (
            session.query(Transaction_type)
            .filter(Transaction_type.transaction_type == row["transaction_type"])
            .one_or_none()
        )
        if transaction_type is None:
            transaction_type = Transaction_type(transaction_type = row["transaction_type"])
            session.add(transaction_type)

        post = (
            session.query(Post)
            .filter(Post.url == row["url"])
            .one_or_none()
        )

        if post is None:
            post = Post(
                url = row["url"], price = float(row["price"]) if row["price"] else None , price_unit = row["price_unit"], area = float(row["area"]) if row["area"] else None,
                num_bedrooms = row["num_bedrooms"], num_bathrooms = row["num_bathrooms"],
                created_date = row["created_date"], expired_date = row["expired_date"],
                num_floors = row["num_floors"], floorth = row["floorth"], direction = row["direction"],
                legal = row["legal"], front = row["front"], alley = row["alley"], surrounding = row["surrounding"],
                surrounding_characteristics = row["surrounding_characteristics"], position = row["position"],
                internal_facility = row["internal_facility"]
            )
            session.add(post)

        # add the items to the relationships
        author.posts.append(post)
        address.posts.append(post)
        project.posts.append(post)
        property_type.posts.append(post)
        transaction_type.posts.append(post)
        session.commit()

    session.close()

def get_real_estate_data_from_sql():
    # connection = psycopg2.connect(user = settings.DATABASE.username,
    #                               password = settings.DATABASE.password,
    #                               host = settings.DATABASE.host,
    #                               port = settings.DATABASE.port,
    #                               database = settings.DATABASE.database)

    # cursor = connection.cursor()
    engine = create_engine("postgresql://postgres:khangmainguyen@34.70.141.169:5432/realestate_db")
    Session = sessionmaker()
    Session.configure(bind=engine)
    session = Session()
    sql = """ SELECT * from real_estate_integrated order by created_date desc limit 1000 """
    # sql = """ SELECT url from nha_dat_247_links where url = 'https://nhadat247.com.vn/ban-nha-rieng/sieu-pham-mat-pho-bach-maiso-hoa-hau108m22t-mt4m23-ty-kinh-doanh-bat-chap-pr95430.html'
    #"""
    # cursor.execute(sql)
    # session.execute(sql)
    list_of_urls = session.execute(sql)
    return list_of_urls


def main():
    print("starting")

    # get the real estate data into a dictionary structure
    # with resources.path(
    #     "project.data", "real_estate_integrated.csv"
    # ) as csv_filepath:
    #     data = get_real_estate_data(csv_filepath)
    #     real_estate_data = data
    

    engine = create_engine("postgresql://postgres:khangmainguyen@34.70.141.169:5432/listings_db")
    Base.metadata.create_all(engine)
    Session = sessionmaker()
    Session.configure(bind=engine)
    session = Session()
    real_estate_data = get_real_estate_data_from_sql()
    populate_database(session, real_estate_data)

    print("finished")


if __name__ == "__main__":
    main()
