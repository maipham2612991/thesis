export class ProjectAddrPrice {
  public project: string;
  public addrCity: string;
  public addrDistrict: string;
  public count: number;
  public averagePrice: number;
  public priceUnit: string;

  constructor(prop: any) {
    this.project  = prop.project
    this.addrCity  = prop.addr_city
    this.addrDistrict  = prop.addr_district
    this.count  = prop.count
    this.averagePrice  = prop.average_price
    this.priceUnit  = prop.price_unit
  }
}