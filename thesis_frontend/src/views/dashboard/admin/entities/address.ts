export class Address {
    private id: string;
    private addrProvince: string;
    private addrCity: string;
    private addrDistrict: string;
    private addrWard: string;
    private addrStreet: string;

    constructor(data: any) {
      this.id = data.id
      this.addrProvince = data.addr_province
      this.addrCity = data.addr_city
      this.addrDistrict = data.addr_district
      this.addrWard = data.addr_ward
      this.addrStreet = data.addr_street
    }
}
