export class Transaction {
    private id: string;
    private transactionType: string;
    private transactionCount: Number;

    constructor(data: any) {
      this.id = data.id
      this.transactionType = data.transaction_type
      this.transactionCount = data.transaction_count
    }

    public get getTransactionType() : string {
      return this.transactionType
    }

    public get getTransactionCount() : Number {
      return this.transactionCount
    }
}
