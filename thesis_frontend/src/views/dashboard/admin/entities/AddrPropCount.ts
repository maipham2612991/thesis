export class AddrPropCount {
    public propertyType: string;
    public addrCityProvince: string[];
    public addrCount: number[];

    constructor(prop: any) {
      this.propertyType = prop.property_type
      this.addrCityProvince = prop.addr_city_province
      this.addrCount = prop.addr_count
    }
}