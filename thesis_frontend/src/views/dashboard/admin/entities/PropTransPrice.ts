export class PropTransPrice {
    public propertyType: string;
    public transactionType: string;
    public avgPrice: Number;
    public priceUnit: string;

    constructor(prop: any) {
      this.propertyType = prop.property_type
      this.transactionType = prop.transaction_type
      
      if (prop.average_total_price <= 1) {
        this.avgPrice = prop.average_total_price * 1000
        this.priceUnit = prop.price_unit.replace("triệu", "nghìn")
      } else {
        this.avgPrice = prop.average_total_price
        this.priceUnit = prop.price_unit 
      }
    }

    public get getPropertyType() : string {
      return this.propertyType
    }

    public get getAvgPrice() : Number {
      return this.avgPrice
    }
}
