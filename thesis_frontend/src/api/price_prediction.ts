import axios from "axios";

const API_PRICE_PRED = process.env.VUE_APP_PREDICT_API;


const API_PRICE= axios.create({
  baseURL: API_PRICE_PRED,
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*"
  }
});

export {
  API_PRICE
};