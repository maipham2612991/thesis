from extract_info_staging.extract_info_staging_controller import InfoExtractorController

if __name__ == '__main__':
    controller = InfoExtractorController()
    controller.integrate()