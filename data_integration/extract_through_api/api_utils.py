import requests

from extract_info_staging.realestateDTO import RealestateDTO
from extract_info_staging.utils import Utils
from extract_through_api.content_type import ContentType
from extract_through_api.tag import Tag
from extract_through_api.tag_item import TagItem

API_URL = 'http://localhost:3005/api/v1/real-estate-extraction'


class APIUtils:
  @staticmethod
  def extract_through_api(re: RealestateDTO, tags: [Tag]) -> RealestateDTO:

    for tag in tags:
      content_type = tag.type
      content_type_value = tag.type.value

      if content_type in (ContentType.SURROUNDING, ContentType.SURROUNDING.SURROUNDING_CHARACTERISTICS):
        content_list = getattr(re, content_type_value)
        content_list.append(tag.content.lower())
        setattr(re, content_type_value, content_list)
      elif content_type == ContentType.INTERIOR_ROOM:
        content = tag.content.lower()
        re.internal_facility.append(content)
      else:
        try:
          # if content_type in (ContentType.ADDR_WARD, ContentType.ADDR_STREET, ContentType.ADDR_DISTRICT,
          #                     ContentType.ADDR_CITY, ContentType.PRICE, ContentType.AREA):
          #   continue

          if getattr(re, content_type_value) is None:
            setattr(re, content_type_value, tag.content.lower())
        except:
          pass

    return re

  @staticmethod
  def get_tag_item_from_api(full_description: [str]) -> [Tag]:
    tags = []
    response = requests.post(url=API_URL,
                            headers={},
                            json=full_description
                        ).json()

    for res in response:
      tag_item = TagItem.from_json_to_tag_item(res)
      tags += tag_item.get_tags()

    return tags
