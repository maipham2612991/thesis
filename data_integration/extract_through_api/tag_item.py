from extract_through_api.tag import Tag


class TagItem:
  def __init__(self,
    score: float,
    tags: [Tag]
  ):
    self.score = score
    self.tags = tags

  def get_tags(self):
    return self.tags

  @staticmethod
  def from_json_to_tag_item(data: dict):
    tags = []
    for tag_json in data.get("tags"):
      tag = Tag.from_json_to_tag(tag_json)
      if tag:
        tags.append(tag)

    return TagItem(
      score=data.get("score"),
      tags=tags
    )
