import enum

class ContentType(enum.Enum):
  # ADDR_STREET = 'addr_street'
  # ADDR_WARD = 'addr_ward'
  # ADDR_DISTRICT = 'addr_district'
  # ADDR_CITY = 'addr_city'
  SURROUNDING = 'surrounding'
  SURROUNDING_CHARACTERISTICS = 'surrounding_characteristics'
  TRANSACTION_TYPE = 'transaction_type'
  REALESTATE_TYPE = 'realestate_type'
  POSITION = 'position'
  LEGAL = 'legal'
  POTENTIAL = 'potential'
  # AREA = 'area'
  # PROJECT = 'project'
  DIRECTION = 'orientation'
  INTERIOR_FLOOR = 'interior_floor'
  INTERIOR_ROOM = 'interior_room'
  # PRICE = 'price'
  PHONE = 'phone'
  EMAIL = 'email'

  @staticmethod
  def from_str(label):
    return ContentType(label)

