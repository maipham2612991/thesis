from extract_through_api.content_type import ContentType


class Tag:
  def __init__(self,
      content: str,
      type: ContentType
  ):
    self.content = content
    self.type = type

  @staticmethod
  def from_json_to_tag(data: dict):

    try:
      return Tag(
        content= data.get("content"),
        type=ContentType.from_str(data.get("type"))
      )
    except ValueError:
      return None