from re import findall
from datetime import date, datetime

re_float_num = r"\d+\.\d+$"

class Utils:
  @staticmethod
  def check_valid_number(number: str):
    try:
      return int(number)
    except ValueError:
      match_float = findall(re_float_num, number)
      if len(match_float) == 1:
        return float(match_float[0])
      else:
        return None

  @staticmethod
  def extract_number(room):
    try:
      n = int(room.split(" ")[0].strip())
      return n if n else None
    except:
      return None

  @staticmethod
  def standardize_price(price, unit):
    # if "nghìn" in unit and (0.1<=price<1000 or 0.1<=price/10**3<1000):
    #   if 0.1<price/10**3<1000:
    #     price /= 10**3
    #   price /= 10**3
    #   unit = unit.replace("nghìn", "triệu")
    #   return price, unit
    # elif "triệu" in unit and (0.1<=price<1000 or 0.1<=price/10**6<1000):
    #   if 0.1<price/10**6<1000:
    #     price /= 10**6
    #   return price, unit
    # elif "tỷ" in unit and (0.1<=price<1000 or 0.1<=price/10**9<1000):
    #   if 0.1<=price/10**9<1000:
    #     price /= 10**9
    #   price *= 10**3
    #   unit = unit.replace("tỷ", "triệu")
    #   return price, unit
    #
    # return None, None
    ## correct data
    if "nghìn" in unit:
      if price >= 1000:
        price /= 10**3
      price /= 10**3
      unit = unit.replace("nghìn", "triệu")
    elif "triệu" in unit and price >= 1000000:
      price /= 10**6
    elif "tỷ" in unit:
      if price >= 1000000000:
        price /= 10**9
      elif price >= 1000000:
        price /= 10**6
      price *= 10**3
      unit = unit.replace("tỷ", "triệu")

    return price, unit

  @staticmethod
  def calculate_price_per_unit(price: int or float, unit: str, area: int or float):
    if "/m²" in unit:
      return price, unit
    else:
      return price/area, unit.replace("triệu", "triệu/m²")

  @staticmethod
  def from_datetime_to_date(dt: datetime):
    return dt.date().strftime("%Y-%m-%d")

