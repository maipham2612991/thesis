from Processing.alonhdat_processing import AloNhaDatProcessing
from Processing.propzy_processing import PropzyProcessing
from db_config import *
import psycopg2
from Processing.batdongsan_processing import BatDongSanProcessing
from Processing.nhadat247_processing import NhaDat247Processing
from Processing.homedy_processing import HomedyProcessing
from Processing.chotot_processing import ChoTotProcessing
from Processing.dothi_processing import DoThiProcessing


class InfoExtractorController:
  def __init__(self):
    self.connection = psycopg2.connect(user=user,
                                  password=password,
                                  host=host,
                                  port=port,
                                  database=database)

    self.cursor = self.connection.cursor()

  def integrate(self):
    batdongsanProcessing = BatDongSanProcessing()
    nhadat247Processing = NhaDat247Processing()
    homedyProcessing = HomedyProcessing()
    propzyProcessing = PropzyProcessing()
    alonhadatProcessing = AloNhaDatProcessing()
    chototProcessing = ChoTotProcessing()
    dothiProcessing = DoThiProcessing()

    res = batdongsanProcessing.query(self.cursor)
    self.import_to_db(res, batdongsanProcessing.name)
    res = nhadat247Processing.query(self.cursor)
    self.import_to_db(res, nhadat247Processing.name)
    res = homedyProcessing.query(self.cursor)
    self.import_to_db(res, homedyProcessing.name)
    res = propzyProcessing.query(self.cursor)
    self.import_to_db(res, propzyProcessing.name)
    res = alonhadatProcessing.query(self.cursor)
    self.import_to_db(res, alonhadatProcessing.name)
    res = chototProcessing.query(self.cursor)
    self.import_to_db(res, chototProcessing.name)
    res = dothiProcessing.query(self.cursor)
    self.import_to_db(res, dothiProcessing.name)

  def import_to_db(self, res, table_name):
    for re in res:
      re_dict = re.__dict__
      columns = ", ".join(re_dict.keys())
      placeholders = ", ".join([repr(i) for i in re_dict.values()])

      sql = f"""INSERT INTO REAL_ESTATE_INTEGRATED ({columns})
              VALUES ({placeholders}) ON CONFLICT DO NOTHING;
      """
      print(sql)

      self.cursor.execute(sql.replace("None", "null").replace("[]", "null").replace("[", "ARRAY["))
      self.connection.commit()

      # mark extracted

      sql = f"""UPDATE {table_name} SET is_extracted = true 
              WHERE url = '{re.url}' 
      """

      print(sql)
      self.cursor.execute(sql)
      self.connection.commit()

