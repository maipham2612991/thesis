

class RealestateDTO:
  def __init__(self):
    self.price: float = None
    self.price_unit: str = None
    self.area: float = None
    self.transaction_type: str = None
    self.property_type: str = None
    self.addr_province:str = None
    self.addr_city: str = None
    self.addr_district: str = None
    self.addr_ward: str = None
    self.addr_street: str = None
    self.num_bedrooms: int = None
    self.num_bathrooms: int = None
    self.project: str = None
    self.project_size: str = None
    self.created_date = None
    self.expired_date = None
    self.num_floors = None
    self.floorth = None
    self.direction: str = None
    self.legal: str = None
    self.front: float = None
    self.alley: float = None
    self.url: str = None
    self.post_author: str = None
    self.phone_number: str = None
    self.surrounding: [str] = []
    self.surrounding_characteristics: [str] = []
    self.position: str = None
    self.internal_facility: [str] = []

  def setArea(self, area):
    self.area = area

  def setPrice(self, price):
    self.price = price

  def setPriceUnit(self, price_unit):
    self.price_unit = price_unit

  def setUrl(self, url):
    self.url = url