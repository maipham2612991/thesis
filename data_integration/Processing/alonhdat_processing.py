from Processing.processing import Processing
from extract_info_staging.realestateDTO import RealestateDTO
from extract_info_staging.utils import Utils


class AloNhaDatProcessing(Processing):
  def __init__(self):
    super().__init__()
    self.name = "raw_alonhadat"

  def extract_all(self, r) -> RealestateDTO:
    re = RealestateDTO()

    # url
    self.extract_url(re, r)

    # area
    self.extract_area(re, r)

    # price
    self.extract_price(re, r)

    self.extract_front(re, r)
    if "nhà trong hẻm" in r[2].lower():
    # route
      re.alley = re.front
      re.front = None

    # Property name
    self.extract_property(re, r)

    # address
    self.extract_address(re, r)

    # bedroom, bathroom
    self.extract_bedroom(re, r)

    self.extract_bathroom(re, r)

    # project
    self.extract_project(re, r)

    # created and expired datetime
    self.extract_created_date(re, r)

    # number_of floor
    self.extract_num_floors(re, r)

    # direction
    self.extract_direction(re, r)

    # legal
    self.extract_legal(re, r)

    self.extract_phone_number(re, r)

    self.extract_project_size(re, r)

    self.extract_post_author(re, r)

    return re

  def extract_project_size(self, re, r):
    re.project_size = r[18]

  def extract_post_author(self, re, r):
    re.post_author = r[15]

  def extract_price(self,re, r):
    price_no_vat = r[1].replace("\xa0", " ")
    if "/" in price_no_vat and "m" in price_no_vat:
      list_price = price_no_vat.replace(",", ".").split(" ")
      re.price = Utils.check_valid_number(list_price[0])
      if re.price:
        unit = "".join(list_price[1:]).replace("/m", "/m²")
        if "triệu" in unit:
          re.price_unit = unit
        elif "ngàn" in unit or "nghìn" in unit:
          re.price /= 1000
          re.price_unit = unit.replace("ngàn", "triệu").replace("nghìn", "triệu")
        else:
          re.price *= 1000
          re.price_unit = unit.replace("tỷ", "triệu")
    else:
      self.standardize_price(re, price_no_vat)

  def extract_address(self, re, r):
    address_id = r[3].lower()
    addresses = address_id.strip().split(",")
    try:
      self.standardize_address(re, addresses)
    except:
      pass

  def extract_property(self, re, r):
    property_name = r[2].strip().lower()
    self.standardize_property(re, property_name.replace("cần bán", "bán"))
    map_key = {
      "mặt bằng": "mặt bằng kinh doanh",
      "biệt thự, nhà liền kề": "nhà biệt thự, liền kề",
      "shop, kiot, quán": "cửa hàng, ki ốt",
      "nhà trong hẻm": "nhà riêng",
      "các loại khác": "loại bất động sản khác",
      "kho, xưởng": "kho, nhà xưởng, đất",
      "đất thổ cư, đất ở": "đất",
      "phòng trọ, nhà trọ": "nhà trọ, phòng trọ",
      "đất nền, liền kề, đất dự án": "đất nền dự án",
      "nhà mặt tiền": "nhà mặt phố",
      "trang trại": "trang trại, khu nghỉ dưỡng",
      "đất nền dự án": "đất nền dự án",
      "nhà hàng, khách sạn": "loại bất động sản khác",
      "trang trại, khu nghỉ dưỡng": "loại bất động sản khác"
    }
    if re.property_type in map_key.keys():
      re.property_type = map_key.get(re.property_type)

  def extract_area(self, re, r):
    total_site_area = r[0]
    self.standardize_area(re, total_site_area)
