from Processing.processing import Processing
from extract_info_staging.realestateDTO import RealestateDTO


class HomedyProcessing(Processing):
  def __init__(self):
    super().__init__()
    self.name = "raw_homedy"

  def extract_all(self, r):
    re = RealestateDTO()

    # url
    self.extract_url(re, r)

    # area
    self.extract_area(re, r)

    # price
    self.extract_price(re, r)

    # project
    self.extract_project(re, r)

    # property
    self.extract_property(re, r)

    # address
    self.extract_address(re, r)

    # bedroom, bathroom
    self.extract_bedroom(re, r)

    self.extract_bathroom(re, r)

    # created and expired datetime
    self.extract_created_date(re, r)

    self.extract_expired_date(re, r)

    # number_of floor
    self.extract_num_floors(re, r)

    # direction
    self.extract_direction(re, r)

    # legal
    self.extract_legal(re, r)

    # front
    self.extract_front(re, r)

    # route
    self.extract_alley(re, r)

    self.extract_phone_number(re, r)

    self.extract_internal_facility(re, r)

    return re

  def extract_property(self, re, r):
    re.transaction_type = r[2].lower().strip()

  def extract_price(self,re, r):
    price_no_vat = r[1].split("-")
    if len(price_no_vat) == 1:
      price = price_no_vat[0]
    else:
      price = price_no_vat[1].strip()
    self.standardize_price(re, price)

  def extract_address(self, re, r):
    prop_type = {"căn hộ chung cư": "căn hộ", "căn hộ dịch vụ": "căn hộ", "nhà riêng": "nhà riêng",
                 "nhà mặt phố": "nhà mặt phố", "nhà phố thương mại": "nhà mặt phố",
                 "nhà biệt thự, liền kề": "nhà biệt thự, liền kề", "nhà trọ, phòng trọ": "nhà trọ, phòng trọ",
                 "đất nền dự án": "đất nền dự án", "bất động sản khác": "loại bất động sản khác",
                 "cửa hàng, mặt bằng bán lẻ": "cửa hàng, ki ốt", "đất, nhà xưởng, kho bãi": "kho, nhà xưởng, đất",
                 "căn hộ": "căn hộ", "đất": "đất", "văn phòng": "văn phòng"}

    post_author = r[15]
    try:
      addr_prop = r[3].lower().replace(re.transaction_type, "").replace("tp", "").strip()

      property_type = ""
      for prop in prop_type.keys():
        if prop in addr_prop:
          property_type = prop
          re.property_type = prop_type.get(prop)
          break

      addr = addr_prop.replace(property_type, "").replace("tại", "").strip()

      if post_author and post_author.lower() == addr.lower():
        re.project = post_author
      else:
        re.post_author = post_author

      if re.project:
        addr = addr.replace(re.project.lower(), "")
      addrs = addr.replace("  ", ", ").replace("-", "").split(",")

      address = [a.strip() for a in addrs if a.strip()]

      self.standardize_address(re, address)
    except:
      pass

    # if re.property_type:
      # re.property_type = re.property_type.replace("bất động sản khác", "loại bất động sản khác")

    # address_id = r[3].lower().split("tại")
    # if len(address_id) == 2:
    #   addresses = address_id[1].strip().split(",")
    #   self.standardize_address(re, addresses)
    #   self.standardize_property(re, address_id[0].strip())
    # elif len(address_id) == 1:
    #   if re.project:
    #     property_name = address_id[0].replace(re.project.lower(), '').strip()
    #     self.standardize_property(re, property_name)
    #   else:
    #     self.standardize_property(re, address_id[0].strip())

  def extract_area(self, re, r):
    total_site_area = r[0].replace("m 2", "m²").split("-")
    if len(total_site_area) == 2:
      area = total_site_area[1].strip()
    else:
      area = total_site_area[0]
    self.standardize_area(re, area)

  def extract_internal_facility(self, re, r):
    if r[23]:
      re.internal_facility = r[23]


