from Processing.processing import Processing
from extract_info_staging.realestateDTO import RealestateDTO
from extract_through_api.content_type import ContentType

property_type = {
  "đất nông nghiệp": "đất",
  "duplex": "căn hộ",
  "nhà phố liền kề": "nhà biệt thự, liền kề",
  "shophouse": "mặt bằng kinh doanh",
  "tập thể, cư xá": "căn hộ",
  "tập thể": "căn hộ",
  "nhà biệt thự": "nhà biệt thự, liền kề",
  "penthouse": "căn hộ",
  "đất công nghiệp": "đất",
  "căn hộ dịch vụ, mini": "căn hộ",
  "chung cư": "căn hộ",
  "officetel": "văn phòng",
  "nhà mặt phố, mặt tiền": "nhà mặt phố",
  "đất thổ cư": "đất",
  "nhà ngõ, hẻm": "nhà riêng",
  "mặt bằng kinh doanh": "mặt bằng kinh doanh",
  "căn hộ dịch vụ": "căn hộ",
  "nhà ngõ": "nhà riêng"
}

class ChoTotProcessing(Processing):
  def __init__(self):
    super().__init__()
    self.name = "raw_chotot"

  def extract_all(self, r) -> RealestateDTO:
    re = RealestateDTO()

    # url
    self.extract_url(re, r)

    # area
    self.extract_area(re, r)

    # price
    self.extract_price(re, r)

    # Property name
    self.extract_property(re, r)

    # address
    self.extract_address(re, r)

    # bedroom, bathroom
    self.extract_bedroom(re, r)

    self.extract_bathroom(re, r)

    # project
    self.extract_project(re, r)

    # created and expired datetime
    self.extract_created_date(re, r)

    self.extract_expired_date(re, r)

    # number_of floor
    self.extract_num_floors(re, r)

    # direction
    self.extract_direction(re, r)

    # legal
    self.extract_legal(re, r)

    # front
    self.extract_front(re, r)

    # route
    self.extract_alley(re, r)

    self.extract_phone_number(re, r)

    self.extract_post_author(re, r)

    return re

  def extract_post_author(self, re, r):
    re.post_author = r[15]

  def extract_price(self,re, r):
    price_no_vat = r[1]
    self.standardize_price(re, price_no_vat)

  def extract_address(self, re, r):
    address_id = r[3]
    try:
      addresses = address_id.lower().strip().split(",")
      if 'dự án' in addresses[0]:
        addresses = addresses[1:]
      self.standardize_address(re, addresses)
    except:
      pass

  def extract_property(self, re, r):
    if 'mua' in re.url:
      re.transaction_type = 'bán'
    elif 'thue' in re.url:
      re.transaction_type = 'cho thuê'
    
    property_name = r[2]
    if property_name:
      property_name = property_name.strip().lower()
      if property_type.get(property_name):
        re.property_type = property_type.get(property_name)
      else:
        re.property_type = property_name

    if not re.transaction_type:
      if re.price_unit == "triệu/m²":
        re.transaction_type = "bán"
      else:
        re.transaction_type = "cho thuê"

  def extract_area(self, re, r):
    total_site_area = r[0].replace("m 2", "m²")
    if total_site_area is None:
      for tag in self.tags:
        if tag.type == ContentType.AREA:
          total_site_area = tag.content
          break
    self.standardize_area(re, total_site_area)

  def extract_num_floors(self, re, r):
    floors = r[17]
    if floors and "--" not in floors:
        re.floorth = self.extract_numbers(floors)

  def extract_numbers(self, floors):
    num = 0
    for floor in floors.split(" "):
      try:
        num += int(floor.strip())
      except:
        continue
    return num if num else None
