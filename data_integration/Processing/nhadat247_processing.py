from Processing.processing import Processing
from extract_info_staging.realestateDTO import RealestateDTO


class NhaDat247Processing(Processing):
  def __init__(self):
    super().__init__()
    self.name = "raw_247"

  def extract_all(self, r):
    re = RealestateDTO()

    # url
    self.extract_url(re, r)

    # area
    self.extract_area(re, r)

    # price
    self.extract_price(re, r)

    # Property name
    self.extract_property(re, r)

    # address
    self.extract_address(re, r)

    # bedroom, bathroom
    self.extract_bedroom(re, r)

    self.extract_bathroom(re, r)

    # project
    self.extract_project(re, r)

    # created and expired datetime
    self.extract_created_date(re, r)

    self.extract_expired_date(re, r)

    # number_of floor
    self.extract_num_floors(re, r)

    # direction
    self.extract_direction(re, r)

    # legal
    self.extract_legal(re, r)

    # front
    self.extract_front(re, r)

    # route
    self.extract_alley(re, r)

    # post_author
    self.extract_post_author(re, r)

    # 19: phone_number
    self.extract_phone_number(re, r)

    return re

  def extract_price(self,re, r):
    price_no_vat = r[1]
    self.standardize_price(re, price_no_vat)

  def extract_address(self, re, r):
    address_id = r[3].lower().split("tại")[1].strip()
    addresses = address_id.strip().split(",")
    self.standardize_address(re, addresses)

  def extract_property(self, re, r):
    property_name = r[2].strip().lower()
    self.standardize_property(re, property_name)

    if re.property_type == "trang trại, khu nghỉ dưỡng":
      re.property_type = "loại bất động sản khác"
    if re.property_type == "kho, nhà xưởng":
      re.property_type = "kho, nhà xưởng, đất"

  def extract_post_author(self, re, r):
    re.post_author = r[15]

  def extract_area(self, re, r):
    total_site_area = r[0]
    self.standardize_area(re, total_site_area)

  def extract_direction(self, re, r):
    try:
      d = r[10].replace("Không xác định", "").strip()
      if d:
        re.direction = d
      else:
        self.process_direction(None, re)
    except:
      pass
