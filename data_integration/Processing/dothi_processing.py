from Processing.processing import Processing
from extract_info_staging.realestateDTO import RealestateDTO


class DoThiProcessing(Processing):
  def __init__(self):
    super().__init__()
    self.name = "raw_dothi"

  def extract_all(self, r):
    re = RealestateDTO()

    # url
    self.extract_url(re, r)

    # area
    self.extract_area(re, r)

    # price
    self.extract_price(re, r)

    # Property name
    self.extract_property(re, r)

    # address
    self.extract_address(re, r)

    # bedroom, bathroom
    self.extract_bedroom(re, r)

    self.extract_bathroom(re, r)

    # project
    self.extract_project(re, r)

    # created and expired datetime
    self.extract_created_date(re, r)

    self.extract_expired_date(re, r)

    # number_of floor
    self.extract_num_floors(re, r)

    # direction
    self.extract_direction(re, r)

    # legal
    self.extract_legal(re, r)

    # front
    self.extract_front(re, r)

    # route
    self.extract_alley(re, r)

    # post_author
    self.extract_post_author(re, r)

    # 19: phone_number
    self.extract_phone_number(re, r)

    return re

  def extract_property(self, re, r):
      property_name = r[2].strip().lower()
      self.standardize_property(re, property_name)
      re.property_type = re.property_type.replace("đất nền dự án (đất trong dự án quy hoạch)", "đất nền dự án")
      re.property_type = re.property_type.replace("nhà mặt phố (nhà mặt tiền trên các tuyến phố)", "nhà mặt phố")
      re.property_type = re.property_type.replace("Bán nhà biệt thự, liền kề (nhà trong dự án quy hoạch)", "nhà biệt thự, liền kề")

  def extract_price(self,re, r):
      price_no_vat = r[1]
      self.standardize_price(re, price_no_vat)

  def extract_area(self, re, r):
      total_site_area = r[0]
      self.standardize_area(re, total_site_area)

  def extract_direction(self, re, r):
      try:
        d = r[10].replace("KXĐ", "").strip()
        if d:
          re.direction = d
        else:
          self.process_direction(None, re)
      except:
        pass

  def extract_post_author(self, re, r):
    re.post_author = r[15]

  def extract_address(self, re, r):
    prop_type = {"căn hộ chung cư": "căn hộ", "căn hộ dịch vụ": "căn hộ", "nhà riêng": "nhà riêng",
                 "nhà mặt phố": "nhà mặt phố", "nhà phố thương mại": "nhà mặt phố",
                 "nhà biệt thự, liền kề": "nhà biệt thự, liền kề", "nhà trọ, phòng trọ": "nhà trọ, phòng trọ",
                 "đất nền dự án": "đất nền dự án", "bất động sản khác": "loại bất động sản khác",
                 "cửa hàng, mặt bằng bán lẻ": "cửa hàng, ki ốt", "đất, nhà xưởng, kho bãi": "kho, nhà xưởng, đất",
                 "căn hộ": "căn hộ", "đất": "đất", "văn phòng": "văn phòng", "nền dự án":"nền dự án", "đất dự án":"đất dự án"}

    post_author = r[15]
    try:
      addr_prop = r[3].lower().replace(re.transaction_type, "").replace("tp", "").strip()

      property_type = ""
      for prop in prop_type.keys():
        if prop in addr_prop:
          property_type = prop
          re.property_type = prop_type.get(prop)
          break

      addr = addr_prop.replace(property_type, "").replace("tại", "").strip()

      if post_author and post_author.lower() == addr.lower():
        re.project = post_author
      else:
        re.post_author = post_author

      if re.project:
        addr = addr.replace(re.project.lower(), "")
      addrs = addr.replace("  ", ", ").replace("-", "").split(",")

      address = [a.strip() for a in addrs if a.strip()]

      self.standardize_address(re, address)
    except:
      pass