from Processing.processing import Processing
from extract_info_staging.realestateDTO import RealestateDTO
from extract_through_api.content_type import ContentType


class BatDongSanProcessing(Processing):
  def __init__(self):
    super().__init__()
    self.name = "raw_batdongsan"

  def extract_all(self, r) -> RealestateDTO:
    re = RealestateDTO()

    # url
    self.extract_url(re, r)

    # area
    self.extract_area(re, r)

    # price
    self.extract_price(re, r)

    # Property name
    self.extract_property(re, r)

    # address
    self.extract_address(re, r)

    # bedroom, bathroom
    self.extract_bedroom(re, r)

    self.extract_bathroom(re, r)

    # project
    self.extract_project(re, r)

    # created and expired datetime
    self.extract_created_date(re, r)

    self.extract_expired_date(re, r)

    # number_of floor
    self.extract_num_floors(re, r)

    # direction
    self.extract_direction(re, r)

    # legal
    self.extract_legal(re, r)

    # front
    self.extract_front(re, r)

    # route
    self.extract_alley(re, r)

    self.extract_phone_number(re, r)

    self.extract_project_size(re, r)

    self.extract_post_author(re, r)

    return re

  def extract_project_size(self, re, r):
    re.project_size = r[18]

  def extract_post_author(self, re, r):
    re.post_author = r[15]

  def extract_price(self,re, r):
    price_no_vat = r[1]
    self.standardize_price(re, price_no_vat)

  def extract_address(self, re, r):
    address_id = r[3].lower()
    addresses = address_id.strip().split(",")
    try:
      if 'dự án' in addresses[0]:
        addresses = addresses[1:]
      self.standardize_address(re, addresses)
    except:
      pass

  def extract_property(self, re, r):
    property_name = r[2].strip().lower() \
      .replace("bán nhà biệt thự, liền kề (nhà trong dự án quy hoạch)", "bán nhà biệt thự, liền kề") \
      .replace("bán nhà mặt phố (nhà mặt tiền trên các tuyến phố)", "bán nhà mặt phố") \
      .replace("bán đất nền dự án (đất trong dự án quy hoạch)", "bán đất nền dự án")

    if re.property_type == "kho, nhà xưởng":
      re.property_type = "kho, nhà xưởng, đất"

    self.standardize_property(re, property_name)

  def extract_area(self, re, r):
    total_site_area = r[0]
    if total_site_area is None:
      for tag in self.tags:
        if tag.type == ContentType.AREA:
          total_site_area = tag.content
          break
    self.standardize_area(re, total_site_area)
