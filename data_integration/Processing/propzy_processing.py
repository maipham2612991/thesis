from Processing.processing import Processing
from extract_info_staging.realestateDTO import RealestateDTO
from extract_info_staging.utils import Utils


class PropzyProcessing(Processing):
  def __init__(self):
    super().__init__()
    self.name = "raw_propzy"

  def extract_all(self, r):
    re = RealestateDTO()

    # url
    self.extract_url(re, r)

    # area
    self.extract_area(re, r)

    # price
    self.extract_price(re, r)

    # Property name
    self.extract_property(re, r)

    # address
    self.extract_address(re, r)

    # bedroom, bathroom
    self.extract_bedroom(re, r)

    self.extract_bathroom(re, r)

    # project
    self.extract_project(re, r)

    # created and expired datetime
    self.extract_created_date(re, r)

    self.extract_expired_date(re, r)

    self.extract_updated_date(re, r)

    # number_of floor
    self.extract_num_floors(re, r)

    # direction
    self.extract_direction(re, r)

    # legal
    self.extract_legal(re, r)

    # front
    self.extract_front(re, r)

    # route
    self.extract_alley(re, r)

    self.extract_phone_number(re, r)

    return re

  def extract_direction(self, re, r):
    if "Không xác định" not in r[10]:
      re.direction = r[10]

    if not re.direction:
      self.process_direction(None, re)

  def extract_num_floors(self, re, r):
    floors = r[17]
    if floors and "--" not in floors:
      if re.property_type and 'căn hộ' in re.property_type:
        re.floorth = self.extract_numbers(floors)
      else:
        re.num_floors = self.extract_numbers(floors)

  def extract_numbers(self, floors):
    num = 0
    for floor in floors.split(" "):
      try:
        num += int(floor.strip())
      except:
        continue
    return num if num else None

  def extract_updated_date(self, re, r):
    created = r[16]
    if created:
      re.created_date = Utils.from_datetime_to_date(created)

  def extract_price(self, re, r):
    price_no_vat = r[1]
    self.standardize_price(re, price_no_vat)

  def extract_address(self, re, r):
    address_id = r[3].lower()\
      .replace("hcm", "hồ chí minh").replace("q.", "quận ").replace("p.", "phường ")\
      .replace("h.", "huyện ").replace("x.", "xã ")
    addresses = address_id.strip().split(",")
    self.standardize_address(re, addresses[::-1])

  def extract_property(self, re, r):
    if 'mua' in re.url:
      re.transaction_type = 'bán'
    elif 'thue' in re.url:
      re.transaction_type = 'cho thuê'

    if 'nha' in re.url:
      re.property_type = 'nhà riêng'
    elif 'can-ho' in re.url:
      re.property_type = "căn hộ"
    elif 'dat-nen-du-an' in re.url:
      re.property_type = "đất nền dự án"
    elif 'dat-nen' in re.url:
      re.property_type = "đất"

  def extract_area(self, re, r):
    total_site_area = r[0]
    self.standardize_area(re, total_site_area)
