from abc import ABC, abstractmethod
from re import findall

from extract_info_staging.utils import Utils
from extract_through_api.api_utils import APIUtils
from extract_through_api.content_type import ContentType
from extract_through_api.tag import Tag
from datetime import datetime

regex_phone = r'\d{3}.?\d{4}.?\d{3}'
regex_email = r'[a-zA-Z0-9_.]+@(.[a-zA-Z]+)+'


class Processing(ABC):
  def __init__(self):
    self.re_list = []
    self.name = None
    self.tags: [Tag] = []

  def query(self, cursor):
    cursor = cursor
    sql = f""" SELECT total_site_area, price_no_vat, property_name, address_id,
                        number_of_bedrooms, number_of_bathrooms, project_id, 
                        created_datetime, expired_datetime, number_of_floors, 
                        direction, legal_info, front_length, route_length, url, 
                        post_author, updated_datetime, floor, project_size, 
                        phone_number, email, full_description, near_facility, internal_facility
        from {self.name} 
        where 
          (is_extracted = false OR is_extracted IS NULL) 
          and total_site_area is not null 
          and price_no_vat is not null 
        limit 10000 ; """
    cursor.execute(sql)
    results = cursor.fetchall()

    for result in results:
      # print(f"query output: {result}")

      full_description = result[21] if result[22] is None else result[21] + result[22]
      full_description = [result[3]] + [desc for desc in full_description if desc]

      try:
        self.tags = APIUtils.get_tag_item_from_api(full_description)
      except:
        pass

      re = self.extract_all(result)
      if not re:
        continue
      # print(f"after extract_all: {re.__dict__}")

      re = APIUtils.extract_through_api(re, self.tags)

      print(f"after call API: {re.__dict__}")

      self.re_list.append(re)

    return self.re_list

  @abstractmethod
  def extract_all(self, re):
    pass

  @abstractmethod
  def extract_address(self, re, r):
    pass

  @abstractmethod
  def extract_property(self, re, r):
    pass

  @abstractmethod
  def extract_area(self, re, r):
    pass

  @abstractmethod
  def extract_price(self, re, r):
    pass

  def extract_url(self, re, r):
    re.url = r[14]

  def extract_direction(self, re, r):
    direction = r[10]
    if direction:
      direction = direction.replace("_", '')
    self.process_direction(direction, re)

  def process_direction(self, direction, re):
    if not direction:
      for tag in self.tags:
        if tag.type == ContentType.DIRECTION:
          direction = tag.content
          break

    if direction:
      re.direction = direction.lower().replace("-", " ").strip().replace(" ", "-") \
        .replace("t.", "tây").replace("đ.", "đông").replace("n.", "nam").replace("b.", "bắc")

  def extract_project(self, re, r):
    project = r[6]

    re.project = project

  def extract_created_date(self, re, r):
    updated = r[16]
    created = r[7]

    if created > datetime.now():
      created = updated
    if created:
      re.created_date = Utils.from_datetime_to_date(created)

  def extract_expired_date(self, re, r):
    expired = r[8]
    if expired:
      re.expired_date = Utils.from_datetime_to_date(expired)

  def extract_legal(self, re, r):
    legal = r[11]
    if legal:
      legal = legal.replace("---", "")
    if not legal:
      for tag in self.tags:
        if tag.type == ContentType.LEGAL:
          legal = tag.content
          break
    if legal:
      if "sổ" in legal.lower() or "cc" in legal.lower() or "chính chủ" in legal.lower() or "sh" in legal.lower():
        re.legal = "có sổ chính chủ"
      else:
        re.legal = legal.strip()

  def extract_num_floors(self, re, r):
    floors = r[9]
    if floors is None:
      for tag in self.tags:
        if tag.type == ContentType.INTERIOR_FLOOR:
          floors = tag.content
          break
    if floors:
      if re.property_type and 'căn hộ' in re.property_type:
        re.floorth = Utils.extract_number(floors)
      else:
        re.num_floors = Utils.extract_number(floors)

  def extract_bathroom(self, re, r):
    bathroom = r[5]
    if bathroom is None:
      for tag in self.tags:
        if tag.type == ContentType.INTERIOR_ROOM and "pn" or "phòng ngủ" in tag.content.lower():
          bathroom = tag.content
          break
    if bathroom:
      re.num_bathrooms = Utils.extract_number(bathroom)

  def extract_bedroom(self, re, r):
    bedroom = r[4]
    if bedroom is None:
      for tag in self.tags:
        if tag.type == ContentType.INTERIOR_ROOM and "wc" or "phòng tắm" in tag.content.lower():
          bedroom = tag.content
          break
    if bedroom:
      re.num_bedrooms = Utils.extract_number(bedroom)

  def extract_alley(self, re, r):
    route = r[13]
    if route:
      re.alley = Utils.check_valid_number(route.replace("m", " ").split(" ")[0].strip())

  def extract_front(self, re, r):
    front = r[12]
    if front:
      re.front = Utils.check_valid_number(front.replace("m", " ").split(" ")[0].strip())

  def standardize_address(self, re, addresses):
    addresses = addresses[::-1]
    first_addr = addresses[0].strip().strip(".")
    if first_addr == "hà nội":
      first_addr = "hà nội"
    if "hà nội" in first_addr or "hồ chí minh" in first_addr or "đà nẵng" in first_addr \
        or "cần thơ" in first_addr or "hải phòng" in first_addr or "hà nội" in first_addr:
      try:
        re.addr_city = first_addr
        re.addr_district = addresses[1].strip()
      except:
        pass
    else:
      try:
        re.addr_province = first_addr
        re.addr_city = addresses[1].strip()
      except:
        pass

    try:
      if 'đường' in addresses[2] or 'phố' in addresses[2]:
        re.addr_street = addresses[2].strip()
      else:
        re.addr_ward = addresses[2].strip()
        re.addr_street = addresses[3].strip()
    except:
      pass

    self.standardize_address_attrs(re)

  def standardize_address_attrs(self, re):
    # province
    if re.addr_province:
      re.addr_province = re.addr_province.replace("tỉnh", "").strip()
      if re.addr_province in "thừa thiên-huế":
        re.addr_province = "thừa thiên huế"
      elif re.addr_province in "bà rịa vũng tàu":
        re.addr_province = "bà rịa vũng tàu"

    # city
    if re.addr_city:
      re.addr_city = re.addr_city.replace("tp", "").replace("thành phố", "").replace("huyện", "").replace("thị xã", "").strip()

    # district
    if re.addr_district:
      district = re.addr_district.replace("quận", "").replace("thành phố", "").replace("huyện", "").strip()
      try:
        if int(district):
          re.addr_district = "quận " + district
      except:
        re.addr_district = district

    # ward
    if re.addr_ward:
      ward = re.addr_ward.replace("phường", "").replace("xã", "").replace("thị trấn", "").strip()
      try:
        if int(ward):
          re.addr_ward = "phường " + ward
      except:
        re.addr_ward = ward

    # street
    if re.addr_street:
      street = re.addr_street.replace("đường", "").replace("phố", "").strip()
      try:
        if int(street):
          re.addr_street = "đường " + street
      except:
        re.addr_street = street

  def standardize_property(self, re, property_name):
    transaction_type = None
    property_type = None
    try:
      property_name = property_name.strip()
      if "bán" in property_name:
        transaction_type = "bán"
        property_type = " ".join(property_name.split(" ")[1:])
      elif "cho thuê" in property_name:
        transaction_type = "cho thuê"
        property_type = " ".join(property_name.split(" ")[2:])
    except:
      pass

    re.transaction_type = transaction_type
    re.property_type = property_type.replace("căn hộ chung cư", "căn hộ")

  def standardize_area(self, re, total_site_area):
    try:
      area_str = total_site_area.lower().replace(",", ".").replace("m²", "").replace("m", "").strip()
      area = Utils.check_valid_number(area_str)
      re.area = area
    except:
      pass

  def standardize_price(self, re, price_no_vat):
    area = re.area
    try:
      list_price_unit = price_no_vat.strip().lower()\
        .replace("/m2", "/m²").replace(",", ".").split(" ")
      price_str = list_price_unit[0]
      unit = "".join(list_price_unit[1:])
      price = Utils.check_valid_number(price_str)
      price, unit = Utils.standardize_price(price, unit)
      price, price_unit = Utils.calculate_price_per_unit(price, unit, area)
      re.price = price
      re.price_unit = price_unit
    except:
      pass

  def extract_phone_number(self, re, r):
    try:
      if r[19]:
        phone = findall(regex_phone, r[19])[0]
        re.phone_number = phone
      elif r[20]:
        phone = findall(regex_phone, r[19])[0]
        re.phone_number = phone
    except:
      pass