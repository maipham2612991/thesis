from flask import Flask, jsonify, request
#from flask_pymongo import PyMongo
#from bson.objectid import ObjectId
from flask_cors import CORS
# from flair.models import TextClassifier
# from flair.data import Sentence
from flask import session
import pickle
import pandas as pd
import numpy as np

app = Flask(__name__)
app.secret_key = "super_secret_key"
# # app.config['SECRET_KEY'] = 'oh_so_secret'

# app.config['MONGO_DBNAME'] = 'exposeModel'
# app.config['MONGO_URI'] = 'mongodb://localhost:27017/exposeModel'
# mongo = PyMongo(app)

CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
CORS(app)
cors = CORS(app, resource={
    r"/*":{
        "origins":"*"
    }
})
#classifier = TextClassifier.load('en-sentiment')
regression_dat = pickle.load(open('final_model.sav', 'rb'))
regression_nha = pickle.load(open('final_model_house.sav', 'rb'))
regression_thue = pickle.load(open('final_model_rent.sav', 'rb'))

print('Loading done')

@app.route('/', methods=['GET', 'POST'])
def index():
    return jsonify("welcome to Arafa API")

@app.route('/api/result', methods=['GET', 'POST'])
def get_result():
    result = []
    # try:
    print('+++++++++++++++++++++++++++')
    print('++++++++++++++++++++++', session['my_result'])
    data_result = session['my_result']
    print('--------', data_result)
    result.append ({'info': data_result['info'],'prediction': data_result['res']})
    # except:
    #     result.append ({'info': [],'prediction': 'No result'})
    return jsonify(result)

# @app.route('/api/task', methods=['GET', 'POST'])
# def input_predict_text():
#     title = request.get_json()['title']

#     sentence = Sentence(title)
#     # # run classifier over sentence
#     #classifier.predict(sentence)
#     #extract text and its prediction
#     # text = sentence.to_plain_string()
#     # label = sentence.labels[0]
#     text = 'abc'
#     label = 'spam'
#     result = {'title' : text, 'tag' : label}
#     session['my_result'] = result
#     return jsonify(result)

@app.route('/api/process', methods=['GET', 'POST'])
def input_predict_text():
    ban = str(request.get_json()['ban']).lower()   
    picked = str(request.get_json()['picked']).lower()
    city = str(request.get_json()['city']).lower()
    province = str(request.get_json()['province']).lower()
    district = str(request.get_json()['district']).lower()
    ward = str(request.get_json()['ward']).lower()
    area = float(request.get_json()['area'])
    front = float(request.get_json()['front'])
    alley = float(request.get_json()['alley'])
    num_floors = int(request.get_json()['num_floors'])
    num_bedrooms = int(request.get_json()['num_bedrooms'])
    num_bathrooms = int(request.get_json()['num_bathrooms'])
    date = str(request.get_json()['date'])
    property_type = str(request.get_json()['property_type']).lower()

    print('Done - Received inputs')
    province = province.replace('thành phố ', '')

    if province in ['hồ chí minh', 'hà nội', 'đà nẵng', 'cần thơ', 'hải phòng']:
        district = city
        city = province
        province = ''

        lst = ['quận ', 'huyện ', 'thị xã ']
        for ele in lst:
            district = district.replace(ele, '')
    else:
        province = province.replace('tỉnh ', '')

        lst = ['huyện ', 'thị xã ', 'thành phố ']
        for ele in lst:
            city = city.replace(ele, '')

        
    if city == '':
        city = None
    if province == '':
        province = None
    if district == '':
        district = None
    if ward == '':
        ward = None

    if ban == 'ban' and picked == 'one':
        df = pd.DataFrame(columns=['area', 'property_type', 'addr_province', 'addr_city', 'addr_district',
        'addr_ward','alley', 'front', 'created_date' ])
        df = df.append({'addr_city':city, 'addr_district':district, 'addr_province' : province, 'addr_ward' : ward,  'area':area, 'front':front, 'alley':alley, 'created_date':date, 'property_type': property_type},  ignore_index = True)
    else:
        df = pd.DataFrame(columns=['area', 'property_type', 'addr_province', 'addr_city', 'addr_district',
        'addr_ward','alley', 'front', 'created_date', 'num_floors', 'num_bathrooms', 'num_bedrooms' ])
        df = df.append({'addr_city':city, 'addr_district':district, 'addr_province' : province, 'addr_ward' : ward,  'area':area, 'front':front, 'alley':alley, 'created_date':date, 'property_type': property_type, 'num_floors': num_floors, 'num_bathrooms': num_bathrooms, 'num_bedrooms': num_bedrooms},  ignore_index = True)

    print('Done - Created dataframe')

    df['created_date'] = pd.to_datetime(df['created_date'])
    df['created_date'] = df['created_date'].dt.strftime('%d.%m.%Y')

    df['year'] = pd.DatetimeIndex(df['created_date']).year
    df['month'] = pd.DatetimeIndex(df['created_date']).month
    #df['day'] = pd.DatetimeIndex(df['created_date']).day

    #df['dayofyear'] = pd.DatetimeIndex(df['created_date']).dayofyear
    df['weekofyear'] = pd.DatetimeIndex(df['created_date']).weekofyear
    df['weekday'] = pd.DatetimeIndex(df['created_date']).weekday
    df['quarter'] = pd.DatetimeIndex(df['created_date']).quarter
    df['is_month_start'] = pd.DatetimeIndex(df['created_date']).is_month_start
    df['is_month_end'] = pd.DatetimeIndex(df['created_date']).is_month_end
    df = df.drop(['created_date'], axis = 1)
    df.at[0, 'area'] = np.log(df.iloc[0]['area'] )
    df = df.replace({np.nan: None})
    df = df.replace({None: 'null'}).replace({'None': 'null'})

    # if picked == 'one':
    #     result = {'info':[df['property_type'][0], df['addr_ward'][0], str(df['addr_district'][0]), df['addr_province'][0], df['addr_city'][0]], 'res': 'aaaaa'}
    # else:
    #     result = {'info':[property_type, ward, district, province, city, area, front, alley, date, num_bathrooms, num_floors, num_bedrooms], 'res': 'aaaa'}
    # session['my_result'] = result

    print('Done - Convert datetime and area')

    lst = ['addr_province', 'addr_city', 'addr_district', 'addr_ward', 'property_type']

    if ban == 'ban':
        if picked == 'one':
            typed = 'dat_'
        else:
            typed = 'nha_'
    else:
        typed = 'thue_'

    try:
        for ele in lst:
            with open('encode_model/' + typed + ele + '.pkl', 'rb') as file:
                model = pickle.load(file)
                df[ele] = model.transform(df[ele].astype(str))
    except:
        #result = {'info': ['Your input'], 'res': str('Our models do not support your input - Please check all of your input again !')}
        result = str('Our models do not support your input - Please check all of your input again !')
        session['my_result'] = result

        print(session['my_result'])
        print('*********')
        return jsonify(result)
            
    attr_lst = [
    'property_type', 'year', 'month', 'weekofyear', 'weekday', 
    'quarter', 'is_month_start', 'is_month_end'
    ]

    with open('encode_model/' + typed + 'one_hot.pkl', 'rb') as file:
        model = pickle.load(file)
        df_encoded = model.transform(df[attr_lst]).todense()
        df_encoded = pd.DataFrame(df_encoded, index=df.index, columns=model.get_feature_names())
        df_other_cols = df.drop(columns=attr_lst, axis=1)
        df = pd.concat([df_other_cols, df_encoded], axis=1)
        
    print('Done - Convert address')
    print('++++++++++++++++++++++=', df.columns[10:20])
    df['alley'] = df['alley'].astype(float)
    df['front'] = df['front'].astype(float)

    if ban == 'ban':
        if picked == 'one':
            result = np.exp(regression_dat.predict(df)) * float(area)
        else:
            result = np.exp(regression_nha.predict(df)) * float(area)
    else:
        result = np.exp(regression_thue.predict(df)) * float(area)

    result = "{:,.0f}".format(result[0] * 1000000)

    str_money = read_money(result)

    result = str(result) + '  ---  ' + str(str_money)

    print('*******', result)

    # if picked == 'one':
    #     #result = {'info':[property_type, ward, district, province, city, area, front, alley, date], 'res': result}
    #     result = result
    # else:
    #     # result = {'info':[property_type, ward, district, province, city, area, front, alley, date, num_bathrooms, num_floors, num_bedrooms], 'res': result}
    #     result = result
    print(result)
    session['my_result'] = result
    # except:
    #     #result = {'info': ['Your input'], 'res': str('Our models do not support your input - Please check all of your input again !')}
    #     result = str('Our models do not support your input - Please check all of your input again !')
    #     session['my_result'] = result

    print(session['my_result'])
    print('*********')
    return jsonify(result)

def read_money(number):
    lst = number.split(',')
    lst_money = [' tỷ ', ' triệu ', ' ngàn ', ' VND']
    final_str = ''

    if len(lst) == 4:
        for i in range(len(lst)):
            temp = str(lst[i]) + lst_money[i]
            final_str += temp
    else:
        for i in range(len(lst)):
            temp = str(lst[i]) + lst_money[i + 1]
            final_str += temp

    return final_str
        

if __name__ == '__main__':
    #app.run(host='localhost', debug=True)
    app.run(host='0.0.0.0', port=5000)

