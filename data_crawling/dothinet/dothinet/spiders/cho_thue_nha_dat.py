# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from dothinet.settings import NUMS_PAGE_CHOTHUENHADAT


class ChoThueNhaDatSpider(scrapy.Spider):
    name = 'cho_thue_nha_dat'
    allowed_domains = ['dothi.net/nha-dat-cho-thue']
    start_urls = ['https://dothi.net/nha-dat-cho-thue.htm']

    def parse(self, response):
        yield self.parse_url(response)
        for i in range(1, NUMS_PAGE_CHOTHUENHADAT + 1):
            url = 'https://dothi.net/nha-dat-cho-thue/p' + str(i) + ".htm"
            yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)
        pass

    def parse_url(self, response):
        urls = response.xpath('//h3//a[@class="vip2"]/@href').extract()
        for url in urls:
            yield { "url": "".join(["https://dothi.net/", url]), "post_type": "nhà đất cho thuê",
                    "link_taken_on":  datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "is_crawled": False }
