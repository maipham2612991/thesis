# -*- coding: utf-8 -*-
import scrapy
from dothinet.settings import database, user, host, port, password
import psycopg2
from scrapy.http import Request
from datetime import datetime
from datetime import date, timedelta

VN_to_English = {
    "Giá:": "price_no_VAT",
    "Diện tích:": "total_site_area",
    "Loại tin rao": "property_name",
    "Số phòng": "number_of_bedrooms",
    "Số tầng": "number_of_floors",
    "Hướng nhà": "direction",
    "Đường vào": "route_length",
    "Mặt tiền": "front_length",
    "Số toilet": "number_of_bathrooms",
    "Ngày đăng tin": "created_datetime",
    "Ngày hết hạn": "expired_datetime",
    "Tên liên lạc": "post_author",
    "Di động": "phone_number",
    'Điện thoại': "phone_number",
    "Email": "email",
    #"Địa chỉ": "address_id"
}

def NextURL():
    connection = psycopg2.connect(user = user,
                                  password = password,
                                  host = host,
                                  port = port,
                                  database = database)

    cursor = connection.cursor()
    sql = """ SELECT url from dothi_links where is_crawled = false; """
    cursor.execute(sql)
    list_of_urls = cursor.fetchall()
    for next_url in list_of_urls:
        sql = """ UPDATE dothi_links SET is_crawled = TRUE
                    WHERE url = '{}' ;
             """.format(next_url[0])
        print(next_url)
        cursor.execute(sql)
        connection.commit()
        yield next_url


class DothiNetContentSpider(scrapy.Spider):
    name = 'dothi_net_content'
    url = NextURL()

    def start_requests(self):
        start_url = next(self.url)[0]
        yield Request(start_url, callback=self.parse, dont_filter=True)

    def parse(self, response):
        crawled_dict = dict()
        """ url dothi """
        crawled_dict['url'] = response.url


        """ post title dothi """
        try:
            crawled_dict['post_title'] = response.xpath("//div[@class='product-detail']//h1/text()").extract()[0].strip()
        except:
            pass


        """ price, area dothi """
        keys_vn = response.xpath("//div[@class='pd-price']/text()").extract()
        keys_vn = [k.strip() for k in keys_vn if k.strip()]
        values = response.xpath("//div[@class='pd-price']//span/text()").extract()
        values = [value.strip() for value in values if value.strip()]

        try:
            for k in keys_vn:
                k_en = VN_to_English.get(k)
                if k_en == 'price_no_VAT':
                    crawled_dict[k_en] = ''.join(values[0])
                elif k_en == 'total_site_area':
                    crawled_dict[k_en] = values[1]
        except:
            pass


        """ address id dothi """
        try:
            address_street = response.xpath("//div[@class='pd-location']//a/text()").extract()
            address_district_city = response.xpath("//div[@class='pd-location']/text()").extract()
            address = str(address_street[0]).strip() + str(address_district_city[2]).strip()
            crawled_dict["address_id"] = address.replace(' -', ',').replace('-', ',')
        except:
            pass

        """ direction, news_type, number_of_bedrooms, ... """
        keys_vn = response.xpath("//table[@id='tbl1']//tbody//tr//td//b/text()").extract()
        values = response.xpath("//table[@id='tbl1']//tbody//tr//td/text()").extract()
        for k, v in zip(keys_vn, values):
            k = k.strip()
            v = v.strip()
            if VN_to_English.get(k) and v:
                crawled_dict[VN_to_English.get(k)] = v


        """ post author, phone homedy """
        keys_vn = response.xpath("//table[@id='tbl2']//tr//td//b/text()").extract()
        values = response.xpath("//table[@id='tbl2']//tr//td/text()").extract()
        try:
            for k, v in zip(keys_vn, values):
                k = k.strip()
                v = v.strip()
                if VN_to_English.get(k) == 'email':
                    crawled_dict[VN_to_English.get(k)] = ''
                if VN_to_English.get(k) and v:
                    crawled_dict[VN_to_English.get(k)] = v
        except:
            pass

        """ full description """
        texts = response.xpath("//div[@class='pd-desc-content']/text()").extract()
        crawled_dict["full_description"] = [text.strip().replace('"', '').replace("'", "").replace("’", "") for text in texts if text.strip()]


        """ date_scrape """
        if 'created_datetime' not in crawled_dict:
            crawled_dict["created_datetime"] = date.today().strftime("%Y-%m-%d")

        yield crawled_dict

        next_url = next(self.url)[0]

        yield Request(next_url, callback=self.parse, dont_filter=True)