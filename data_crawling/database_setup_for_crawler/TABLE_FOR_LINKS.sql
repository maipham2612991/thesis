
CREATE TABLE bat_dong_san_links (
    url VARCHAR UNIQUE NOT NULL,
    post_type VARCHAR NOT NULL,
    link_taken_on TIMESTAMP,
    is_crawled BOOLEAN
);

CREATE TABLE cho_tot_links (
    url VARCHAR UNIQUE NOT NULL,
    post_type VARCHAR NOT NULL,
    link_taken_on TIMESTAMP,
    is_crawled BOOLEAN
);


CREATE TABLE nha_dat_247_links (
    url VARCHAR UNIQUE NOT NULL,
    post_type VARCHAR NOT NULL,
    link_taken_on TIMESTAMP,
    is_crawled BOOLEAN
);

CREATE TABLE propzy_links (
    url VARCHAR UNIQUE NOT NULL,
    post_type VARCHAR NOT NULL,
    link_taken_on TIMESTAMP,
    is_crawled BOOLEAN
);

CREATE TABLE homedy_links (
    url VARCHAR UNIQUE NOT NULL,
    post_type VARCHAR NOT NULL,
    link_taken_on TIMESTAMP,
    is_crawled BOOLEAN
);

CREATE TABLE dothi_links (
    url VARCHAR UNIQUE NOT NULL,
    post_type VARCHAR NOT NULL,
    link_taken_on TIMESTAMP,
    is_crawled BOOLEAN
);

CREATE TABLE alo_nha_dat_links (
    url VARCHAR UNIQUE NOT NULL,
    post_type VARCHAR NOT NULL,
    link_taken_on TIMESTAMP,
    is_crawled BOOLEAN
);

