# -*- coding: utf-8 -*-
import scrapy
from nhadat247com.settings import database, user, host, port, password
import psycopg2
from scrapy.http import Request
from datetime import datetime, timedelta, date
from pytz import timezone


VN_to_English = {
    "Giá:": "price_no_VAT",
    "Diện tích:": "total_site_area",
    "Loại tin rao": "property_name",
    "Hướng nhà": "direction",
    "Số phòng": "number_of_bedrooms",
    "Đường vào": "route_length",
    "Mặt tiền": "front_length",
    "Số tầng": "number_of_floors",
    "Số toilet": "number_of_bathrooms",
    "Điện thoại": "phone_number",
    "Email": "email",
    "property_code": "property_code",
    "created_datetime": "created_datetime"
}


def NextURL():
    connection = psycopg2.connect(user = user,
                                  password = password,
                                  host = host,
                                  port = port,
                                  database = database)

    cursor = connection.cursor()
    sql = """ SELECT url from nha_dat_247_links where is_crawled = false limit 10000; """
    # sql = """ SELECT url from nha_dat_247_links where url = 'https://nhadat247.com.vn/ban-nha-rieng/sieu-pham-mat-pho-bach-maiso-hoa-hau108m22t-mt4m23-ty-kinh-doanh-bat-chap-pr95430.html'
    #"""
    cursor.execute(sql)
    list_of_urls = cursor.fetchall()
    for next_url in list_of_urls:
        sql = """ UPDATE nha_dat_247_links SET is_crawled = TRUE
                    WHERE url = '{}' ;
             """.format(next_url[0])
        print(next_url)
        cursor.execute(sql)
        connection.commit()
        yield next_url


class Nhadat247ComContentSpider(scrapy.Spider):
    name = 'nhadat247_com_content'
    url = NextURL()

    def start_requests(self):
        start_url = next(self.url)[0]
        yield Request(start_url, callback=self.parse, dont_filter=True)

    def parse(self, response):
        crawled_dict = dict()
        """ url """
        crawled_dict['url'] = response.url

        """ post title """
        try:
            crawled_dict['post_title'] = response.xpath("//div[@class='content-left']//div//h1/text()").extract()[0].strip()
        except:
            pass
            

        """ price, area, property_code, created_date """
        keys_vn = response.xpath("//div[@class='pd-price']/text()").extract()
        values = response.xpath("//div[@class='pd-price']//span/text()").extract()
        try:
            values = values + [response.xpath("//div[@class='pd-price']//div/text()")[1].extract().replace("|", "").strip()]
        except:
            pass
        for k, v in zip([k.strip() for k in keys_vn if k.strip()] + ["property_code", "created_datetime"],
                        [v.strip() for v in values if v.strip()]):
            if VN_to_English.get(k):
                try:
                    datetime_obj = datetime.strptime(v, "%d/%m/%Y")
                    crawled_dict[VN_to_English.get(k)] = datetime_obj.strftime("%Y-%m-%d")
                except:
                    crawled_dict[VN_to_English.get(k)] = v
        #if crawled_dict.get("created_datetime"):
        try:
            print(crawled_dict.get("url"))
            if "hôm qua" in crawled_dict.get("created_datetime"):
                crawled_dict['created_datetime'] = (datetime.now(timezone('UTC')).astimezone(timezone('Asia/Ho_Chi_Minh')).date() - timedelta(days=1)).strftime("%Y-%m-%d")
            elif "hôm nay" in crawled_dict.get("created_datetime"):
                crawled_dict['created_datetime'] = (datetime.now(timezone('UTC')).astimezone(timezone('Asia/Ho_Chi_Minh')).date()).strftime("%Y-%m-%d")
            elif "hôm kia" in crawled_dict.get("created_datetime"):
                crawled_dict['created_datetime'] = (datetime.now(timezone('UTC')).astimezone(timezone('Asia/Ho_Chi_Minh')).date() - timedelta(days=2)).strftime("%Y-%m-%d")
        except:
            pass

        """ address id """
        try:
            address_info = response.xpath("//div[@class='pd-location']//a/text()").extract()
            crawled_dict["address_id"] = ", ".join([addr.strip() for addr in address_info if addr.strip()])
        except:
            pass

        """ direction, news_type, number_of_bedrooms, ... """
        keys_vn = response.xpath("//table[@id='tbl1']//tbody//tr//td//b/text()").extract()
        values = response.xpath("//table[@id='tbl1']//tbody//tr//td/text()").extract()
        for k, v in zip(keys_vn, values):
            k = k.strip()
            v = v.strip()
            if VN_to_English.get(k) and v:
                crawled_dict[VN_to_English.get(k)] = v

        """ post author """
        keys_vn = response.xpath("//table[@id='tbl2']//tr//td//b/text()").extract()
        values = response.xpath("//table[@id='tbl2']//tr//td/text()").extract()
        try:
            crawled_dict['post_author'] = keys_vn[1].strip()
            for k, v in zip(keys_vn[2:], values[-4:]):
                k = k.strip()
                v = v.strip()
                if VN_to_English.get(k) and v:
                    crawled_dict[VN_to_English.get(k)] = v

        except:
            pass

        """ full description """
        texts = response.xpath("//div[@class='pd-desc-content']/text()").extract()
        crawled_dict["full_description"] = [text.strip().replace('"', '').replace("'", "").replace("’", "") for text in texts if text.strip()]

        """ get phone number """
        import re
        try:
            crawled_dict["phone_number"] = re.findall("\d{10}", " ".join(texts))[0]

        except:
            pass

        """ date_scrape """
        from datetime import date
        crawled_dict["updated_datetime"] = date.today().strftime("%Y-%m-%d")
        print(crawled_dict)

        yield crawled_dict

        next_url = next(self.url)[0]

        yield Request(next_url, callback=self.parse, dont_filter=True)
