# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from nhadat247com.settings import NUMS_PAGE_NHADATBAN


class NhaDatBanSpider(scrapy.Spider):
    name = 'nha_dat_ban'
    allowed_domains = ['nhadat247.com.vn/nha-dat-ban']
    start_urls = ['https://nhadat247.com.vn/nha-dat-ban/']

    def parse(self, response):
        yield self.parse_url(response)
        for i in range(1, NUMS_PAGE_NHADATBAN +1):
            url = self.start_urls[0] + str(i)
            yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)
        pass

    def parse_url(self, response):
        urls = response.xpath("//div[@class='desc']//h4//a/@href").extract()
        for url in urls:
            yield { "url": "".join(["https://nhadat247.com.vn", url]), "post_type": "nhà đất bán",
                    "link_taken_on":  datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "is_crawled": False }
