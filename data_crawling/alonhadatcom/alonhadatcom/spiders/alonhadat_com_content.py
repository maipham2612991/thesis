# -*- coding: utf-8 -*-
import scrapy
from alonhadatcom.settings import database, user, host, port, password
import psycopg2
from scrapy.http import Request
from datetime import datetime, timedelta, date
from pytz import timezone


VN_to_English = {
    "Giá:": "price_no_VAT",
    "Diện tích:": "total_site_area",
    "Địa chỉ tài sản:": "address_id",
    "Mã tin": "property_code",
    "Hướng": "direction",
    "Loại tin": "property_name",
    "Đường trước nhà": "front_length",
    "Loại BDS": "property_name",
    "Pháp lý": "legal_info",
    "Số lầu": "number_of_floors",
    "Số phòng ngủ": "number_of_bedrooms",
    "Thuộc dự án": "project_id"
}

INTERNAL_FACILITY = [
    "Phòng ăn", "Nhà bếp",
    "Sân thượng", "Chổ để xe hơi"
]


def NextURL():
    connection = psycopg2.connect(user = user,
                                  password = password,
                                  host = host,
                                  port = port,
                                  database = database)

    cursor = connection.cursor()
    sql = """ SELECT url from alo_nha_dat_links where is_crawled = false"""
    cursor.execute(sql)
    list_of_urls = cursor.fetchall()
    for next_url in list_of_urls:
        sql = """ UPDATE alo_nha_dat_links SET is_crawled = TRUE
                    WHERE url = '{}' ;
             """.format(next_url[0])
        print(next_url)
        cursor.execute(sql)
        connection.commit()
        yield next_url


class AlonhadatComContentSpider(scrapy.Spider):
    name = 'alonhadat_com_content'
    url = NextURL()

    def start_requests(self):
        start_url = next(self.url)[0]
        try:
            yield Request(start_url, callback=self.parse, dont_filter=True)
        except:
            pass

    def parse(self, response):
        crawled_dict = dict()
        """ url """
        crawled_dict['url'] = response.url

        """ post title """
        try:
            crawled_dict['post_title'] = response.xpath("//div[@class='property']//div//h1/text()").extract()[0].strip()
        except:
            pass


        """ price, area, address_id """
        keys_vn = response.xpath("//div//span[@class='label']/text()").extract()[::-1]
        values = response.xpath("//div//span[@class='value']/text()").extract()[::-1]
        if len(keys_vn) != len(values):
            # values[2:4] = [''.join(values[2 : 4])]
            values[2] = values[3] + values[2]
        for k, v in zip([k.strip() for k in keys_vn if k.strip()], [v.strip() for v in values if v.strip()]):
            if VN_to_English.get(k):
                crawled_dict[VN_to_English.get(k)] = v

        try:
            created_date = response.xpath("//div//span[@class='date']/text()").extract()[0].strip().split(":")
            crawled_dict['created_datetime'] = created_date[1].lower()
        except:
            pass

        if crawled_dict.get("created_datetime"):
            try:
                if "hôm qua" in crawled_dict.get("created_datetime"):
                    crawled_dict['created_datetime'] = (datetime.now(timezone('UTC')).astimezone(timezone('Asia/Ho_Chi_Minh')).date() - timedelta(days=1)).strftime("%Y-%m-%d")
                elif "hôm nay" in crawled_dict.get("created_datetime"):
                    crawled_dict['created_datetime'] = (datetime.now(timezone('UTC')).astimezone(timezone('Asia/Ho_Chi_Minh')).date()).strftime("%Y-%m-%d")
                elif "hôm kia" in crawled_dict.get("created_datetime"):
                    crawled_dict['created_datetime'] = (datetime.now(timezone('UTC')).astimezone(timezone('Asia/Ho_Chi_Minh')).date() - timedelta(days=2)).strftime("%Y-%m-%d")
                else:
                    crawled_dict['created_datetime'] = datetime.strptime(v, "%d/%m/%Y").strftime("%Y-%m-%d")
            except:
                pass

        """ direction, news_type, number_of_bedrooms, ... """
        crawled_dict["property_name"] = ""
        crawled_dict["internal_facility"] = []

        for tr in range(1, 6):
            for td in range(1, 7, 2):
                xpath_key = f"//div[@class='infor']//table//tr[{tr}]//td[{td}]/text()"
                xpath_value = f"//div[@class='infor']//table//tr[{tr}]//td[{td+1}]/text()"
                try:
                    key_vn = response.xpath(xpath_key).extract()[0]
                    value = response.xpath(xpath_value).extract()[0]
                    key = VN_to_English.get(key_vn)

                    if key == "property_name":
                        crawled_dict[key] =  crawled_dict[key] + " " + value
                    elif key:
                        crawled_dict[key] = value
                    elif key in INTERNAL_FACILITY and "-" not in value and "_" not in value:
                            crawled_dict["internal_facility"] += [value]
                    else:
                        continue
                except:
                    continue
        if not crawled_dict["internal_facility"]:
            crawled_dict.pop("internal_facility")
        # all_keys_values = response.xpath("//div[@class='infor']//table//tr//td/text()").extract()
        # filter_key_value = [kv for kv in all_keys_values if "_" not in kv and "-" not in kv]
        # for kv in all_keys_values:
            
        # keys_vn = response.xpath("//table[@id='tbl1']//tbody//tr//td//b/text()").extract()
        # values = response.xpath("//table[@id='tbl1']//tbody//tr//td/text()").extract()
        # for k, v in zip(keys_vn, values):
        #     k = k.strip()
        #     v = v.strip()
        #     if VN_to_English.get(k) and v:
        #         crawled_dict[VN_to_English.get(k)] = v

        """ post author """
        try:
            crawled_dict["post_author"] = response.xpath("//div[@class='name']/text()").extract()[0].strip()
        except:
            pass

        """ project """
        try:
            crawled_dict["project_id"] = response.xpath("//span[@class='project']//a/text()").extract()[0].strip()
        except:
            pass

        """ full description """
        texts = response.xpath("//div[@class='detail text-content']/text()").extract()
        crawled_dict["full_description"] = [text.strip().replace('"', '').replace("'", "").replace("’", "") for text in texts if text.strip()]

        """ get phone number """
        import re
        try:
            crawled_dict["phone_number"] = re.findall("\d{10}", " ".join(texts))[0]
        except:
            pass
        if not crawled_dict.get("phone_number"):
            try:
                crawled_dict["phone_number"] = response.xpath("//div[@class='fone']//a/text()").extract()[0].strip()
            except:
                pass

        """ date_scrape """
        from datetime import date
        crawled_dict["updated_datetime"] = date.today().strftime("%Y-%m-%d")
        print(crawled_dict)

        yield crawled_dict

        next_url = next(self.url)[0]

        yield Request(next_url, callback=self.parse, dont_filter=True)
