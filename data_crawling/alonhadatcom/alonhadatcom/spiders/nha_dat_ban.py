# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from alonhadatcom.settings import NUMS_PAGE_NHADATBAN
# import urlparse


class NhaDatBanSpider(scrapy.Spider):
    name = 'nha_dat_ban'
    allowed_domains = ['alonhadat.com.vn/nha-dat/can-ban']
    start_urls = ['https://alonhadat.com.vn/nha-dat/can-ban.html']

    def parse(self, response):
        yield self.parse_url(response)
        for i in range(1, NUMS_PAGE_NHADATBAN +1):
            page = "/trang--{}.html".format(i)
            url = self.start_urls[0].replace(".html", page)
            yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)

    def parse_url(self, response):
        urls = response.xpath("//div[@class='ct_title']//a/@href").extract()
        for url in urls:
            yield { "url": "".join(["https://alonhadat.com.vn", url]), "post_type": "nhà đất bán",
                    "link_taken_on":  datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "is_crawled": False }
