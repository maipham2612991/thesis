# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import psycopg2
from nhachototcom.settings import database, user, password, host, port
import nhachototcom


class NhachototcomPipeline(object):
    def __init__(self, database=database, user=user, password=password, host=host, port=port):
        self.connection = psycopg2.connect(user=user, password=password, host=host,
                                           port=port, database=database)

        self.cursor = self.connection.cursor()

    def process_item(self, item, spider):
        if not isinstance(spider, nhachototcom.spiders.chotot_com_content.ChoTotComContentSpider):
            sql =   """INSERT INTO cho_tot_links (url, post_type, link_taken_on, is_crawled )
                    VALUES ( '{}', '{}', '{}', {} )
                    ON CONFLICT DO NOTHING ;
            """.format(item.get("url"), item.get("post_type"), item.get("link_taken_on"), item.get("is_crawled"))

            try:
                # print(sql)
                self.cursor.execute(sql)
                self.connection.commit()
            except:
                pass
            return item
        else:
            item_keys = item.keys()
            sql = """ INSERT INTO raw_chotot ( {} )
                    VALUES ( {} ) ON CONFLICT DO NOTHING ;
            """.format(", ".join(item_keys), " , ".join(["'"+str(item.get(i)).replace("'", "").replace('"', '').replace("’", "")+"'"
                                                                           if type(item.get(i)) is not list
                                                                           else "ARRAY"+str(item.get(i))
                                                         for i in item_keys]))
            print(sql)
            try:
                self.cursor.execute(sql)
                self.connection.commit()
            except:
                self.connection.rollback()

            # sql = """ UPDATE homedy_links SET is_crawled = TRUE """
            #         WHERE url = '{}' ;
            #  """.format(item.get('url'))
            #
            # self.cursor.execute(sql)
            # self.connection.commit()

            return item
