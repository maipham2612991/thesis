# -*- coding: utf-8 -*-
import scrapy
from nhachototcom.settings import database, user, host, port, password
import psycopg2
from scrapy.http import Request
from datetime import datetime, timedelta, date
from pytz import timezone
from scrapy import Selector

VN_to_English = {
    # "Giá:": "price_no_VAT",
    # "Diện tích:": "total_site_area",
    # "Loại tin rao": "property_name",
    "Hướng cửa chính": "direction",
    "Hướng đất": "direction",
    # "Đường vào": "route_length",
    # "Mặt tiền": "front_length",
    # "Số tầng": "number_of_floors",
    # "Điện thoại": "phone_number",
    # "Email": "email",
    # "property_code": "property_code",
    # "created_datetime": "created_datetime",
    "Giấy tờ pháp lý": "legal_info",
    "Số phòng ngủ": "number_of_bedrooms",
    "Số phòng vệ sinh": "number_of_bathrooms",
    "Tầng số": "floor",
    "Dự Án": "project_id",
    "Loại hình đất": "property_name",
    "Loại hình căn hộ": "property_name",
    "Loại hình nhà ở": "property_name",
    "Loại hình văn phòng": "property_name"
}

property_type = {
    'Đất': 'đất',
    'Căn hộ/Chung cư': 'căn hộ',
    'Nhà ở': 'nhà riêng',
    'Văn phòng, Mặt bằng kinh doanh': 'mặt bằng kinh doanh',
    'Thuê Nhà ở': 'nhà riêng',
    'Thuê Phòng trọ': 'nhà trọ, phòng trọ',
    'Phòng trọ': 'nhà trọ, phòng trọ'
}


def NextURL():
    connection = psycopg2.connect(user = user,
                                  password = password,
                                  host = host,
                                  port = port,
                                  database = database)

    cursor = connection.cursor()
    sql = """ SELECT url from cho_tot_links where is_crawled = false limit 10000; """
    # sql = """ SELECT url from nha_dat_247_links where url = 'https://nhadat247.com.vn/ban-nha-rieng/sieu-pham-mat-pho-bach-maiso-hoa-hau108m22t-mt4m23-ty-kinh-doanh-bat-chap-pr95430.html'
    #"""
    cursor.execute(sql)
    list_of_urls = cursor.fetchall()
    for next_url in list_of_urls:
        sql = """ UPDATE cho_tot_links SET is_crawled = TRUE
                    WHERE url = '{}' ;
             """.format(next_url[0])
        print(next_url)
        cursor.execute(sql)
        connection.commit()
        yield next_url


class ChoTotComContentSpider(scrapy.Spider):
    name = 'chotot_com_content'
    url = NextURL()

    def start_requests(self):
        start_url = next(self.url)[0]
        yield Request(start_url, callback=self.parse, dont_filter=True)

    def parse(self, response):
        crawled_dict = dict()
        """ url """
        crawled_dict['url'] = response.url

        """ post title """
        try:
            crawled_dict['post_title'] = response.xpath("//h1[@class='adTilte___3UqYW']/text()").extract()[1].strip()
        except:
            pass
            

        """ price """
        try:
            crawled_dict['price_no_VAT'] = response.xpath("//span[@itemprop='price']/text()").extract()[0].strip()
        except:
            pass

        """ area """
        try:
            values = response.xpath("//span[@class='squareMetre___5_gjS']/text()").extract()
            values = [value.strip() for value in values if value.strip()]
            crawled_dict["total_site_area"] = " ".join(values[1:3]) + " " + response.xpath("//span[@class='squareMetre___5_gjS']/sup/text()").extract()[0].strip()
        except:
            pass

        """ created_date """
        try:
            date_created_str = response.xpath("//div[@class='imageCaption___cMU2J']/span/text()").extract()[3].strip()
            number = 0
            num_days = 0
            for s in date_created_str.split(" "):
                if s.isdigit():
                    number = int(s)
                elif s == "ngày":
                    num_days = number
                elif s == "tuần":
                    num_days = 7*number
                elif s == "tháng":
                    num_days = 30*number
                else:
                    num_days = 0
            crawled_dict['created_datetime'] = (datetime.now(timezone('UTC')).astimezone(timezone('Asia/Ho_Chi_Minh')).date() - timedelta(days=num_days)).strftime("%Y-%m-%d")
        except:
            pass

        """ property_code """

        """ address id """
        try:
            crawled_dict["address_id"] = response.xpath("//span[@class='fz13']/text()").extract()[0].strip()
        except:
            pass

        """ number_of_bedrooms, number_of_bathrooms, floor, project_id """
        # keys_vn = response.xpath("//div[@class='media-body media-middle']/span/span[1]/text").extract()
        # values = response.xpath("//span[@class='adParamValue___25KeI']/text()").extract()
        # if len(keys_vn) != len(values):
        #     values = values[1:] 
        # for k, v in zip(keys_vn, values):
        #     k = k.strip().replace(":","")
        #     v = v.strip()
        #     if k == "Số phòng ngủ":
        #         v = v.split(" ")[0]
        #     elif k == "Số phòng vệ sinh":
        #         v = v.split(" ")[0]
        #     if VN_to_English.get(k) and v:
        #         crawled_dict[VN_to_English.get(k)] = v

        properties = response.xpath("//div[@class='media-body media-middle']/span").extract()
        for i in properties:
            # print(i)
            sel = Selector(text=i)
            k = sel.xpath("//span/span[1]/text()").extract()
            if k:
                k = k[0].strip().replace(":","")
                v = None
                if k == "Dự Án":
                    try:
                        v = sel.xpath("//span/span[2]/a/text()").extract()[0].strip()
                    except:
                        pass
                else:
                    try:
                        v = sel.xpath("//span/span[2]/text()").extract()[0].strip()
                        if k == "Số phòng ngủ" or k == "Số phòng vệ sinh":
                            v = v.split(" ")[0]
                            v = v if v.isdigit() else None
                    except:
                        pass
            else:
                k = ""
            if VN_to_English.get(k) and v:
                crawled_dict[VN_to_English.get(k)] = v

        """property_name"""
        if not crawled_dict.get('property_name'):
            try:
                property_key = response.xpath("//span[@itemprop='name']/text()").extract()[3].strip()
                if property_type.get(property_key) and property_key:
                    crawled_dict['property_name'] = property_type.get(property_key)
            except:
                pass
        """ post author """
        try:
            crawled_dict['post_author'] = response.xpath("//div[@class='nameDiv___2aJ46']/b/text()").extract()[0].strip()
        except:
            pass

        """ full description """
        try:
            crawled_dict["full_description"] = [response.xpath("//p[@class='adBody___ev-xe']/text()").extract()[0].replace("\n"," ")]
        except:
            pass
        """ get phone number """
        try:
            crawled_dict["phone_number"] = response.xpath("//a[@class='btn buttonSuccess___2-DmW']/@href").extract()[0].split(":")[1]
        except:
            pass

        """ date_scrape """
        from datetime import date
        crawled_dict["updated_datetime"] = date.today().strftime("%Y-%m-%d")
        # print(crawled_dict)

        yield crawled_dict

        next_url = next(self.url)[0]

        yield Request(next_url, callback=self.parse, dont_filter=True)
