# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from nhachototcom.settings import NUMS_PAGE_CHOTOTBAN


class ChoTotBanSpider(scrapy.Spider):
    name = 'cho_tot_ban'
    allowed_domains = ['nha.chotot.com/toan-quoc/mua-ban-bat-dong-san']
    start_urls = ['https://nha.chotot.com/toan-quoc/mua-ban-bat-dong-san']

    def parse(self, response):
        yield self.parse_url(response)
        for i in range(1, NUMS_PAGE_CHOTOTBAN + 1):
            url = self.start_urls[0] + '?page=' + str(i)
            yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)
        pass

    def parse_url(self, response):
        urls = response.xpath("//a[@class='adItem___2GCVQ']/@href").extract()
        for url in urls:
            # print(url)
            url = url.replace('[object Object]','mua-ban')
            yield { "url": "".join(["https://nha.chotot.com", url]), "post_type": "nhà đất bán",
                    "link_taken_on":  datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "is_crawled": False }
