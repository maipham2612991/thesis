import psycopg2
from db_config import *


try:
    connection = psycopg2.connect(user = user,
                                  password = password,
                                  host = host,
                                  port = port,
                                  database = database)

    cursor = connection.cursor()

    # Print PostgreSQL version
    cursor.execute("SELECT version();")
    record = cursor.fetchone()
    print("You are connected to - ", record,"\n")

    with open("database_setup_for_crawler/TABLE_FOR_LINKS.sql", "r") as sqlFile:
        sql = sqlFile.read()

    cursor.execute(sql)
    connection.commit()

    with open("database_setup_for_crawler/TABLE_FOR_REALESTATE_CONTENT.sql", "r") as sqlFile:
        sql = sqlFile.read()

    cursor.execute(sql)
    connection.commit()


    cursor.execute(sql)
    connection.commit()

except (Exception, psycopg2.Error) as error :
    print ("Error while connecting to PostgreSQL", error)

# finally:
#     #closing database connection.
#     if(connection):
#         cursor.close()
#         connection.close()
#         print("PostgreSQL connection is closed")