# -*- coding: utf-8 -*-
import scrapy
from datetime import date
from batdongsancom.settings import NUMS_PAGE_DUAN


class DuAnSpider(scrapy.Spider):
    name = 'du_an'
    allowed_domains = ['batdongsan.com.vn/can-ho-chung-cu']
    start_urls = ['http://batdongsan.com.vn/can-ho-chung-cu/']
    next_urls = ['https://batdongsan.com.vn/cao-oc-van-phong', 'https://batdongsan.com.vn/trung-tam-thuong-mai',
                 'https://batdongsan.com.vn/khu-do-thi-moi', 'https://batdongsan.com.vn/khu-phuc-hop',
                 'https://batdongsan.com.vn/nha-o-xa-hoi', 'https://batdongsan.com.vn/khu-nghi-duong-sinh-thai',
                 'https://batdongsan.com.vn/khu-cong-nghiep', 'https://batdongsan.com.vn/du-an-khac',
                 'https://batdongsan.com.vn/biet-thu-lien-ke']

    def parse(self, response):
        yield self.parse_url(response)
        for i in range(2, NUMS_PAGE_DUAN + 1):
            url = self.start_urls[0] + "p{}".format(i)
            yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)

        for start_url in self.next_urls:
            yield scrapy.Request(url=start_url, callback=self.parse_url, dont_filter=True)
            for i in range(2, NUMS_PAGE_DUAN + 1):
                url = start_url + "/p{}".format(i)
                yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)

    def parse_url(self, response):
        urls = response.xpath("//div[@class='bigfont']//h3//a/@href").extract()
        for url in urls:
            yield { "url": "".join(["https://batdongsan.com.vn", url]), "post_type": "dự án",
                    "link_taken_on":  date.today().strftime("%Y-%m-%d"), "is_crawled": False }
