# -*- coding: utf-8 -*-
import scrapy
from batdongsancom.settings import database, user, host, port, password
import psycopg2
from scrapy.http import Request
import json


with open("./batdongsancom/xpath_dict.json", "r") as file:
    xpath_dict = json.load(file)

VN_to_English = {
    "Mức giá:": "price_no_VAT",
    "Diện tích:": "total_site_area",
    "Loại tin đăng:": "property_name",
    "Địa chỉ:": "address_id",
    "Số phòng ngủ:": "number_of_bedrooms",
    "Số toilet:": "number_of_bathrooms",
    "Tên dự án:": "project_id",
    "Quy mô:": "project_size",
    "Ngày đăng:": "created_datetime",
    "Ngày hết hạn:": "expired_datetime",
    "Mã tin:": "property_code",
    "Địa chỉ": "address_id",
    "Loại tin rao": "property_name",
    "Ngày đăng tin": "created_datetime",
    "Ngày hết hạn": "expired_datetime",
    "Tên liên lạc": "post_author",
    "Điện thoại": "phone_number",
    "Mobile": "phone_number",
    "Email": "email",
    "Pháp lý:": "legal_info",
    "Số tầng:": "number_of_floors",
    "Hướng nhà:": "direction",
    "Mặt tiền:": "front_length",
    "Đường vào:": "route_length",
}


def NextURL():
    connection = psycopg2.connect(user = user,
                                  password = password,
                                  host = host,
                                  port = port,
                                  database = database)

    cursor = connection.cursor()
    sql = """ SELECT url, post_type from bat_dong_san_links where post_type != 'dự án' and is_crawled = false; """
    cursor.execute(sql)
    list_of_urls = cursor.fetchall()
    for next_url in list_of_urls:
        sql = """ UPDATE bat_dong_san_links SET is_crawled = TRUE
                    WHERE url = '{}' ;
             """.format(next_url[0])
        print(next_url)
        cursor.execute(sql)
        connection.commit()
        yield next_url


class BatdongsanComContentSpider(scrapy.Spider):
    name = 'batdongsan_com_content'
    url = NextURL()

    def start_requests(self):
        start_url, post_type = next(self.url)
        # request = Request(start_url, dont_filter=True)
        if post_type != "cần mua cần thuê":
            request = Request(start_url, callback=self.parse_mua_thue, dont_filter=True)
        else:
            request = Request(start_url, callback=self.parse_can_mua_thue, dont_filter=True)

        yield request

    def parse_mua_thue(self, response):
        crawled_dict = dict()
        """ url """
        crawled_dict['url'] = response.url

        """ post title """
        try:
            crawled_dict['post_title'] = response.xpath(xpath_dict.get("mua_thue_post_title")).extract()[0].strip()
        except:
            pass

        """ price, area, rooms: characteristics1"""
        keys_vn = response.xpath(xpath_dict.get("mua_thue_characteristic1_keys")).extract()
        values = response.xpath(xpath_dict.get("mua_thue_characteristic1_values")).extract()
        for k, v in zip([k.strip() for k in keys_vn if k.strip()], [v.strip() for v in values if v.strip()]):
            if VN_to_English.get(k):
                crawled_dict[VN_to_English.get(k)] = v

        """ project, news_type, address, number_of_bedrooms, ...: characteristics2 """
        keys_vn = response.xpath(xpath_dict.get("mua_thue_characteristic2_keys")).extract()
        values = response.xpath(xpath_dict.get("mua_thue_characteristic2_values")).extract()
        for k, v in zip([k.strip() for k in keys_vn if k.strip()], [v.strip() for v in values if v.strip()]):
            if VN_to_English.get(k):
                crawled_dict[VN_to_English.get(k)] = v

        """ post_author """
        try:
            crawled_dict['post_author'] = response.xpath(xpath_dict.get("mua_thue_post_author")).extract()[0].strip()
        except:
            pass

        """ posted_day, expire_day: post_day """
        from datetime import datetime
        keys_vn = response.xpath(xpath_dict.get("mua_thue_post_day_keys")).extract()
        values = response.xpath(xpath_dict.get("mua_thue_post_day_values")).extract()
        for k, v in zip([k.strip() for k in keys_vn if k.strip()], [v.strip() for v in values if v.strip()]):
            if VN_to_English.get(k):
                try:
                    datetime_obj = datetime.strptime(v, "%d/%m/%Y")
                    crawled_dict[VN_to_English.get(k)] = datetime_obj.strftime("%Y-%m-%d")
                except:
                    crawled_dict[VN_to_English.get(k)] = v

        """ full description """
        texts = response.xpath(xpath_dict.get("mua_thue_full_description")).extract()
        crawled_dict["full_description"] = [text.strip().replace('"', '').replace("'", "").replace("’", "") for text in texts if text.strip()]

        """ get phone number """
        import re
        try:
            crawled_dict["phone_number"] = re.findall("\d{10}", " ".join(texts))[0]

        except:
            pass

        """ date_scrape """
        from datetime import date
        crawled_dict["updated_datetime"] = date.today().strftime("%Y-%m-%d")

        yield crawled_dict

        next_url, next_post_type = next(self.url)
        if next_post_type != "cần mua cần thuê":
            request = Request(next_url, callback=self.parse_mua_thue, dont_filter=True)
        else:
            request = Request(next_url, callback=self.parse_can_mua_thue, dont_filter=True)

        yield request

    def parse_can_mua_thue(self, response):
        crawled_dict = dict()

        """ url """
        crawled_dict['url'] = response.url

        """ post title """
        crawled_dict['post_title'] = response.xpath(xpath_dict.get("post_title")).extract()[0].strip()

        """ price """
        crawled_dict['price_no_VAT'] = response.xpath(xpath_dict.get("can_mua_thue_price")).extract()[0].strip()

        """ area """
        crawled_dict['total_site_area'] = response.xpath(xpath_dict.get("can_mua_thue_area")).extract()[0].strip()

        """ real estate characteristic """
        from datetime import datetime
        keys_vn = response.xpath(xpath_dict.get("can_mua_thue_characteristics_keys")).extract()
        values = response.xpath(xpath_dict.get("can_mua_thue_characteristics_values")).extract()
        for k, v in zip([k.strip() for k in keys_vn[::-1] if k.strip()], [v.strip() for v in values[::-1] if v.strip()]):
            if VN_to_English.get(k):
                try:
                    datetime_obj = datetime.strptime(v, "%d-%m-%Y")
                    crawled_dict[VN_to_English.get(k)] = datetime_obj.strftime("%Y-%m-%d")
                except:
                    crawled_dict[VN_to_English.get(k)] = v

        if not crawled_dict.get("project_id"):
            try:
                crawled_dict['project_id'] = response.xpath(xpath_dict.get("can_mua_thue_project_id")).extract()[0].strip()
            except:
                pass

        """ user post info """
        keys_vn = response.xpath(xpath_dict.get("can_mua_thue_user_info_keys")).extract()
        values = response.xpath(xpath_dict.get("can_mua_thue_user_info_values")).extract()
        for k, v in zip([k.strip() for k in keys_vn if k.strip()], [v.strip() for v in values if v.strip()]):
            if VN_to_English.get(k) and VN_to_English.get(k) != "address_id":
                crawled_dict[VN_to_English.get(k)] = v

        """ property name """
        crawled_dict['property_name'] = response.xpath(xpath_dict.get("can_mua_thue_property_name")).extract()[0].strip()

        """ full description """
        texts = response.xpath(xpath_dict.get("can_mua_thue_full_description")).extract()
        crawled_dict["full_description"] = [text.strip().replace('"', '').replace("'", "").replace("’", "") for text in texts if text.strip()]

        """ date_scrape """
        from datetime import date
        crawled_dict["updated_datetime"] = date.today().strftime("%Y-%m-%d")

        yield crawled_dict

        next_url, next_post_type = next(self.url)
        if next_post_type != "cần mua cần thuê":
            request = Request(next_url, callback=self.parse_mua_thue, dont_filter=True)
        else:
            request = Request(next_url, callback=self.parse_can_mua_thue, dont_filter=True)

        yield request

