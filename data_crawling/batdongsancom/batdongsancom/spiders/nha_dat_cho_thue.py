# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from batdongsancom.settings import NUMS_PAGE_NHADATCHOTHUE


class NhaDatChoThueSpider(scrapy.Spider):
    name = 'nha_dat_cho_thue'
    allowed_domains = ['batdongsan.com.vn/nha-dat-cho-thue']
    start_urls = ['https://batdongsan.com.vn/nha-dat-cho-thue/']

    def parse(self, response):
        yield self.parse_url(response)
        for i in range(1, NUMS_PAGE_NHADATCHOTHUE + 1):
            url = self.start_urls[0] + "p{}".format(i)
            yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)

    def parse_url(self, response):
        urls = response.xpath("//div[@class='vip5 vipaddon product-item clearfix']//a/@href").extract()
        for url in urls:
            a = { "url": ''.join(["https://batdongsan.com.vn", url]), "post_type": "nhà đất cho thuê",
                    "link_taken_on":  datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "is_crawled": False }
            print(a)
            yield a

