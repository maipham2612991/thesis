# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from batdongsancom.settings import NUMS_PAGE_CANMUACANTHUE


class CanMuaCanThueSpider(scrapy.Spider):
    name = 'can_mua_can_thue'
    allowed_domains = ['batdongsan.com.vn/can-mua-can-thue']
    start_urls = ['https://batdongsan.com.vn/can-mua-can-thue/']

    def parse(self, response):
        yield self.parse_url(response)
        for i in range(1, NUMS_PAGE_CANMUACANTHUE+1):
            url = self.start_urls[0] + "p{}".format(i)
            yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)
        pass

    def parse_url(self, response):
        urls = response.xpath("//div[@class='p-title']//a/@href").extract()
        for url in urls:
            yield { "url": "".join(["https://batdongsan.com.vn", url]), "post_type": "cần mua cần thuê",
                    "link_taken_on": datetime.now().strftime("%Y-%m-%d %H:%M:%S") , "is_crawled": False }
