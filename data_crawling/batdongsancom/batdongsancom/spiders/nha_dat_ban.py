# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from batdongsancom.settings import NUMS_PAGE_NHADATBAN
# import urlparse


class NhaDatBanSpider(scrapy.Spider):
    name = 'nha_dat_ban'
    allowed_domains = ['batdongsan.com.vn/nha-dat-ban']
    start_urls = ['https://batdongsan.com.vn/nha-dat-ban/']

    def parse(self, response):
        yield self.parse_url(response)
        for i in range(1, NUMS_PAGE_NHADATBAN +1):
            url = self.start_urls[0] + "p{}".format(i)
            yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)

    def parse_url(self, response):
        urls = response.xpath("//div[@class='vip0 vipaddon product-item clearfix']//a/@href").extract()
        for url in urls:
            yield { "url": "".join(["https://batdongsan.com.vn", url]), "post_type": "nhà đất bán",
                    "link_taken_on":  datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "is_crawled": False }
