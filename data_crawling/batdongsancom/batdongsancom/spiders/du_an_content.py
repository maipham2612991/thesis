# -*- coding: utf-8 -*-
import scrapy
from batdongsancom.settings import database, user, host, port, password
import psycopg2
from scrapy.http import Request


VN_to_English = {
    "Tên dự án": "project_name",
    "Địa chỉ": "project_addr",
    "Loại hình phát triển": "project_type",
    "Quy mô dự án": "project_scale",
}

def NextURL():
    connection = psycopg2.connect(user = user,
                                  password = password,
                                  host = host,
                                  port = port,
                                  database = database)

    cursor = connection.cursor()
    sql = """ SELECT url, post_type from bat_dong_san_links where post_type = 'dự án' and is_crawled = false; """
    cursor.execute(sql)
    list_of_urls = cursor.fetchall()
    for next_url in list_of_urls:
        sql = """ UPDATE bat_dong_san_links SET is_crawled = TRUE
                    WHERE url = '{}' ;
             """.format(next_url[0])

        cursor.execute(sql)
        connection.commit()
        yield next_url


class DuAnContentSpider(scrapy.Spider):
    name = 'du_an_content'
    url = NextURL()

    def start_requests(self):
        start_url = next(self.url)[0]
        yield Request(start_url, callback=self.parse, dont_filter=True)

    def parse(self, response):
        crawled_dict = dict()

        keys_vn = response.xpath("//div[@class='prj-i']//div[@class='fl']/text()").extract()
        values = response.xpath("//div[@class='prj-i']//div[@class='fr']/text()").extract()
        for k, v in zip([k.strip() for k in keys_vn if k.strip()], [v.strip() for v in values if v.strip()]):
            if VN_to_English.get(k):
                crawled_dict[VN_to_English.get(k)] = v

        texts = response.xpath("//div[@class='prj-noidung a1']//*/text()").extract()
        crawled_dict['project_overview'] = [text.strip() for text in texts if text.strip()]
        yield crawled_dict

        next_url = next(self.url)[0]

        yield Request(next_url, callback=self.parse, dont_filter=True)
