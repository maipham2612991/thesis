# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import psycopg2
from batdongsancom.settings import database, user, password, host, port
import batdongsancom
from scrapy.exceptions import DropItem
from psycopg2.errors import InFailedSqlTransaction


class BatdongsancomPipeline(object):

    def __init__(self, database=database, user=user, password=password, host=host, port=port):
        self.connection = psycopg2.connect(user=user, password=password, host=host,
                                           port=port, database=database)

        self.cursor = self.connection.cursor()

    def process_item(self, item, spider):
        #print(type(spider))
        if isinstance(spider, batdongsancom.spiders.du_an_content.DuAnContentSpider):
            return item
        elif not isinstance(spider, batdongsancom.spiders.batdongsan_com_content.BatdongsanComContentSpider):
            sql =   """INSERT INTO bat_dong_san_links (url, post_type, link_taken_on, is_crawled )
                    VALUES ( '{}', '{}', '{}', {} )
                    ON CONFLICT do NOTHING;
            """.format(item.get("url"), item.get("post_type"), item.get("link_taken_on"), item.get("is_crawled"))

            try:
                self.cursor.execute(sql)
                self.connection.commit()
            except:
                self.connection.rollback()
            return item
        else:
            item_keys = item.keys()
            sql = """ INSERT INTO raw_batdongsan ( {} )
                    VALUES ( {} ) ON CONFLICT do NOTHING;
            """.format(", ".join(item_keys), " , ".join(["'"+str(item.get(i)).replace("'", "").replace('"', '').replace("’", "")+"'"
                                                                           if type(item.get(i)) is not list
                                                                           else "ARRAY"+str(item.get(i))
                                                         for i in item_keys]))
            # print(sql)
            try:
                self.cursor.execute(sql)
                self.connection.commit()
            except:
                self.connection.rollback()

            return item



