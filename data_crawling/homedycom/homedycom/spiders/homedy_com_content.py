# -*- coding: utf-8 -*-
import scrapy
from pytz import timezone
from homedycom.settings import database, user, host, port, password
import psycopg2
from scrapy.http import Request
from datetime import datetime
from datetime import date, timedelta

VN_to_English = {
    "Giá:": "price_no_VAT",
    "Diện tích:": "total_site_area",
    "Tình trạng pháp lý": "legal_info",
    "Số tầng": "number_of_floors",
    "Hướng nhà": "direction",
    "Đường vào": "route_length",
    "Mặt tiền": "front_length",
    "Số toilet": "number_of_bathrooms",
}

def NextURL():
    connection = psycopg2.connect(user = user,
                                  password = password,
                                  host = host,
                                  port = port,
                                  database = database)

    cursor = connection.cursor()
    sql = """ SELECT url from homedy_links where is_crawled = false; """
    cursor.execute(sql)
    list_of_urls = cursor.fetchall()
    for next_url in list_of_urls:
        sql = """ UPDATE homedy_links SET is_crawled = TRUE
                    WHERE url = '{}' ;
             """.format(next_url[0])
        print(next_url)
        cursor.execute(sql)
        connection.commit()
        yield next_url


class HomedyComContentSpider(scrapy.Spider):
    name = 'homedy_com_content'
    url = NextURL()

    def start_requests(self):
        start_url = next(self.url)[0]
        yield Request(start_url, callback=self.parse, dont_filter=True)

    def parse(self, response):
        crawled_dict = dict()
        """ url homedy """
        crawled_dict['url'] = response.url

        """ post title homedy """
        try:
            crawled_dict['post_title'] = response.xpath("//div[@class='col-sm-8']//h1/text()").extract()[0].strip()
        except:
            pass

        """ price, area homedy """
        keys_vn = response.xpath("//div[@class='row']//div[@class='cell label cell-left']/text()").extract()
        keys_vn = [k.strip() for k in keys_vn if k.strip()]
        values = response.xpath("//div[@class='row']//div[@class='cell cell-right']//*/text()").extract()
        values = [value.strip() for value in values if value.strip()]

        try:
            for k in keys_vn:
                k_en = VN_to_English.get(k)
                if k_en == 'price_no_VAT':
                    price_unit = response.xpath("//div[@class='row']//div[@class='cell cell-right']/text()").extract()[1].strip()
                    crawled_dict[k_en] = values[0] + " " + price_unit if price_unit else values[0]
                    values = values[1:]
                elif k_en == 'total_site_area':
                    crawled_dict[k_en] = " ".join(values[0:3])
        except:
            pass

        """ address id homedy """
        try:
            property_info = " ".join(response.xpath("//div[@class='address']//a/text()").extract())
            property_info = property_info + " " + ", ".join(response.xpath("//div[@class='address']//span/text()").extract())
            crawled_dict["address_id"] = property_info
        except:
            pass

        """ direction, news_type, number_of_bedrooms, ... homedy """
        keys_vn = response.xpath("//div[@class='row']//div[@class='cell lable']//span/text()").extract()
        keys_vn = [k.strip() for k in keys_vn]
        values = response.xpath("//div[@class='row']//div[@class='cell']/text()").extract()
        values = [v.strip(":").strip() for v in values]

        for k, v in zip(keys_vn, values):
            if VN_to_English.get(k) and v:
                crawled_dict[VN_to_English.get(k)] = v

        """ internal facility homedy """
        internal_facility = response.xpath("//div[@class='item']//div[@class='title']/text()").extract()
        if internal_facility:
            crawled_dict['internal_facility'] = [nf.strip() for nf in internal_facility if nf.strip()]

        """ full description homedy"""
        texts = response.xpath("//div[@class='description readmore']//p/text()").extract()
        crawled_dict["full_description"] = [text.strip().replace('"', '').replace("'", "").replace("’", "") for text in texts if text.strip()]

        """ post author, phone homedy """
        try:
            crawled_dict["phone_number"] = response.xpath("//a[@class='btn mobile mobile-counter pc-mobile-number']/@data-mobile").extract()[0].strip()
        except:
            pass
        try:
            crawled_dict["post_author"] = response.xpath("//div[@class='name']//a/@title").extract()[0].strip()
        except:
            pass
        try:
            crawled_dict["project_id"] = response.xpath("//div[@class='info']//a/@title").extract()[0].strip()
        except:
            pass

        """ property_code, created_date """
        try:
            crawled_dict['property_code'] = response.xpath("//div[@class='product-info']//span[@class='code']/text()").extract()[0].strip()
            crawled_dict['property_name'] = response.xpath("//div[@class='product-info']//span[@class='code type-name']/text()").extract()[0].strip()

            date_time_str = response.xpath("//div[@class='product-info']//span[@class='']/text()").extract()[0].strip()
            crawled_dict['expired_datetime'] = datetime.strptime(date_time_str, "%d/%m/%Y").strftime("%Y-%m-%d")

            date_created_str = response.xpath("//div[@class='product-info']//span[@class='date-created']/text()").extract()[0].strip()
            number = 0
            num_days = 0
            for s in date_created_str.split(" "):
                if s.isdigit():
                    number = int(s)
                elif s == "ngày":
                    num_days = number
                elif s == "tuần":
                    num_days = 7*number
                elif s == "tháng":
                    num_days = 30*number
                else:
                    num_days = 0
            crawled_dict['created_datetime'] = (datetime.now(timezone('UTC')).astimezone(timezone('Asia/Ho_Chi_Minh')).date() - timedelta(days=num_days)).strftime("%Y-%m-%d")

        except:
            pass

        """ date_scrape """
        crawled_dict["updated_datetime"] = date.today().strftime("%Y-%m-%d")

        yield crawled_dict

        next_url = next(self.url)[0]

        yield Request(next_url, callback=self.parse, dont_filter=True)