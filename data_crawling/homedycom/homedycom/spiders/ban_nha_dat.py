# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from homedycom.settings import NUMS_PAGE_BANNHADAT


class BanNhaDatSpider(scrapy.Spider):
    name = 'ban_nha_dat'
    allowed_domains = ['homedy.com/ban-nha-dat']
    start_urls = ['https://homedy.com/ban-nha-dat/?sort=new']

    def parse(self, response):
        yield self.parse_url(response)
        for i in range(1, NUMS_PAGE_BANNHADAT +1):
            url = 'https://homedy.com/ban-nha-dat/p' + str(i) + "?sort=new"
            yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)
        pass

    def parse_url(self, response):
        urls = response.xpath("//h3//a[@class='title padding-hoz']/@href").extract()
        for url in urls:
            yield { "url": "".join(["https://homedy.com/", url]), "post_type": "nhà đất bán",
                    "link_taken_on":  datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "is_crawled": False }
