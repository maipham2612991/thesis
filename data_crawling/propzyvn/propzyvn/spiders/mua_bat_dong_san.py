# -*- coding: utf-8 -*-
import scrapy
from datetime import datetime
from propzyvn.settings import NUMS_PAGE_MUABDS


class MuaBatDongSanSpider(scrapy.Spider):
    name = 'mua_bat_dong_san'
    allowed_domains = ['propzy.vn/mua/bat-dong-san/hcm']
    start_urls = ['https://propzy.vn/mua/bat-dong-san/hcm/']

    def parse(self, response):
        yield self.parse_url(response)
        for i in range(1, NUMS_PAGE_MUABDS +1):
            url = self.start_urls[0] + "p"+str(i)
            yield scrapy.Request(url=url, callback=self.parse_url, dont_filter=True)
        pass

    def parse_url(self, response):
        urls = response.xpath("//div[@class='divtext']//h2//a[@class='title']/@href").extract()
        for url in urls:
            yield { "url": "".join(["https://propzy.vn", url]), "post_type": "nhà đất bán",
                    "link_taken_on":  datetime.now().strftime("%Y-%m-%d %H:%M:%S"), "is_crawled": False }
