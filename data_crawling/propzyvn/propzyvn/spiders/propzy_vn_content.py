# -*- coding: utf-8 -*-
import scrapy
from propzyvn.settings import database, user, host, port, password
import psycopg2
from scrapy.http import Request
from datetime import datetime

VN_to_English = {
    "Hướng": "direction",
    "Phòng ngủ": "number_of_bedrooms",
    "Phòng tắm": "number_of_bathrooms",
    "Tầng": "floor",
    "Giấy tờ": "legal_info",
    "Hẻm": "route_length",
}


def NextURL():
    connection = psycopg2.connect(user = user,
                                  password = password,
                                  host = host,
                                  port = port,
                                  database = database)

    cursor = connection.cursor()
    sql = """ SELECT url from propzy_links where is_crawled = false; """
    cursor.execute(sql)
    list_of_urls = cursor.fetchall()
    for next_url in list_of_urls:
        sql = """ UPDATE propzy_links SET is_crawled = TRUE
                    WHERE url = '{}' ;
             """.format(next_url[0])
        print(next_url)
        cursor.execute(sql)
        connection.commit()
        yield next_url


class PropzyVnContentSpider(scrapy.Spider):
    name = 'propzy_vn_content'
    url = NextURL()

    def start_requests(self):
        start_url = next(self.url)[0]
        yield Request(start_url, callback=self.parse, dont_filter=True)

    def parse(self, response):
        crawled_dict = dict()
        """ url propzy """
        crawled_dict['url'] = response.url

        """ post title propzy """
        try:
            crawled_dict['post_title'] = response.xpath("//div[@class='t-detail']//h1[@class='h4']/text()").extract()[0].strip()
        except:
            pass

        """ price, area propzy """
        try:
            price_and_area = response.xpath("//span[@class='price']//b/text()").extract()[0].split("-")
            crawled_dict['price_no_vat'] = price_and_area[0].strip()
            crawled_dict['total_site_area'] = price_and_area[1].strip()
        except:
            pass

        """ address id propzy """
        try:
            address_info = response.xpath("//div[@class='breadcrumbs']//a/text()").extract() + \
                           response.xpath("//div[@class='breadcrumbs']//a//b/text()").extract()
            crawled_dict["address_id"] = ", ".join([addr.strip() for addr in address_info if addr.strip()])
        except:
            pass

        """ direction, news_type, number_of_bedrooms, ... propzy """
        kv_list = response.xpath("//ul[@class='cols-2']//li/text()").extract()
        for kv in kv_list:
            k, v = kv.split(":")
            k = k.strip()
            v = v.strip()
            if VN_to_English.get(k) and v:
                crawled_dict[VN_to_English.get(k)] = v

        """ near facility propzy """
        try:
            near_facility = response.xpath("//ul[@class='cols-3']//li//span/text()").extract()
            if near_facility:
                crawled_dict['near_facility'] = [nf.strip() for nf in near_facility if nf.strip()]
        except:
            pass

        """ full description """
        texts = response.xpath("//div[@class='boxwidget entry-content']//div//*/text()").extract()
        crawled_dict["full_description"] = [text.strip().replace('"', '').replace("'", "").replace("’", "") for text in texts if text.strip()]

        """ get phone number """
        import re
        try:
            crawled_dict["phone_number"] = re.findall("\d{10}", " ".join(texts))[0]

        except:
            pass

        """ date_scrape """
        from datetime import date
        crawled_dict["updated_datetime"] = date.today().strftime("%Y-%m-%d")

        yield crawled_dict

        next_url = next(self.url)[0]

        yield Request(next_url, callback=self.parse, dont_filter=True)
