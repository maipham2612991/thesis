import time

from .generate_data import Generating
from utils.preprocess import *


MAX_FLOORS = 30
MAX_BEDROOMS = 200
MAX_BATHROOMS = 100
LESS_THAN_DROP = 2
DAT = 'đất'
DAT_NEN_DA = 'đất nền dự án'
HCM = 'hồ chí minh'

attr_scale = ['area', 'price']
attr_outl_rm = ['area', 'price'] 

attr_fill_zero = ['front', 'alley', 'num_bedrooms', 'num_bathrooms', 'num_floors']

attr_drop_na = ['addr_province', 'addr_city', 'addr_district']

attr_list_for_train = [
            'area', 'property_type', 'price', 
            'addr_province', 'addr_city', 'addr_district', 'addr_ward',
            'year', 'month', 'weekofyear', 'weekday', 
            'quarter', 'is_month_start', 'is_month_end', 'alley', 'front',
            'num_floors', 'num_bathrooms', 'num_bedrooms'
        ]

attr_list_for_label = [
    'addr_province', 'addr_city', 'addr_district', 'addr_ward', 'property_type'
]

attr_list_for_onehot = [
    'property_type', 'year', 'month', 'weekofyear', 'weekday', 
    'quarter', 'is_month_start', 'is_month_end'
]

attr_list_for_drop_dups = ['area', 'property_type', 'addr_province', 'addr_ward', 
        'addr_city', 'addr_district', 'year', 'month', 'weekofyear', 'weekday',
        'quarter', 'is_month_start', 'is_month_end']
# attr_scale = ['area', 'price']
# attr_outl_rm = ['area', 'price'] 

# attr_fill_zero = ['front', 'alley']

# attr_drop_na = ['addr_province', 'addr_city', 'addr_district']

# attr_list_for_train = [
#             'area', 'property_type', 'price', 
#             'addr_province', 'addr_city', 'addr_district', 'addr_ward',
#             'year', 'month', 'weekofyear', 'weekday', 
#             'quarter', 'is_month_start', 'is_month_end', 'alley', 'front',

#         ]

# attr_list_for_label = [
#     'addr_province', 'addr_city', 'addr_district', 'addr_ward', 'property_type'
# ]

# attr_list_for_onehot = [
#     'property_type', 'year', 'month', 'weekofyear', 'weekday', 
#     'quarter', 'is_month_start', 'is_month_end'
# ]

# attr_list_for_drop_dups = ['area', 'property_type', 'addr_province', 'addr_ward', 
#         'addr_city', 'addr_district', 'year', 'month', 'weekofyear', 'weekday',
#         'quarter', 'is_month_start', 'is_month_end']


class HouseGenerating(Generating):
    def __init__(self):
        super().__init__()

    
    def process(self, df):
        start = time.time()
        print('Pre-processing data first ...')
        sell_cond = df['transaction_type'] == 'bán'
        df = df[sell_cond]

        df['price_all'] = df.price * df.area
        df = df[df['price_all'] >= 50]

        df = df.dropna(subset=['price'])
        df = df[df['property_type'] != 'nhà trọ, phòng trọ']
        df['property_type'] = df['property_type'].replace({'đất nền dự án':'đất nền dự án'})
        df = df[df['price'] != 0]
        df = df.dropna(subset=['property_type'])
        df = df.dropna(subset=['area'])
        df = df.dropna(subset=['addr_street', 'addr_ward', 'addr_district', 'addr_city', 'addr_province'], how = 'all')
        df= df.replace({np.nan: None})
        
        print('Filtering ...')
        # success_condition = df['status'] != 'Failed'
        # df = df[success_condition]

        # Filter data with only "dat" in property_type:
        prop_cond = (df['property_type'] != DAT) & (df['property_type'] != DAT_NEN_DA)
        df = df[prop_cond]
        print('Info: ', df.info())

        # Convert created_date to data for training:
        print('Converting datetime ...')
        df = convert_datetime(df)

        # Filtering attributes for training:
        print('Choosing attributes ...')
        df = df[attr_list_for_train]

        # Clean data:
        print('Cleaning data ...')
        df = self._clean_data(df)

        # Drop rare appearance of a value in a categorical variable:
        print('Drop rare appearance of a value in a categorical variable ...')
        v = df[['addr_ward']].astype(str)
        df = df[v.replace(v.apply(pd.Series.value_counts)).gt(LESS_THAN_DROP).all(1)]
       
        # Drop duplicates:
        print('Drop duplicates and drop non-full address ...')
        df = drop_duplicates(df, attr_list_for_drop_dups)
        df = df.dropna(thresh=2, subset=attr_drop_na)
        
        # Pre-process property_type:
        print('Choose types of property_type ...')
        df = df.replace({'loại bất động sản khác':'bất động sản khác', 'nhà phố thương mại':'nhà mặt phố'})
        df = df[(df['property_type'] != 'bất động sản khác') & (df['property_type'] != 'kho, nhà xưởng')  & (df['property_type'] != 'văn phòng') & ((df['property_type'] != 'trang trại, khu nghỉ dưỡng'))] 

        # Process high-percentage missing values:
        print('Process high-percentage missing values ...')
        df = fill_with_zero(df, attr_fill_zero)
        # df = fill_with_zero(df, ['front', 'alley', 'num_floors'])
        # df = fill_with_median(df, ['num_bedrooms', 'num_bathrooms'])
        # df['num_bedrooms'] = df['num_bedrooms'].replace({None:0})
        # df['num_bathrooms'] = df['num_bathrooms'].replace({None:0})
        # df['num_floors'] = df['num_floors'].replace({None:0})

        # Remove big values in #bedrooms, ...:
        print('Remove big values in #bedrooms, #floors, #bathrooms ...')
        df = df[df['num_bedrooms'] <= MAX_BEDROOMS]
        df = df[df['num_bathrooms'] <= MAX_BATHROOMS]
        df = df[df['num_floors'] <= MAX_FLOORS]

        df = df.replace({None: 'null'}).replace({'None': 'null'})
        # Get encoding:
        print('Getting encoding ...')
        df = encode_data(df, attr_list_for_label, attr_list_for_onehot, 'nha')

        # Scaling data:
        print('Scaling data ...')
        df = log_transformation(df, attr_scale)

        # Remove outliers:
        print('Outliers removal ...')
        df = remove_outliers(df, attr_outl_rm)
        
        stop = time.time()
        print(f"Time for processing data: {stop - start}s")

        print(df.columns)
        print(df.info())
        return df


    def _clean_data(self, df):
        print('Cleaning province ...')
        # for index, row in df.iterrows():
        #     if row['addr_province'] != None:
        #         if len(row['addr_province'].split()) > 4 or '|' in row['addr_province'] or any(map(str.isdigit, row['addr_province'])):
        #             df = df.drop([index])

        # out = ['tp.hcm', 'thành phố đồng hới', 'tphcm', 'phú lương', 'vĩnh hung hoàng mai', 'hẻm', 'ngọc thụy', 'gia thụy', 'vietnam', 'giáp nhị thịnh liệt', 'lạc long quân', 'ngọc lâm', 'cộng hòa', 'trần đăng ninh', 'tân bình',
        # 'hoàng việt', 'lê thanh nghị', 'đường hoa lan', 'phú nhuận',
        # 'bàu cát', 'lê văn huân', 'núi thành', 'giải phóng',
        # 'the manhattan', 'hoàng cầu', 'việt nam']

        # for index, row in df.iterrows():
        #     if row['addr_province'] in out:
        #         df = df.drop([index])

        # sub = 'tỉnh '
        # for index, row in df.iterrows():
        #     if row['addr_province'] != None:
        #         if sub in row['addr_province']:
        #             a = str(row['addr_province']).replace(sub, '')
        #             df.loc[index, 'addr_province'] = a

        # df = df[df['addr_province'] != 'cự khối']

        df['addr_province'] = df['addr_province'].str.replace('tỉnh ', '', regex=False)
        v = df[['addr_province']].astype(str)
        df = df[v.replace(v.apply(pd.Series.value_counts)).gt(6).all(1)]
      
        print('Cleaning city ...')
        # for index, row in df.iterrows():
        #     if row['addr_city'] != None:
        #         if len(row['addr_city'].split()) > 4 or  row['addr_city'].isdigit():
        #             df = df.drop([index])

        out2 = ['huyện ', 'thành phố ', 'thị xã ', 'tp ', 'tx ', 'tx. ', 'tp. ']
        # for index, row in df.iterrows():
        #     if row['addr_city'] != None:
        #         for sub in out2:
        #             if sub in row['addr_city']:
        #                 a = str(row['addr_city']).replace(sub, '')
        #                 df.loc[index, 'addr_city'] = a
                        
        # df = df[df['addr_city'] != 'hà nộis']
        for ele in out2:
            df['addr_city'] = df['addr_city'].str.replace(ele, '', regex=False)

        v = df[['addr_city']].astype(str)
        df = df[v.replace(v.apply(pd.Series.value_counts)).gt(6).all(1)]

        print('Cleaning district ...')
        # for index, row in df.iterrows():
        #     if row['addr_district'] != None:
        #         if len(row['addr_district'].split()) > 4 or len(row['addr_district'].split()) == 0:
        #             df = df.drop([index])
          
        # out2 = [ 'hùng vương quận ', 's quận ', 'huyện ', 'quận ', 'q. ', 'p. ', 'q.', 'thị xã ']
        # for index, row in df.iterrows():
        #     #print(index)
        #     if row['addr_district'] != None and row['addr_district'].isdigit() == False:
        #         for sub in out2:
        #             if sub in row['addr_district']:
        #                 a = str(row['addr_district']).replace(sub, '')
        #                 df.loc[index, 'addr_district'] = a
        #                 break
        out2 = ['huyện ', 'quận ', 'thị xã ', 'hùng vương quận ', 's quận ', 'huyện ', 'quận ', 'q. ', 'p. ', 'q.']
        # # for index, row in df.iterrows():
        # #     #print(index)
        # #     if row['addr_district'] != None and row['addr_district'].isdigit() == False:
        # #         for sub in out2:
        # #             if sub in row['addr_district']:
        # #                 a = str(row['addr_district']).replace(sub, '')
        # #                 df.loc[index, 'addr_district'] = a
        # #                 break

        for ele in out2:
            df['addr_district'] = df['addr_district'].str.replace(ele, '', regex=False)


        # df = df[(df['addr_district']!='thị trấn nhà bè') & (df['addr_district']!='số 8 phú nhuận')]
        # df = df[df['addr_district']!='ng hành sơn']

        v = df[['addr_district']].astype(str)
        df = df[v.replace(v.apply(pd.Series.value_counts)).gt(3).all(1)]

        print('Cleaning ward ...')
        # ward = ['phường', 'xã', 'thị trấn']

        # for index, row in df.iterrows():
        #     if row['addr_ward'] != None:
        #         if row['addr_ward'].startswith(tuple(ward)) == False:
        #             df = df.drop([index])

        v = df[['addr_ward']].astype(str)
        df = df[v.replace(v.apply(pd.Series.value_counts)).gt(3).all(1)]

        return df

        
