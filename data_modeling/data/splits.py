import numpy as np

from sklearn.model_selection import train_test_split
from abc import ABC, abstractmethod


class TrainTestSplitUsingLib(ABC):
    def __init__(self):
        self.test_size = 0.2
        self.random_state = 42
        self.shuffle = True


    def __call__(self, data):
        data_X = data.drop(['price'], axis=1)
        data_y = data['price']

        X_train, X_test, y_train, y_test = train_test_split(
            data_X, data_y,
            test_size = self.test_size,
            random_state = self.random_state
        )

        return X_train, X_test, y_train, y_test


