import time

from .generate_data import Generating
from utils.preprocess import *


HCM = 'hồ chí minh'
DAT = 'đất'
DAT_NEN_DA = 'đất nền dự án'

attr_drop_na = ['addr_province', 'addr_city', 'addr_district']

attr_fill_zero = ['front', 'alley']

attr_outl_rm = ['area', 'price'] 
attr_scale = ['area', 'price']

attr_list_for_train = [
            'area', 'property_type', 'price', 
            'addr_province', 'addr_city', 'addr_district', 'addr_ward',
            'year', 'month', 'weekofyear', 'weekday', 
            'quarter', 'is_month_start', 'is_month_end', 'alley', 'front'
        ]

attr_list_for_label = [
    'addr_province', 'addr_city', 'addr_district', 'addr_ward', 'property_type'
]

attr_list_for_onehot = [
    'property_type', 'year', 'month', 'weekofyear', 'weekday', 
    'quarter', 'is_month_start', 'is_month_end'
]

attr_list_for_drop_dups = ['area', 'property_type', 'addr_province', 'addr_ward', 
        'addr_city', 'addr_district', 'year', 'month', 'weekofyear', 'weekday',
        'quarter', 'is_month_start', 'is_month_end']

# attr_list_for_drop_dups = ['area', 'property_type', 'addr_province', 'addr_ward', 
#         'addr_city', 'addr_district', 'year', 'month',
#         'quarter']


class DatGenerating(Generating):
    def __init__(self):
        super().__init__()


    def process(self, df):
        # Filter successful converting address to longitude and latitude 
        start = time.time()
        print('Pre-processing data first ...')
        sell_cond = df['transaction_type'] == 'bán'
        df = df[sell_cond]

        df['price_all'] = df.price * df.area
        df = df[df['price_all'] >= 50]
        
        df = df.dropna(subset=['price'])
        df = df[df['property_type'] != 'nhà trọ, phòng trọ']
        df['property_type'] = df['property_type'].replace({'đất nền dự án':'đất nền dự án'})
        df = df[df['price'] != 0]
        df = df.dropna(subset=['property_type'])
        df = df.dropna(subset=['area'])
        df = df.dropna(subset=['addr_street', 'addr_ward', 'addr_district', 'addr_city', 'addr_province'], how = 'all')
        df= df.replace({np.nan: None})

        print('Filtering ...')
        # success_condition = df['status'] != 'Failed'
        # df = df[success_condition]

        # Filter data with only "dat" in property_type:
        prop_cond = (df['property_type'] == DAT) | (df['property_type'] == DAT_NEN_DA)
        df = df[prop_cond]
        print('Info: ', df.info())
        # Dat in Ho Chi Minh city:
        #df = df[df['addr_city'] == HCM]
        
        # Convert created_date to data for training:
        print('Converting datetime ...')
        df = convert_datetime(df)

        # Filtering attributes for training:
        print('Choosing attributes ...')
        df = df[attr_list_for_train]

        # Clean data:
        print('Cleaning data ...')
        df = self._clean_data(df)

        # Drop duplicates:
        print('Drop duplicates and drop non-full address ...')
        df = drop_duplicates(df, attr_list_for_drop_dups)
        df = df.dropna(thresh=2, subset=attr_drop_na)

        # Process high-percentage missing values:
        print('Process high-percentage missing values ...')
        df = fill_with_zero(df, attr_fill_zero)
        # df = fill_with_median(df, attr_fill_zero)

        df = df.replace({None: 'null'}).replace({'None': 'null'})
        # Get encoding:
        print('Getting encoding ...')
        df = encode_data(df, attr_list_for_label, attr_list_for_onehot, 'dat')

        # Scaling data:
        print('Scaling data ...')
        df = log_transformation(df, attr_scale)

        # Remove outliers:
        print('Outliers removal ...')
        df = remove_outliers(df, attr_outl_rm)
        #df = remove_outliers_using_DBSCAN(df, attr_outl_rm)
        # df = remove_outliers_using_Isolation_Forest(df)
        
        print(df.columns)
        
        stop = time.time()
        print(f"Time for processing data: {stop - start}s")

        print(df.info())
        return df


    def _clean_data(self, df):
        # print('Cleaning district ...')
        # st_dic = {'bình chánh':'huyện bình chánh', 'cần giờ':'huyện cần giờ', 'củ chi':'huyện củ chi', 'thủ đức': 'quận thủ đức', 'hóc môn': 'huyện hóc môn', 'nhà bè':'huyện nhà bè', 
        # 'gò vấp':'quận gò vấp', 'bình tân':'quận bình tân', 'tân phú':'quận tân phú', 'bình thạnh': 'quận bình thạnh', 'nền dự án  huyện củ chi': 'huyện củ chi', 'tân bình':'quận tân bình', 'phú nhuận':'quận phú nhuận', 'nền dự án  quận bình tân':'quận bình tân',
        # 'phường long thạnh mỹ quận 9':'quận 9', '980a nguyễn duy trinh  phường phú hữu quận 9':'quận 9', 'quận 7':'quận 7', 'quận 12':'quận 12', 'quận 2':'quận 2', 'quận 9':'quận 9', 'quận 6':'quận 6', 'quận 10':'quận 10', 'quận 8':'quận 8', 'quận 1':'quận 1', 'quận 5':'quận 5',
        # 'quận 4':'quận 4', 'quận 11':'quận 11','quận 3':'quận 3', 'quận bình thạnh':'quận bình thạnh', 'quận gò vấp':'quận gò vấp', 'quận phú nhuận':'quận phú nhuận', 'huyện củ chi':'huyện củ chi', 'huyện nhà bè':'huyện nhà bè', 'huyện bình chánh': 'huyện bình chánh', 'huyện hóc môn':'huyện hóc môn',
        # 'huyện cần giờ':'huyện cần giờ', 'quận thủ đức':'quận thủ đức', 'quận tân phú':'quận tân phú', 'quận tân bình':'quận tân bình', 'quận bình tân':'quận bình tân'}
        # df['addr_district'] = df['addr_district'].map(st_dic)
        # #print('Unique district:', df.addr_district.unique())
        # # b = list(dic.keys())
        # # print(np.setdiff1d(a,b))

        # print('Cleaning ward ...')
        # ward = ['phường', 'xã', 'thị trấn']
        # for index, row in df.iterrows():
        #     if (row['addr_ward'] != None) and (ward[0] not in row['addr_ward']) and (ward[1] not in row['addr_ward']) and (ward[2] not in row['addr_ward']):
        #         #print(row['addr_ward'])
        #         # row['addr_ward'] = None
        #         df.at[index,'addr_ward'] = None
        #     if row['addr_ward'] == 'nền dự án  phường bình hưng hòa b':
        #         df.at[index,'addr_ward'] = 'phường bình hưng hòa b'
        #     if row['addr_ward'] == 'kp 5 phường hiệp bình chánh':
        #         df.at[index,'addr_ward'] = 'phường hiệp bình chánh'

        # return df
        print('Cleaning province ...')
        # for index, row in df.iterrows():
        #     if row['addr_province'] != None:
        #         if len(row['addr_province'].split()) > 4 or '|' in row['addr_province']:
        #             df = df.drop([index])

        # out = ['chợ sông thao','hcm', 'tỉnh bến tre', 'thị trấn tân khai', 
        # 'tỉnh kon tum', 'tỉnh kiên giang', 'tỉnh bình dương', 'hien ninh', 
        # 'tỉnh hậu giang', 'tỉnh long an', 'đường nguyễn thị định', 'tỉnh khánh hòa', 
        # 'hồ chí min2.5','quận 8', 'phố bạch mai', 'tp bảo lộc']

        # for index, row in df.iterrows():
        #     if row['addr_province'] in out:
        #         df = df.drop([index])

        df['addr_province'] = df['addr_province'].str.replace('tỉnh ', '', regex=False)
        v = df[['addr_province']].astype(str)
        df = df[v.replace(v.apply(pd.Series.value_counts)).gt(6).all(1)]

        print('Cleaning city ...')
        # for index, row in df.iterrows():
        #     if row['addr_city'] != None:
        #         if len(row['addr_city'].split()) > 4 or  row['addr_city'].isdigit():
        #             df = df.drop([index])

        out2 = ['huyện ', 'thành phố ', 'thị xã ', 'tp ', 'tx ', 'tx. ']
        # for index, row in df.iterrows():
        #     if row['addr_city'] != None:
        #         for sub in out2:
        #             if sub in row['addr_city']:
        #                 rep = str(row['addr_city']).replace(sub, '')
        #                 df.loc[index, 'addr_city'] = rep

        for ele in out2:
            df['addr_city'] = df['addr_city'].str.replace(ele, '', regex=False)

        v = df[['addr_city']].astype(str)
        df = df[v.replace(v.apply(pd.Series.value_counts)).gt(6).all(1)]

        print('Cleaning district ...')
        # for index, row in df.iterrows():
        #     if row['addr_district'] != None:
        #         if len(row['addr_district'].split()) > 4 :
        #             df = df.drop([index])

        out2 = ['huyện ', 'quận ', 'thị xã ']
        # for index, row in df.iterrows():
        #     if row['addr_district'] != None and row['addr_district'].isdigit() == False:
        #         for sub in out2:
        #             if sub in row['addr_district']:
        #                 rep = str(row['addr_district']).replace(sub, '')
        #                 df.loc[index, 'addr_district'] = rep
        for ele in out2:
            df['addr_district'] = df['addr_district'].str.replace(ele, '', regex=False)

        v = df[['addr_district']].astype(str)
        df = df[v.replace(v.apply(pd.Series.value_counts)).gt(3).all(1)]

        print('Clean ward ...')
        # ward = ['phường', 'xã', 'thị trấn']
        # for index, row in df.iterrows():
        #     if row['addr_ward'] != None:
        #         if row['addr_ward'].startswith(tuple(ward)) == False:
        #             df = df.drop([index])

        v = df[['addr_ward']].astype(str)
        df = df[v.replace(v.apply(pd.Series.value_counts)).gt(3).all(1)]

        return df



