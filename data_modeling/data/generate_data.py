import psycopg2
import json
import pandas as pd

from sqlalchemy import create_engine
from .db_config import *
from abc import ABC, abstractmethod


path = '../models_saved/data_columns.json'


class Generating(ABC):
    def __init__(self):
        self.engine = create_engine('postgresql://'
                                            + user 
                                            + ':' + password
                                            + '@' + host
                                            + ':' + port
                                            + '/' + database)
        self.sqlengine = self.engine


    def query(self):
        sql = f"""SELECT distinct * FROM real_estate_integrated_3 
                WHERE price > 0 
        """
        df = pd.read_sql_query(sql, con = self.sqlengine)

        if df.shape[0] > 0:
            print('Head of dataframe:', df.head())
            print('Infomation:', df.info())
            print('=' * 15)
            
            processed_df = self.process(df)

            print('Done')
            print('=' * 15)
            return processed_df
        else:
            print('Error with reading data')


    @abstractmethod
    def process(self, df):
        pass
        


        

