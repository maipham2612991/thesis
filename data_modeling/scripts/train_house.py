import warnings
warnings.filterwarnings("ignore")
import sys
sys.path.append('..')

from data import HouseGenerating
from data import TrainTestSplitUsingLib
from models import RE_XGBoostRegressor


CV = False


# def run_model():
print('Start process data ...')
nha = HouseGenerating()
df = nha.query()

print('Split data ...')
tts = TrainTestSplitUsingLib()
X_train, X_test, y_train, y_test = tts(df)

print('Start training model ...')
xgb_model = RE_XGBoostRegressor()
xgb_model.construct_model()
xgb_model.fit(X_train, y_train)

print('Evaluating ...')
result = xgb_model.evaluate(X_test, y_test, CV)

print('Save model ...')
xgb_model.save_model('final_model_house.sav')

print('Evaluating with KFold ...')
xgb_model.evaluate_kfold(df)
# if __name__ == '___main___':
#     run_model()
