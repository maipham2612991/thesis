import warnings
warnings.filterwarnings("ignore")
import sys
sys.path.append('..')

from data import RentGenerating
from data import TrainTestSplitUsingLib
from models import RE_RandomForestRegressor


CV = False


# def run_model():
print('Start process data ...')
thue = RentGenerating()
df = thue.query()

print('Split data ...')
tts = TrainTestSplitUsingLib()
X_train, X_test, y_train, y_test = tts(df)

print('Start training model ...')
model = RE_RandomForestRegressor()
model.construct_model()
model.fit(X_train, y_train)

print('Evaluating ...')
result = model.evaluate(X_test, y_test, CV)

print('Save model ...')
model.save_model('final_model_rent.sav')

print('Evaluating with KFold ...')
model.evaluate_kfold(df)
# if __name__ == '___main___':
#     run_model()
