import warnings
warnings.filterwarnings("ignore")
import sys
sys.path.append('..')

from data import HouseGenerating
from data import TrainTestSplitUsingLib
from models import RE_RandomForestRegressor


cv = 5
scoring = 'r2'
parameters = {
 'bootstrap': [True, False],
 'max_depth': [10, 20, 30, 40, 50, 60, 70, 80, 90, 100, None],
 'max_features': ['auto', 'sqrt'],
 'min_samples_leaf': [1, 2, 4],
 'min_samples_split': [2, 5, 10],
 'n_estimators': [200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000]
}

# def run_model():
print('Start process data ...')
nha = HouseGenerating()
df = nha.query()

print('Split data ...')
tts = TrainTestSplitUsingLib()
X_train, X_test, y_train, y_test = tts(df)

print('Start tunning parameters ...')
model = RE_RandomForestRegressor()
model.construct_model()
model.tune_hyperparameter_model(X_train, y_train, parameters, scoring, cv)

print('Evaluating ...')
model.evaluate_cv(X_test, y_test)

