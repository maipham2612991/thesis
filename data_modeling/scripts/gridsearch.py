import warnings
warnings.filterwarnings("ignore")
import sys
sys.path.append('..')

from data import DatGenerating
from data import TrainTestSplitUsingLib
from models import RE_XGBoostRegressor


cv = 5
scoring = 'r2'
parameters = {
    #'objective':['reg:linear'],
    #'learning_rate': [0.01, 0.02, 0.03, 0.05, 0.07, 0.1, 0.2], #so called `eta` value
    #'learning_rate': [0.01],
    #'learning_rate': [0.008, 0.009, 0.01, 0.011, 0.012],
    ##'learning_rate': [0.012, 0.013, 0.014, 0.015],
    ###'max_depth': [3, 4, 5, 6, 7, 8], ####8
    'max_depth': [8],
    ###'min_child_weight': [1, 2, 3, 4, 5, 6], ###6
    'min_child_weight': [2], 
    #'silent': [1],
    # 'gamma': [0, 0.1, 0.2, 0.3, 0.4, 0.5], ###0.2
    #'subsample':[i/10.0 for i in range(6,11)], ###1
    'gamma': [0.2],
    ###'colsample_bytree':[i/10.0 for i in range(6,11)], ###0.7
    'colsample_bytree': [0.7],
    #'reg_lambda':[0.5, 1],
    #'reg_alpha':[0, 0.001, 0.002, 0.003, 0.004, 0.005, 0.01, 0.02, 0.03, 0.04, 0.05], ###0.05
    'reg_alpha': [0.05],
    #'n_estimators': [500, 1000, 3000, 5000]
    #'n_estimators': [5000, 7000, 10000]
    #'n_estimators': [10000, 12000, 15000]
    #'n_estimators': [8500, 9000, 9500]
    ##'n_estimators': [10000]
}


# def run_model():
print('Start process data ...')
dat = DatGenerating()
df = dat.query()

print('Split data ...')
tts = TrainTestSplitUsingLib()
X_train, X_test, y_train, y_test = tts(df)

print('Start tunning parameters ...')
xgb_model = RE_XGBoostRegressor()
xgb_model.construct_model()
xgb_model.tune_hyperparameter_model(X_train, y_train, parameters, scoring, cv)

print('Evaluating ...')
xgb_model.evaluate_cv(X_test, y_test)

