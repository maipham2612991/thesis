import warnings
warnings.filterwarnings("ignore")
import sys
sys.path.append('..')

from utils.metrics import *

from data import HouseGenerating
from data import TrainTestSplitUsingLib
from models import RE_XGBoostRegressor
from models import RE_ElasticNet
from models import RE_Lasso
from models import RE_GradientBoostingRegressor
from models import RE_BayesianRidge
from models import RE_DecisionTreeRegressor
from models import RE_RandomForestRegressor
from models import RE_LassoLarsIC
from models import RE_LinearRegression
from models import RE_SVR
from models import RE_KNN
from models import RE_Ridge
from models import RE_ANN

from sklearn.preprocessing import PolynomialFeatures


CV = False


# def run_model():
print('Start process data ...')
nha = HouseGenerating()
df = nha.query()

print('Split data ...')
tts = TrainTestSplitUsingLib()
X_train, X_test, y_train, y_test = tts(df)

print('Start training model ...')
models = [ RE_GradientBoostingRegressor(),
    RE_BayesianRidge(), RE_LassoLarsIC(), RE_RandomForestRegressor(), RE_DecisionTreeRegressor()]

#models = [RE_SVR()]

for model_class in models:
    model_class.construct_model()
    print(model_class.model.__class__.__name__, 'is training ...')
    
    try:
        model_class.fit(X_train, y_train)
    except:
        print('Cannot train this model')
        continue
    
    result = model_class.evaluate(X_test, y_test, CV)
    print('='*10)

# poly_reg = PolynomialFeatures(degree=2)
# X_train_poly = poly_reg.fit_transform(X_train)
# model = RE_LinearRegression()
# model.construct_model()
# print('Polynomial is training')

# try:
#     model.fit(X_train_poly, y_train)
#     y_pred = model.predict(poly_reg.fit_transform(X_test))
#     mse = MSE(y_test, y_pred)
#     mae = MAE(y_test, y_pred)
#     rmse = RMSE(y_test, y_pred)
#     r2 = R2_SCORE(y_test, y_pred)
    
#     print(f"MSE:\t{mse}")
#     print(f"RMSE:\t{rmse}")
#     print(f"R2 SCORE:\t{r2}")
#     print(f"MAE:\t{mae}")

# except:
#     print('Cannot train this model')


# if __name__ == '___main___':
#     run_model()
