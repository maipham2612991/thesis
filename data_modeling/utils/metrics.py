import numpy as np

from sklearn.metrics import r2_score
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_error
from math import sqrt


def MSE(y_actual, y_pred):
    return np.mean(np.subtract(y_actual, y_pred) ** 2)

def MSE_for_logscale(y_actual, y_pred):
    return np.mean(np.subtract(np.exp(y_actual), np.exp(y_pred)) ** 2)

def RMSE(y_actual, y_pred):
    return np.sqrt(MSE(y_actual, y_pred))

def MAE(y_actual, y_pred):
    return mean_absolute_error(y_actual, y_pred)

def RMSE_for_logscale(y_actual, y_pred):
    return np.sqrt(MSE_for_logscale(y_actual, y_pred))

def R2_SCORE(y_actual, y_pred):
    return r2_score(y_actual, y_pred) 

def R2_SCORE_for_logscale(y_actual, y_pred):
    return r2_score(np.exp(y_actual), np.exp(y_pred))
