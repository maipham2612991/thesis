import pickle
import numpy as np
import pandas as pd

from scipy import stats
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.cluster import DBSCAN
from sklearn.ensemble import IsolationForest


SCORE = 3
path = '../encode_model/'

def convert_datetime(df):
    df['created_date'] = pd.to_datetime(df['created_date'])
    df['created_date'] = df['created_date'].dt.strftime('%d.%m.%Y')

    df['year'] = pd.DatetimeIndex(df['created_date']).year
    df['month'] = pd.DatetimeIndex(df['created_date']).month
    df['day'] = pd.DatetimeIndex(df['created_date']).day

    df['dayofyear'] = pd.DatetimeIndex(df['created_date']).dayofyear
    df['weekofyear'] = pd.DatetimeIndex(df['created_date']).weekofyear
    df['weekday'] = pd.DatetimeIndex(df['created_date']).weekday
    df['quarter'] = pd.DatetimeIndex(df['created_date']).quarter
    df['is_month_start'] = pd.DatetimeIndex(df['created_date']).is_month_start
    df['is_month_end'] = pd.DatetimeIndex(df['created_date']).is_month_end
    
    return df

def remove_outliers(df, attr_lst):
    z_scores = stats.zscore(df[attr_lst])
    abs_z_scores = np.abs(z_scores)
    filtered_entries = (abs_z_scores < SCORE).all(axis=1)
    return df[filtered_entries]

def remove_outliers_using_Isolation_Forest(df):
    clf=IsolationForest(n_estimators=100, max_samples='auto', contamination=float(.12), 
                        max_features=1.0, bootstrap=False, n_jobs=-1, random_state=42, verbose=0)
    clf.fit(df)
    pred = clf.predict(df)
    metrics_df['anomaly'] = pred
    new_df = metrics_df.loc[metrics_df['anomaly']==1]
    return new_df

def remove_outliers_using_DBSCAN(df, attr_lst):
    data = df[attr_lst]
    model = DBSCAN(eps=0.8, min_samples=10).fit(data)
    outlier_df = pd.DataFrame(data)
    df = outlier_df[model.labels_ != -1]
    return pd.DataFrame(df)

def encode_data(df, attr_list_for_label, attr_list_for_onehot, typed):
    # return pd.get_dummies(df, columns = attr_lst)
    attr_lst_1 = attr_list_for_label
    for i in range(len(attr_lst_1)):
        le = LabelEncoder()
        df[attr_lst_1[i]] = le.fit_transform(df[attr_lst_1[i]].astype(str))
        with open(path + str(typed) + '_' + str(attr_lst_1[i]) + '.pkl', 'wb') as f:
            pickle.dump(le, f)
    
    attr_lst = attr_list_for_onehot
    ohe = OneHotEncoder()
    df_encoded = ohe.fit_transform(df[attr_lst]).todense()
    df_encoded = pd.DataFrame(df_encoded, index=df.index, columns=ohe.get_feature_names())
    df_other_cols = df.drop(columns=attr_lst, axis=1)
    df = pd.concat([df_other_cols, df_encoded], axis=1)
    with open(path + str(typed) + '_' + 'one_hot.pkl', 'wb') as f:
        pickle.dump(ohe, f)

    return df

def scale_data(df, attr_lst):
    for attr in attr_lst:
        sc = MinMaxScaler()
        df[attr] = sc.fit_transform(df[attr].values.reshape(-1, 1))
    
    return df

def drop_duplicates(df, attr_lst):
    return df.drop_duplicates(subset=attr_lst)

def fill_with_zero(df, attr_lst):
    for attr in attr_lst:
        df[attr] = df[attr].fillna(0)
        #df[attr] = df[attr].map(lambda x:0 if x==0 else 1)
    
    return df

def fill_with_mean(df, attr_lst):
    # for attr in attr_lst:
    #     df[attr] = df[attr].fillna(df[attr], inplace=True)
        #df[attr] = df[attr].map(lambda x:0 if x==0 else 1)
    df[attr_lst] = df[attr_lst].apply(lambda x: x.fillna(x.mean()), axis=0)
    
    return df

def fill_with_median(df, attr_lst):
    # for attr in attr_lst:
    #     df[attr] = df[attr].fillna(df[attr], inplace=True)
        #df[attr] = df[attr].map(lambda x:0 if x==0 else 1)
    df[attr_lst] = df[attr_lst].apply(lambda x: x.fillna(x.median()), axis=0)
    
    return df

def log_transformation(df, attr_lst):
    for attr in attr_lst:
        df[attr] = np.log(df[attr])
    
    return df

