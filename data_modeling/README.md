# Training, comparing models and tunning for crawled real estate data
This code is only about training, comparing, tunning models for crawled land data. And house data model is just trained in many jupyter notebook files. I will update and add code for application later.

## Pre-conditions:
- Install Python 3, pip
- Virtualenv (optional)

## Installation
```bash
pip install requirements.txt
```

## Usage
- Just trainning XGBoost Regressor model
```bash
cd scripts/
python3 train.py
```

- Comparing may models: XGBoostRegressor, Random Forest, Gradient Boosting, ...
```bash
cd scripts/
python3 compare.py
```

- Applying GridSearchCV, RandomizedSearchCV.
- You can add one more parameter to function in line 39 of gridsearch.py to change from GridSearchCV to RandomizedSearchCV
```bash
cd scripts/
python3 gridsearch.py
```
