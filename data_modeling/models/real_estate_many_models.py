import time
import numpy as np

from utils.metrics import *
from .model_based import Model_Base
from sklearn.tree import DecisionTreeRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.svm import SVR
from sklearn.linear_model import ElasticNet, Lasso, BayesianRidge, LassoLarsIC, LinearRegression, Ridge
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.kernel_ridge import KernelRidge
from sklearn.neighbors import KNeighborsRegressor
from sklearn.model_selection import KFold

from keras.models import Sequential
from keras.layers import Dense

from resource import getrusage, RUSAGE_SELF


CV = False


class RE_ElasticNet(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = ElasticNet()

    
    def evaluate_kfold_model(self, data, verbose=1):
        model = ElasticNet()
        self.evaluate_kfold(data, model, 1)


class RE_Lasso(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = Lasso()


    def evaluate_kfold_model(self, data, verbose=1):
        model = Lasso()
        self.evaluate_kfold(data, model, 1)


class RE_GradientBoostingRegressor(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = GradientBoostingRegressor()


    def evaluate_kfold_model(self, data, verbose=1):
        model = GradientBoostingRegressor()
        self.evaluate_kfold(data, model, 1)


class RE_BayesianRidge(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = BayesianRidge()

    
    def evaluate_kfold_model(self, data, verbose=1):
        model = BayesianRidge()
        self.evaluate_kfold(data, model, 1)


class RE_DecisionTreeRegressor(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = DecisionTreeRegressor()

    
    def evaluate_kfold_model(self, data, verbose=1):
        model = DecisionTreeRegressor()
        self.evaluate_kfold(data, model, 1)


class RE_RandomForestRegressor(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = RandomForestRegressor()

    
    def evaluate_kfold_model(self, data, verbose=1):
        model = RandomForestRegressor()
        self.evaluate_kfold(data, model, 1)


class RE_LassoLarsIC(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = LassoLarsIC()


    def evaluate_kfold_model(self, data, verbose=1):
        model = LassoLarsIC()
        self.evaluate_kfold(data, model, 1)


class RE_LinearRegression(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = LinearRegression()

    
    def evaluate_kfold_model(self, data, verbose=1):
        model = LinearRegression()
        self.evaluate_kfold(data, model, 1)


class RE_SVR(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = SVR()

    
    def evaluate_kfold_model(self, data, verbose=1):
        model = SVR()
        self.evaluate_kfold(data, model, 1)


class RE_KNN(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = KNeighborsRegressor(n_neighbors=3)

    
    def evaluate_kfold_model(self, data, verbose=1):
        model = KNeighborsRegressor()
        self.evaluate_kfold(data, model, 1)


class RE_Ridge(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = Ridge(alpha=1.0)


    def evaluate_kfold_model(self, data, verbose=1):
        model = Ridge(alpha=1.0)
        self.evaluate_kfold(data, model, 1)


class RE_ANN(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self):
        self.model = Sequential()
        self.model.add(Dense(20, kernel_initializer='normal', activation='relu'))
        self.model.add(Dense(10, kernel_initializer='normal', activation='relu'))
        self.model.add(Dense(1, kernel_initializer='normal'))
        # Compile model
        self.model.compile(loss='mean_squared_error', optimizer='adam')


    def fit(self, X, y):
        start = time.time()
        self.model.fit(X, y, batch_size = 20, epochs = 50, verbose=0)
        stop = time.time()
        print(f"Peak memory: {getrusage(RUSAGE_SELF).ru_maxrss} KiB")
        print(f"Training time: {stop - start}s")


    def evaluate(self, test_set_X, test_set_y, cv=False, verbose=1):
        y_pred = self.predict(test_set_X, cv)
        y_pred = np.array(y_pred)
        y_pred = y_pred.ravel()

        self.mse = MSE(test_set_y, y_pred)
        self.mae = MAE(test_set_y, y_pred)
        self.rmse = RMSE(test_set_y, y_pred)
        self.r2 = R2_SCORE(test_set_y, y_pred)
        #self.r2_log = R2_SCORE_for_logscale(test_set_y, y_pred)
        self.rmse_log = RMSE_for_logscale(test_set_y, y_pred)

        if verbose == 1:
            print(f"MSE:\t{self.mse}")
            print(f"RMSE:\t{self.rmse}")
            print(f"R2 SCORE:\t{self.r2}")
            #print(f"R2 SCORE FOR LOGSCALE:\t{self.r2_log}")
            print(f"MAE:\t{self.mae}")
            print(f"RMSE FOR LOGSCALE:\t{self.rmse_log}")

        # return {"MSE": self.mse, "RMSE": self.rmse, "R2_SCORE": self.r2, "MAE": self.mae, "R2_LOG": self.r2_log}
        return {"MSE": self.mse, "RMSE": self.rmse, "R2_SCORE": self.r2, "MAE": self.mae, "RMSE_log": self.rmse_log}


    def evaluate_kfold_model(self, data, verbose=1):
        cv = KFold(n_splits=self.n_splits, random_state=42, shuffle=True)

        data_X = data.drop(['price'], axis=1)
        data_y = data['price']

        start = time.time()
        num_split = 1
        for train_index, test_index in cv.split(data_X):
            self.construct_model()
            X_train, X_test, y_train, y_test = data_X.iloc[train_index,:], data_X.iloc[test_index,:], np.array(data_y)[train_index], np.array(data_y)[test_index]
            
            self.fit(X_train, y_train)
            result = self.evaluate(X_test, y_test, CV, verbose=0)

            self.r2_scores.append(result['R2_SCORE'])
            self.rmse_scores.append(result['RMSE'])
            self.mse_scores.append(result['MSE'])
            self.mae_scores.append(result['MAE'])

            # if verbose == 1:
            #     print('Number of splits:', num_split)
            #     print(f"MSE:\t{result['MSE']}")
            #     print(f"RMSE:\t{result['RMSE']}")
            #     print(f"R2 SCORE:\t{result['R2_SCORE']}")
            #     print(f"MAE:\t{result['MAE']}")
            #     print('=' * 10)

            num_split = num_split + 1
        stop = time.time()

        if verbose == 1:
            print('Mean of R2 score =', np.mean(self.r2_scores))
            print('Mean of MSE score =', np.mean(self.mse_scores))
            print('Mean of RMSE score =', np.mean(self.rmse_scores))
            print('Mean of MAE score =', np.mean(self.mae_scores))
            print(f"Time for K-Fold: {stop - start}s")







    