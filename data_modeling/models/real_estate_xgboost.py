import time
import numpy as np

from xgboost.sklearn import XGBRegressor
from .model_based import Model_Base
from sklearn.model_selection import KFold


CV = False


class RE_XGBoostRegressor(Model_Base):
    def __init__(self):
        super().__init__()


    def construct_model(self
        # self, learning_rate=default, max_depth=default, min_child_weight=default,
        # subsample=default, colsample_bytree=default, reg_lambda=default, 
        # n_estimators=default
    ):
        self.model = XGBRegressor(
            # learning_rate=learning_rate, max_depth=max_depth, min_child_weight=min_child_weight, reg_lambda=reg_lambda,
            # subsample=1, colsample_bytree=0.7, gamma=0.2, reg_alpha=0.01,
            # n_estimators=10000, learning_rate=0.01
            # max_depth=8, min_child_weight=6
        )


    def evaluate_kfold_model(self, data, verbose=1):
        # cv = KFold(n_splits=self.n_splits, random_state=42, shuffle=True)

        # data_X = data.drop(['price'], axis=1)
        # data_y = data['price']

        # start = time.time()
        # num_split = 1
        # for train_index, test_index in cv.split(data_X):
        #     self.construct_model()
        #     X_train, X_test, y_train, y_test = data_X.iloc[train_index,:], data_X.iloc[test_index,:], np.array(data_y)[train_index], np.array(data_y)[test_index]
            
        #     self.fit(X_train, y_train)
        #     result = self.evaluate(X_test, y_test, CV, verbose=0)

        #     self.r2_scores.append(result['R2_SCORE'])
        #     self.rmse_scores.append(result['RMSE'])
        #     self.mse_scores.append(result['MSE'])
        #     self.mae_scores.append(result['MAE'])

        #     if verbose == 1:
        #         print('Number of splits:', num_split)
        #         print(f"MSE:\t{result['MSE']}")
        #         print(f"RMSE:\t{result['RMSE']}")
        #         print(f"R2 SCORE:\t{result['R2_SCORE']}")
        #         print(f"MAE:\t{result['MAE']}")
        #         print('=' * 10)

        #     num_split = num_split + 1
        # stop = time.time()

        # if verbose == 1:
        #     print('Mean of R2 score =', np.mean(self.r2_scores))
        #     print('Mean of MSE score =', np.mean(self.mse_scores))
        #     print('Mean of RMSE score =', np.mean(self.rmse_scores))
        #     print('Mean of MAE score =', np.mean(self.mae_scores))
        #     print(f"Time for K-Fold: {stop - start}s")
        model = XGBRegressor()
        self.evaluate_kfold(data, model, 1)
    

