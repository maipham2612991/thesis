import pickle
import time

from utils.metrics import *
from abc import ABC, abstractmethod
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import KFold

from resource import getrusage, RUSAGE_SELF


GRID_SEARCH_CV = 'grid'
RANDOMIZED_SEARCH_CV = 'randomized'
CV = True


class Model_Base:
    def __init__(self):
        self.model = None
        self.cv_model = None
        self.rmse = None
        self.mse = None
        self.r2 = None
        self.r2_scores = list()
        self.rmse_scores = list()
        self.mse_scores = list()
        self.mae_scores = list()
        self.n_splits = 10


    def fit(self, X, y):
        start = time.time()
        self.model.fit(X, y)
        stop = time.time()
        # print(f"Peak memory: {getrusage(RUSAGE_SELF).ru_maxrss} KiB")
        # print(f"Training time: {stop - start}s")


    def predict(self, X, cv=False):
        if cv:
            return self.cv_model.best_estimator_.predict(X)
        else:
            return self.model.predict(X)


    def save_model(self, filename):
        pickle.dump(self.model, open('../models_saved/' + filename, 'wb'))
        print('Model saved successfully')


    def evaluate(self, test_set_X, test_set_y, cv=False, verbose=1):
        y_pred = self.predict(test_set_X, cv)

        self.mse = MSE(test_set_y, y_pred)
        self.mae = MAE(test_set_y, y_pred)
        self.rmse = RMSE(test_set_y, y_pred)
        self.r2 = R2_SCORE(test_set_y, y_pred)
        #self.r2_log = R2_SCORE_for_logscale(test_set_y, y_pred)
        self.rmse_log = RMSE_for_logscale(test_set_y, y_pred)

        if verbose == 1:
            print(f"MSE:\t{self.mse}")
            print(f"RMSE:\t{self.rmse}")
            print(f"R2 SCORE:\t{self.r2}")
            #print(f"R2 SCORE FOR LOGSCALE:\t{self.r2_log}")
            print(f"MAE:\t{self.mae}")
            print(f"RMSE FOR LOGSCALE:\t{self.rmse_log}")

        # return {"MSE": self.mse, "RMSE": self.rmse, "R2_SCORE": self.r2, "MAE": self.mae, "R2_LOG": self.r2_log}
        return {"MSE": self.mse, "RMSE": self.rmse, "R2_SCORE": self.r2, "MAE": self.mae, "RMSE_log": self.rmse_log}



    def tune_hyperparameter_model(self, X_train, y_train, hyper_parameter_candidates, 
        scoring_parameter , cv_fold, search_cv_type="grid", verbose=3):   
        """
        apply grid search cv and randomized search cv algorithms to 
        find optimal hyperparameters model 
        :param model: defined machine learning model
        :param X_train: feature training data
        :param y_train: target (label) training data
        :param hyper_parameter_candidates: dictionary of 
        hyperparameter candidates
        :param scoring_parameter: parameter that controls what metric 
        to apply to the evaluated model
        :param cv_fold: number of cv divided folds
        :param search_cv_type: type of search cv (gridsearchcv or 
        randomizedsearchcv)
        :return cv_model: defined regression model
        """
        try:
            if (search_cv_type==GRID_SEARCH_CV):
                self.cv_model = GridSearchCV(estimator=self.model, 
                param_grid=hyper_parameter_candidates, 
                scoring=scoring_parameter, cv=cv_fold, verbose=verbose)
            elif (search_cv_type==RANDOMIZED_SEARCH_CV):
                self.cv_model = RandomizedSearchCV(estimator=self.model, 
                param_distributions=hyper_parameter_candidates, 
                scoring=scoring_parameter, cv=cv_fold, verbose=verbose)
            self.cv_model.fit(X_train, y_train)
        except:
            print('Error occured during tunning hyperparameters for model !')


    def evaluate_cv(self, X_test, y_test, verbose=1):
        print('Best score:', self.cv_model.best_score_)
        print('Best parameters:', self.cv_model.best_params_)

        self.evaluate(X_test, y_test, CV)


    # @abstractmethod
    def evaluate_kfold(self, data, model, verbose=1):
        CV = False
        cv = KFold(n_splits=self.n_splits, random_state=42, shuffle=True)

        data_X = data.drop(['price'], axis=1)
        data_y = data['price']

        start = time.time()
        num_split = 1
        for train_index, test_index in cv.split(data_X):
            # self.construct_model()
            self.model = model
            X_train, X_test, y_train, y_test = data_X.iloc[train_index,:], data_X.iloc[test_index,:], np.array(data_y)[train_index], np.array(data_y)[test_index]
            
            self.fit(X_train, y_train)
            result = self.evaluate(X_test, y_test, CV, verbose=0)

            self.r2_scores.append(result['R2_SCORE'])
            self.rmse_scores.append(result['RMSE'])
            self.mse_scores.append(result['MSE'])
            self.mae_scores.append(result['MAE'])

            # if verbose == 1:
            #     print('Number of splits:', num_split)
            #     print(f"MSE:\t{result['MSE']}")
            #     print(f"RMSE:\t{result['RMSE']}")
            #     print(f"R2 SCORE:\t{result['R2_SCORE']}")
            #     print(f"MAE:\t{result['MAE']}")
            #     print('=' * 10)

            num_split = num_split + 1
        stop = time.time()

        if verbose == 1:
            print('Mean of R2 score =', np.mean(self.r2_scores))
            print('Mean of MSE score =', np.mean(self.mse_scores))
            print('Mean of RMSE score =', np.mean(self.rmse_scores))
            print('Mean of MAE score =', np.mean(self.mae_scores))
            print(f"Time for K-Fold: {stop - start}s")


    @abstractmethod
    def evaluate_kfold_model(self, data, verbose=1):
        pass



