package service_dashboard.dashboard;


import com.google.common.base.Preconditions;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service_dashboard.vertx.ThesisAbstractVerticle;
import service_dashboard.vertx.VertxUtils;

public class RealEstateDashboardVerticle extends ThesisAbstractVerticle {
  private static final String API_V1 = "/api/v1";
  private static final Logger LOG = LoggerFactory.getLogger(RealEstateDashboardVerticle.class);
  private static final int httpPort = Integer.parseInt(System.getenv().getOrDefault(
      "HTTP_PORT", "9000"
  ));

  @Override
  public void start(Promise<Void> startPromise) {
    Router router = Router.router(vertx);
    VertxUtils.configVertxToAllowCORS(router);
    router.route().handler(BodyHandler.create());
    vertx.createHttpServer(VertxUtils.getHttpServerOptions())
        .requestHandler(router)
        .listen(
            httpPort,
            httpServerAsyncResult -> {
              if (httpServerAsyncResult.succeeded()) {
                LOG.info("Http server running on port {}", httpPort);
                startPromise.complete();
              } else {
                startPromise.fail("Start Http server failed");
              }
            }
        );

    router.get(API_V1 + "/test").handler(this::testAvailability);
    router.get(API_V1 + "/getAveragePricePerMonth").handler(this::getAveragePricePerMonth);
    router.get(API_V1 + "/getTransactionCount").handler(this::getTransactionCount);
    router.get(API_V1 + "/getPropertyCount").handler(this::getPropertyCount);
    router.get(API_V1 + "/getProvinceCityCount").handler(this::getProvinceCityCount);
    router.get(API_V1 + "/getMetropolisPriceArea").handler(this::getMetropolisPriceArea);
    router.get(API_V1 + "/getProject").handler(this::getProject);
    router.get(API_V1 + "/getPriceArea").handler(this::getPriceArea);
    router.get(API_V1 + "/getProjectProvince").handler(this::getProjectProvince);
    router.get(API_V1 + "/getCityDistrictPriceArea").handler(this::getCityDistrictPriceArea);
  }

  private void getCityDistrictPriceArea(RoutingContext context) {
    LOG.info("Processing Http request from {}", RealEstateDashboardWorker.CITY_DISTRICT_PRICE_AREA);

    JsonObject messageToWorker = new JsonObject()
        .put("transaction", Preconditions.checkNotNull(
            context.request().getParam("transaction"), "transaction is required"
        ))
        .put("province", Preconditions.checkNotNull(
            context.request().getParam("province"), "province is required"
        ));

    delegateToWorker(
        context,
        RealEstateDashboardWorker.class,
        RealEstateDashboardWorker.CITY_DISTRICT_PRICE_AREA,
        messageToWorker
    );
  }

  private void getProjectProvince(RoutingContext context) {
    LOG.info("Processing Http request from {}", RealEstateDashboardWorker.GET_PROJECT);

    JsonObject messageToWorker = new JsonObject()
        .put("transaction", Preconditions.checkNotNull(
            context.request().getParam("transaction"), "transaction is required"
        ))
        .put("sort", Preconditions.checkNotNull(
            context.request().getParam("sort"), "sort type is required"
        ))
        .put("province", Preconditions.checkNotNull(
            context.request().getParam("province"), "province type is required"
        ));

    delegateToWorker(
        context,
        RealEstateDashboardWorker.class,
        RealEstateDashboardWorker.GET_PROJECT_PROVINCE,
        messageToWorker
    );
  }

  private void getPriceArea(RoutingContext context) {
    LOG.info("Processing Http request from {}", RealEstateDashboardWorker.GET_PRICE_AREA);

    JsonObject messageToWorker = new JsonObject()
        .put("transaction", Preconditions.checkNotNull(
            context.request().getParam("transaction"), "transaction is required"
        ));

    delegateToWorker(
        context,
        RealEstateDashboardWorker.class,
        RealEstateDashboardWorker.GET_PRICE_AREA,
        messageToWorker
    );
  }

  private void getProject(RoutingContext context) {
    LOG.info("Processing Http request from {}", RealEstateDashboardWorker.GET_PROJECT);

    JsonObject messageToWorker = new JsonObject()
        .put("transaction", Preconditions.checkNotNull(
            context.request().getParam("transaction"), "transaction is required"
        ))
        .put("sort", Preconditions.checkNotNull(
            context.request().getParam("sort"), "sort type is required"
        ));

    delegateToWorker(
        context,
        RealEstateDashboardWorker.class,
        RealEstateDashboardWorker.GET_PROJECT,
        messageToWorker
    );
  }

  private void getMetropolisPriceArea(RoutingContext context) {
    LOG.info("Processing Http request from {}", RealEstateDashboardWorker.METROPOLIS_PRICE_AREA);

    JsonObject messageToWorker = new JsonObject()
        .put("transaction", Preconditions.checkNotNull(
            context.request().getParam("transaction"), "transaction is required"
        ));

    delegateToWorker(
        context,
        RealEstateDashboardWorker.class,
        RealEstateDashboardWorker.METROPOLIS_PRICE_AREA,
        messageToWorker
    );
  }


  private void getProvinceCityCount(RoutingContext context) {
    LOG.info("Processing Http request from {}", RealEstateDashboardWorker.PROVINCE_CITY_COUNT);

    JsonObject messageToWorker = new JsonObject()
        .put("transaction", Preconditions.checkNotNull(
            context.request().getParam("transaction"), "transaction is required"
        ));

    if (context.request().getParam("province") != null) {
      messageToWorker.put("province", context.request().getParam("province"));
    }

    delegateToWorker(
        context,
        RealEstateDashboardWorker.class,
        RealEstateDashboardWorker.PROVINCE_CITY_COUNT,
        messageToWorker
    );
  }


  private void getPropertyCount(RoutingContext context) {
    LOG.info("Processing Http request from {}", RealEstateDashboardWorker.PROPERTY_COUNT);

    JsonObject messageToWorker = new JsonObject()
        .put("transaction", Preconditions.checkNotNull(
            context.request().getParam("transaction"), "transaction is required"
        ));

    if (context.request().getParam("province") != null) {
      messageToWorker.put("province", context.request().getParam("province"));
    }

    delegateToWorker(
        context,
        RealEstateDashboardWorker.class,
        RealEstateDashboardWorker.PROPERTY_COUNT,
        messageToWorker
    );
  }

  private void getTransactionCount(RoutingContext context) {
    LOG.info("Processing Http request from {}", RealEstateDashboardWorker.TRANSACTION_COUNT);

    JsonObject messageToWorker = new JsonObject();
    if (context.request().getParam("province") != null) {
      messageToWorker.put("province", context.request().getParam("province"));
    }

    delegateToWorker(
        context,
        RealEstateDashboardWorker.class,
        RealEstateDashboardWorker.TRANSACTION_COUNT,
        messageToWorker
    );
  }

  private void getAveragePricePerMonth(RoutingContext context) {
    LOG.info("Processing Http request from {}", RealEstateDashboardWorker.AVERAGE_PRICE_PER_MONTH);

    JsonObject messageToWorker = new JsonObject()
        .put("transaction", Preconditions.checkNotNull(
            context.request().getParam("transaction"), "transaction is required"
        ));

    if (context.request().getParam("province") != null) {
      messageToWorker.put("province", context.request().getParam("province"));
    }

    delegateToWorker(
        context,
        RealEstateDashboardWorker.class,
        RealEstateDashboardWorker.AVERAGE_PRICE_PER_MONTH,
        messageToWorker
    );
  }

  private void testAvailability(RoutingContext context) {
    LOG.info("Processing Http request from {}", RealEstateDashboardWorker.TEST_AVAILABILITY);

    delegateToWorker(
        context,
        RealEstateDashboardWorker.class,
        RealEstateDashboardWorker.TEST_AVAILABILITY,
        new JsonObject()
    );
  }
}
