package service_dashboard.dashboard;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import service_dashboard.vertx.VertxUtils;

public class RealEstateDashboardMain {

  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx(VertxUtils.configVertxOption());
    vertx.deployVerticle(RealEstateDashboardVerticle.class.getName(), new DeploymentOptions().setInstances(8));
    vertx.deployVerticle(RealEstateDashboardWorker.class.getName(), new DeploymentOptions().setInstances(8));
  }
}
