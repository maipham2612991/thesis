package service_dashboard.dashboard;


import com.google.common.base.Preconditions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service_dashboard.dao.MetropolisPriceArea;
import service_dashboard.realestate.IRealEstateManager;
import service_dashboard.realestate.RealEstateImpl;
import service_dashboard.vertx.ThesisAbstractVerticle;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class RealEstateDashboardWorker extends ThesisAbstractVerticle {

  static final String CITY_DISTRICT_PRICE_AREA = "city_district_price_area";
  static final String GET_PROJECT_PROVINCE = "get_project_province";
  static final String AVERAGE_PRICE_PER_MONTH = "average_price_per_month";
  static final String TRANSACTION_COUNT = "transaction_count";
  static final String PROPERTY_COUNT = "property_count";
  static final String PROVINCE_CITY_COUNT = "province_city_count";
  static final String METROPOLIS_PRICE_AREA = "metropolis_price_area";
  static final String GET_PROJECT = "get_project";
  static final String GET_PRICE_AREA = "get_price_area";
  static final String TEST_AVAILABILITY = "test_availability";

  private static final Logger LOG = LoggerFactory.getLogger(RealEstateDashboardWorker.class);
  private IRealEstateManager realEstateManager;

  @Override
  public void start() {
    realEstateManager = new RealEstateImpl();

    vertx.eventBus().<JsonObject>consumer(
        this.getClass().getSimpleName() + "/" + TEST_AVAILABILITY,
        this::testAvailability
    );

    vertx.eventBus().<JsonObject>consumer(
        this.getClass().getSimpleName() + "/" + AVERAGE_PRICE_PER_MONTH,
        this::getAveragePricePerMonth
    );
    vertx.eventBus().<JsonObject>consumer(
        this.getClass().getSimpleName() + "/" + TRANSACTION_COUNT,
        this::getTransactionCount
    );
    vertx.eventBus().<JsonObject>consumer(
        this.getClass().getSimpleName() + "/" + PROPERTY_COUNT,
        this::getPropertyCount
    );
    vertx.eventBus().<JsonObject>consumer(
        this.getClass().getSimpleName() + "/" + PROVINCE_CITY_COUNT,
        this::getProvinceCityCount
    );
    vertx.eventBus().<JsonObject>consumer(
        this.getClass().getSimpleName() + "/" + METROPOLIS_PRICE_AREA,
        this::getMetropolisPriceArea
    );
    vertx.eventBus().<JsonObject>consumer(
        this.getClass().getSimpleName() + "/" + GET_PROJECT,
        this::getProject
    );
    vertx.eventBus().<JsonObject>consumer(
        this.getClass().getSimpleName() + "/" + GET_PRICE_AREA,
        this::getPriceArea
    );
    vertx.eventBus().<JsonObject>consumer(
        this.getClass().getSimpleName() + "/" + GET_PROJECT_PROVINCE,
        this::getProjectProvince
    );
    vertx.eventBus().<JsonObject>consumer(
        this.getClass().getSimpleName() + "/" + CITY_DISTRICT_PRICE_AREA,
        this::getCityDistrictPriceArea
    );
  }

  private void getCityDistrictPriceArea(Message<JsonObject> message) {
    try {
      String transaction_type = Preconditions.checkNotNull(
          message.body().getString("transaction", "transaction is required")
      );
      String province = Preconditions.checkNotNull(
          message.body().getString("province", "province type is required")
      );

      JsonArray result = realEstateManager.queryCityDistrictPriceArea(transaction_type, province);

      message.reply(
          new JsonObject().put("entities", result)
      );
    } catch (Exception e) {
      LOG.error("Error getCityDistrictPriceArea in RealEstateDashboardWorker");
      message.fail(500, e.getMessage());
    }
  }

  private void getProjectProvince(Message<JsonObject> message) {
    try {
      String transaction_type = Preconditions.checkNotNull(
          message.body().getString("transaction", "transaction is required")
      );
      String sort_type = Preconditions.checkNotNull(
          message.body().getString("sort", "sort type is required")
      );

      String province = Preconditions.checkNotNull(
          message.body().getString("province", "province type is required")
      );

      JsonArray result = realEstateManager.queryProjectProvince(transaction_type, sort_type, province);

      message.reply(
          new JsonObject().put("entities", result)
      );
    } catch (Exception e) {
      LOG.error("Error getProvinceCityCount in RealEstateDashboardWorker");
      message.fail(500, e.getMessage());
    }
  }

  private void getPriceArea(Message<JsonObject> message) {
    try {
      String transaction_type = Preconditions.checkNotNull(
          message.body().getString("transaction", "transaction is required")
      );

      JsonArray result = realEstateManager.queryPriceArea(transaction_type);

      message.reply(
          new JsonObject().put("entities", result)
      );
    } catch (Exception e) {
      LOG.error("Error getAveragePricePerMonth in RealEstateDashboardWorker");
      message.fail(500, e.getMessage());
    }
  }

  private void getProject(Message<JsonObject> message) {
    try {
      String transaction_type = Preconditions.checkNotNull(
          message.body().getString("transaction", "transaction is required")
      );
      String sort_type = Preconditions.checkNotNull(
          message.body().getString("sort", "sort type is required")
      );

      JsonArray result = realEstateManager.queryProject(transaction_type, sort_type);

      message.reply(
          new JsonObject().put("entities", result)
      );
    } catch (Exception e) {
      LOG.error("Error getProvinceCityCount in RealEstateDashboardWorker");
      message.fail(500, e.getMessage());
    }
  }

  private void getMetropolisPriceArea(Message<JsonObject> message) {
    try {
      String transaction_type = Preconditions.checkNotNull(
          message.body().getString("transaction", "transaction is required")
      );

      String property = "property";
      String Hanoi = "HÀ NỘI";
      String Hcm = "HỒ CHÍ MINH";
      JsonObject prices = new JsonObject(), areas = new JsonObject();

      List<MetropolisPriceArea> metropolisPriceAreas = realEstateManager.queryMetropolisPriceArea(transaction_type);
      List<String> properties = metropolisPriceAreas.stream()
          .map(MetropolisPriceArea::get_property_type)
          .distinct()
          .collect(Collectors.toList());

      JsonArray dimensions = new JsonArray();
      dimensions.add(property).add(Hanoi).add(Hcm);
      prices.put("dimensions", dimensions);
      areas.put("dimensions", dimensions);

      JsonArray price_sources = new JsonArray(), area_sources = new JsonArray();

      for (String property_type: properties) {
        JsonObject price_source = new JsonObject(), area_source = new JsonObject();
        for (MetropolisPriceArea metropolisPriceArea: metropolisPriceAreas) {
          price_source.put(property, property_type);
          area_source.put(property, property_type);
          if (property_type.equals(metropolisPriceArea.get_property_type())) {
            if (Hanoi.equals(metropolisPriceArea.get_province_city())) {
              price_source.put(Hanoi, metropolisPriceArea.get_average_price());
              area_source.put(Hanoi, metropolisPriceArea.get_total_area());
            } else {
              price_source.put(Hcm, metropolisPriceArea.get_average_price());
              area_source.put(Hcm, metropolisPriceArea.get_total_area());
            }
          }
        }
        price_sources.add(price_source);
        area_sources.add(area_source);
      }

      prices.put("source", price_sources);
      areas.put("source", area_sources);
      prices.put("unit", metropolisPriceAreas.get(0).get_price_unit());
      areas.put("unit", "m²");

      message.reply(
          new JsonObject().put("price", prices).put("area", areas)
      );
    } catch (Exception e) {
      LOG.error("Error getProvinceCityCount in RealEstateDashboardWorker");
      message.fail(500, e.getMessage());
    }
  }

  private void getProvinceCityCount(Message<JsonObject> message) {
    try {
      String transaction_type = Preconditions.checkNotNull(
          message.body().getString("transaction", "transaction is required")
      );

      String province = message.body().containsKey("province") ? message.body().getString("province") : null;

      JsonArray result = realEstateManager.queryProvinceCity(transaction_type, province);

      message.reply(
          new JsonObject().put("entities", result)
      );
    } catch (Exception e) {
      LOG.error("Error getProvinceCityCount in RealEstateDashboardWorker");
      message.fail(500, e.getMessage());
    }
  }

  private void getPropertyCount(Message<JsonObject> message) {
    try {
      String transaction_type = Preconditions.checkNotNull(
          message.body().getString("transaction", "transaction is required")
      );

      String province = message.body().containsKey("province") ? message.body().getString("province") : null;

      JsonArray result = realEstateManager.queryPropertyCount(transaction_type, province);

      message.reply(
          new JsonObject().put("entities", result)
      );
    } catch (Exception e) {
      LOG.error("Error getPropertyCount in RealEstateDashboardWorker");
      message.fail(500, e.getMessage());
    }
  }

  private void getTransactionCount(Message<JsonObject> message) {
    try {
      String province = message.body().containsKey("province") ? message.body().getString("province") : null;

      JsonArray result = realEstateManager.queryTransactionCount(province);

      message.reply(
          new JsonObject().put("entities", result)
      );
    } catch (Exception e) {
      LOG.error("Error getTransactionCount in RealEstateDashboardWorker");
      message.fail(500, e.getMessage());
    }
  }

  private void getAveragePricePerMonth(Message<JsonObject> message) {
    try {
      String transaction_type = Preconditions.checkNotNull(
          message.body().getString("transaction", "transaction is required")
      );

      String province = message.body().containsKey("province") ? message.body().getString("province") : null;

      JsonArray result = realEstateManager.queryAveragePricePerMonth(transaction_type, province);

      message.reply(
          new JsonObject().put("entities", result)
      );
    } catch (Exception e) {
      LOG.error("Error getAveragePricePerMonth in RealEstateDashboardWorker");
      message.fail(500, e.getMessage());
    }
  }

  private void testAvailability(Message<JsonObject> message) {
    message.reply(
        new JsonObject()
        .put("test", "service is available")
    );
  }
}
