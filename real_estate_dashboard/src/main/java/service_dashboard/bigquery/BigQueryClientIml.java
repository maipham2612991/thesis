package service_dashboard.bigquery;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.bigquery.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service_dashboard.realestate.RealEstateImpl;

import java.io.File;
import java.io.FileInputStream;
import java.util.UUID;

public class BigQueryClientIml implements BigQueryClientManager {
  private final BigQuery bigQuery;
  private static final String PROJECT = "real-estate-thesis-2021";
  private static final Logger LOG = LoggerFactory.getLogger(RealEstateImpl.class);

  public BigQueryClientIml() {
    try {
      GoogleCredentials credentials = GoogleCredentials.fromStream(
          new FileInputStream(
              new File("real-estate-thesis-2021-535501c44bee.json")
          )
      );
      this.bigQuery = BigQueryOptions.newBuilder().setCredentials(credentials).setProjectId(PROJECT).build().getService();
    } catch (Exception ex) {
      LOG.error("Error bigquery credentials in BigQueryClientIml");
      ex.printStackTrace();
      throw new RuntimeException(ex);
    }
  }

  @Override
  public TableResult query(String query) throws InterruptedException {
    JobId jobId = JobId.of(UUID.randomUUID().toString());

    QueryJobConfiguration queryConfig =
        QueryJobConfiguration.newBuilder(query)
            .setUseLegacySql(false)
            .setUseQueryCache(true)
            .setPriority(QueryJobConfiguration.Priority.INTERACTIVE)
            .build();

    Job queryJob = bigQuery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());

    while (!queryJob.isDone()) {
      queryJob = queryJob.waitFor();
    }

    return queryJob.getQueryResults();
  }
}
