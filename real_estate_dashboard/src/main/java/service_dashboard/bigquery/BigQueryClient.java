package service_dashboard.bigquery;

import com.google.cloud.bigquery.TableResult;

public interface BigQueryClient {

  public TableResult query(String query) throws InterruptedException;

}
