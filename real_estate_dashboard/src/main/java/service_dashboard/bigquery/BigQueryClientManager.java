package service_dashboard.bigquery;

import com.google.cloud.bigquery.TableResult;

public interface BigQueryClientManager {

  TableResult query(String query) throws InterruptedException;

}
