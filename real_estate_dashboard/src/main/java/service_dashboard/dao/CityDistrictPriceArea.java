package service_dashboard.dao;

public class CityDistrictPriceArea {
  public static String FIELD_CITY_DISTRICT = "city_district";
  public static String FIELD_AVERAGE_PRICE = "average_price";
  public static String FIELD_TOTAL_AREA = "total_area";
  public static String FIELD_PRICE_UNIT = "price_unit";
  public static String FIELD_TRANSACTION_TYPE = "transaction_type";
}
