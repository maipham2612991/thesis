package service_dashboard.dao;

import io.vertx.core.json.JsonObject;

public class ProvinceCityCount {
  public static String FIELD_PROVINCE_CITY = "province_city";
  public static String FIELD_CITY_DISTRICT = "city_district";
  public static String FIELD_PROVINCE_CITY_COUNT = "province_city_count";
  public static String FIELD_TRANSACTION_TYPE = "transaction_type";

  private String province_city;
  private int province_city_count;

  public ProvinceCityCount(String province_city, int province_city_count) {
    this.province_city = province_city;
    this.province_city_count = province_city_count;
  }

  public JsonObject toJsonObject() {
    return new JsonObject().put(FIELD_PROVINCE_CITY, province_city.toUpperCase())
                          .put(FIELD_PROVINCE_CITY_COUNT, province_city_count);
  }

  public String get_province_city() {
    return province_city;
  }

  public int get_province_city_count() {
    return province_city_count;
  }
}
