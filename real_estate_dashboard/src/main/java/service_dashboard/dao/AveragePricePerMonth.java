package service_dashboard.dao;

public class AveragePricePerMonth {
  public static String FIELD_TRANSACTION_TYPE = "transaction_type";
  public static String FIELD_PROPERTY_TYPE = "property_type";
  public static String FIELD_TIME_TO_MONTH = "time_to_month";
  public static String FIELD_AVERAGE_PRICE = "average_price";
  public static String FIELD_AVERAGE_TOTAL_PRICE = "average_total_price";
  public static String FIELD_PRICE_UNIT = "price_unit";
}
