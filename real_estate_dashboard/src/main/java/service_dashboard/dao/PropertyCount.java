package service_dashboard.dao;

public class PropertyCount {
  public static String FIELD_TRANSACTION_TYPE = "transaction_type";
  public static String FIELD_PROPERTY_TYPE = "property_type";
  public static String FIELD_PROPERTY_COUNT = "property_count";
}
