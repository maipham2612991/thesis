package service_dashboard.dao;

public class PriceArea {
  public static String FIELD_AVERAGE_PRICE = "average_price";
  public static String FIELD_PROVINCE_CITY = "province_city";
  public static String FIELD_TOTAL_AREA = "total_area";
}
