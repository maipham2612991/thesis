package service_dashboard.dao;

public class MetropolisPriceArea {
  public static String FIELD_PROVINCE_CITY = "province_city";
  public static String FIELD_PROPERTY_TYPE = "property_type";
  public static String FIELD_AVERAGE_PRICE = "average_price";
  public static String FIELD_PROVINCE_CITY_COUNT = "province_city_count";
  public static String FIELD_PRICE_UNIT = "price_unit";
  public static String FIELD_TRANSACTION_TYPE = "transaction_type";

  private String province_city;
  private String property_type;
  private Double average_price;
  private int province_city_count;
  private String price_unit;

  public MetropolisPriceArea(String province_city, String property_type, Double average_price, int province_city_count, String price_unit) {
    this.province_city = province_city;
    this.property_type = property_type;
    this.average_price = average_price;
    this.province_city_count = province_city_count;
    this.price_unit = price_unit;
  }

  public String get_province_city() {
    return province_city;
  }

  public String get_property_type() {
    return property_type;
  }

  public Double get_average_price() {
    return average_price;
  }

  public int get_total_area() {
    return province_city_count;
  }

  public String get_price_unit() {
    return price_unit;
  }
}
