package service_dashboard.dao;

public class Project {
  public static String FIELD_PROJECT_NAME = "project_name";
  public static String FIELD_POST_COUNT = "post_count";
  public static String FIELD_AVERAGE_PRICE = "average_price";
}
