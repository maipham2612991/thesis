package service_dashboard.realestate;

import io.vertx.core.json.JsonArray;
import service_dashboard.dao.MetropolisPriceArea;

import java.util.List;

public interface IRealEstateManager {
//  ApiFuture<List<TransactionDimension>> queryTransaction() throws InterruptedException;

  JsonArray queryAveragePricePerMonth(String transaction, String province);

  JsonArray queryTransactionCount(String province);

  JsonArray queryPropertyCount(String transaction, String province);

  JsonArray queryProvinceCity(String transaction_type, String province);

  List<MetropolisPriceArea> queryMetropolisPriceArea(String transaction_type);

  JsonArray queryProject(String transaction_type, String sort_type);

  JsonArray queryPriceArea(String transaction_type);

  JsonArray queryProjectProvince(String transaction, String sort_type, String province);

  JsonArray queryCityDistrictPriceArea(String transaction_type, String province);
}
