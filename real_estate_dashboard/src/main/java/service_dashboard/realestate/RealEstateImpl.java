package service_dashboard.realestate;

import com.google.cloud.bigquery.FieldValue;
import com.google.cloud.bigquery.FieldValueList;
import com.google.cloud.bigquery.TableResult;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.commons.text.WordUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service_dashboard.bigquery.BigQueryClientIml;
import service_dashboard.bigquery.BigQueryClientManager;
import service_dashboard.dao.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RealEstateImpl implements IRealEstateManager{
  private BigQueryClientManager bigQueryClient;
  private static final Logger LOG = LoggerFactory.getLogger(RealEstateImpl.class);
  private static final String PROJECT = "real-estate-thesis-2021";
  private static final String DATASET = "real_estate";

  public RealEstateImpl() {
    bigQueryClient = new BigQueryClientIml();
  }

  @Override
  public JsonArray queryAveragePricePerMonth(String transaction, String province) {
    JsonArray entities = new JsonArray();
    String query;

    if (province == null) {
      query = String.format(
          "SELECT \n" +
              " %s," +
              " %s," +
              " %s," +
              " %s," +
              " %s," +
              " %s \n" +
              "FROM `%s.AVERAGE_PRICE_PER_MONTH`" +
              "WHERE %s = \"%s\" " +
              "LIMIT 4",
          AveragePricePerMonth.FIELD_TRANSACTION_TYPE,
          AveragePricePerMonth.FIELD_PROPERTY_TYPE,
          AveragePricePerMonth.FIELD_TIME_TO_MONTH,
          AveragePricePerMonth.FIELD_AVERAGE_PRICE,
          AveragePricePerMonth.FIELD_AVERAGE_TOTAL_PRICE,
          AveragePricePerMonth.FIELD_PRICE_UNIT,
          PROJECT + "." + DATASET,
          AveragePricePerMonth.FIELD_TRANSACTION_TYPE,
          transaction
      );
    } else {
      query = String.format(
          "SELECT \n" +
              " %s," +
              " %s," +
              " %s," +
              " %s," +
              " %s," +
              " %s \n" +
              "FROM `%s.PROVINCE_AVERAGE_PRICE_PER_MONTH`" +
              "WHERE " +
              " %s = \"%s\" AND " +
              " addr_province_city = \"%s\" " +
              "LIMIT 4",
          AveragePricePerMonth.FIELD_TRANSACTION_TYPE,
          AveragePricePerMonth.FIELD_PROPERTY_TYPE,
          AveragePricePerMonth.FIELD_TIME_TO_MONTH,
          AveragePricePerMonth.FIELD_AVERAGE_PRICE,
          AveragePricePerMonth.FIELD_AVERAGE_TOTAL_PRICE,
          AveragePricePerMonth.FIELD_PRICE_UNIT,
          PROJECT + "." + DATASET,
          AveragePricePerMonth.FIELD_TRANSACTION_TYPE,
          transaction,
          province.toLowerCase()
      );
    }

    try {
      TableResult result = bigQueryClient.query(query);

      for (FieldValueList row : result.iterateAll()) {
        String transaction_type = row.get(AveragePricePerMonth.FIELD_TRANSACTION_TYPE).getStringValue();
        String property_type = row.get(AveragePricePerMonth.FIELD_PROPERTY_TYPE).getStringValue();

        List<FieldValue> months = row.get(AveragePricePerMonth.FIELD_TIME_TO_MONTH).getRepeatedValue();
        List<String> time_to_month = new ArrayList<>();
        for (FieldValue month: months) {
          time_to_month.add(month.getStringValue());
        }

        List<FieldValue> prices = row.get(AveragePricePerMonth.FIELD_AVERAGE_PRICE).getRepeatedValue();
        List<Double> average_price = new ArrayList<>();
        for (FieldValue price: prices) {
          average_price.add(price.getDoubleValue());
        }

        Double average_total_price = row.get(AveragePricePerMonth.FIELD_AVERAGE_TOTAL_PRICE).getDoubleValue();
        String price_unit = row.get(AveragePricePerMonth.FIELD_PRICE_UNIT).getStringValue();

        JsonObject record = new JsonObject()
            .put(AveragePricePerMonth.FIELD_TRANSACTION_TYPE, transaction_type)
            .put(AveragePricePerMonth.FIELD_PROPERTY_TYPE, property_type)
            .put(AveragePricePerMonth.FIELD_TIME_TO_MONTH, time_to_month)
            .put(AveragePricePerMonth.FIELD_AVERAGE_PRICE, average_price)
            .put(AveragePricePerMonth.FIELD_AVERAGE_TOTAL_PRICE, average_total_price)
            .put(AveragePricePerMonth.FIELD_PRICE_UNIT, price_unit);
        entities.add(record);
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return entities;
  }

  @Override
  public JsonArray queryTransactionCount(String province) {
    JsonArray entities = new JsonArray();
    String query;

    if (province == null) {
      query = String.format(
          "SELECT \n" +
              "  %s, \n" +
              "  sum(count) as %s \n" +
              "FROM `%s.TRANS_PROP_ADDR_FACT` \n" +
              "GROUP BY \n" +
              "  %s",
          TransactionCount.FIELD_TRANSACTION_TYPE,
          TransactionCount.FIELD_TRANSACTION_COUNT,
          PROJECT + "." + DATASET,
          TransactionCount.FIELD_TRANSACTION_TYPE
      );
    } else {
      query = String.format(
          "SELECT \n" +
              "  %s, \n" +
              "  sum(count) as %s \n" +
              "FROM `%s.ADDR_TRANS_PROP_PRICE` " +
              "WHERE \n" +
              "  province_city = \"%s\" \n" +
              "GROUP BY \n" +
              "  %s",
          TransactionCount.FIELD_TRANSACTION_TYPE,
          TransactionCount.FIELD_TRANSACTION_COUNT,
          PROJECT + "." + DATASET,
          province.toLowerCase(),
          TransactionCount.FIELD_TRANSACTION_TYPE
      );
    }


    try {
      TableResult result = bigQueryClient.query(query);

      for (FieldValueList row : result.iterateAll()) {
        String transaction_type = row.get(TransactionCount.FIELD_TRANSACTION_TYPE).getStringValue();
        int transaction_count = Integer.parseInt(row.get(TransactionCount.FIELD_TRANSACTION_COUNT).getStringValue());

        JsonObject item = new JsonObject()
            .put(TransactionCount.FIELD_TRANSACTION_TYPE, transaction_type.toUpperCase())
            .put(TransactionCount.FIELD_TRANSACTION_COUNT, transaction_count);

        entities.add(item);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return entities;
  }

  @Override
  public JsonArray queryPropertyCount(String transaction, String province) {
    JsonArray entities = new JsonArray();
    String query;

    if (province == null) {
      query = String.format(
          "SELECT \n" +
              "  %s,\n" +
              "  sum(count) as %s \n" +
              "FROM `%s.ADDR_TRANS_PROP_PRICE` \n" +
              "WHERE %s = \"%s\"\n" +
              "GROUP BY \n" +
              "  %s",
          PropertyCount.FIELD_PROPERTY_TYPE,
          PropertyCount.FIELD_PROPERTY_COUNT,
          PROJECT + "." + DATASET,
          PropertyCount.FIELD_TRANSACTION_TYPE,
          transaction,
          PropertyCount.FIELD_PROPERTY_TYPE
      );
    } else {
      query = String.format(
          "SELECT \n" +
              "  %s, \n" +
              "  sum(count) as %s \n" +
              "FROM `%s.ADDR_TRANS_PROP_PRICE` \n" +
              "WHERE " +
              "  %s = \"%s\" AND " +
              "  province_city = \"%s\" \n" +
              "GROUP BY \n" +
              "  %s",
          PropertyCount.FIELD_PROPERTY_TYPE,
          PropertyCount.FIELD_PROPERTY_COUNT,
          PROJECT + "." + DATASET,
          PropertyCount.FIELD_TRANSACTION_TYPE,
          transaction,
          province.toLowerCase(),
          PropertyCount.FIELD_PROPERTY_TYPE
      );
    }


    try {
      TableResult result = bigQueryClient.query(query);

      for (FieldValueList row : result.iterateAll()) {
        String property_type = row.get(PropertyCount.FIELD_PROPERTY_TYPE).getStringValue();
        int property_count = Integer.parseInt(row.get(PropertyCount.FIELD_PROPERTY_COUNT).getStringValue());

        JsonObject item = new JsonObject()
            .put(PropertyCount.FIELD_PROPERTY_TYPE, property_type)
            .put(PropertyCount.FIELD_PROPERTY_COUNT, property_count);

        entities.add(item);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return entities;
  }

  @Override
  public JsonArray queryProvinceCity(String transaction, String province) {
    JsonArray entities = new JsonArray();
    List<ProvinceCityCount> provinceCityCounts = new ArrayList<>();
    String query;
    String other_cities;
    String ADDR;

    if (province == null) {
      query = String.format(
          "SELECT \n" +
              "  addr.addr_province_city as %s, \n" +
              "  sum(fact.count) as %s \n" +
              "FROM \n" +
              "  `%s.TRANS_PROP_ADDR_FACT` as fact, \n" +
              "  `%s.ADDR_DIMENSION` as addr \n" +
              "WHERE \n" +
              "  fact.addr_id = addr.id AND \n" +
              "  %s = \"%s\"\n" +
              "GROUP BY \n" +
              "  %s",
          ProvinceCityCount.FIELD_PROVINCE_CITY,
          ProvinceCityCount.FIELD_PROVINCE_CITY_COUNT,
          PROJECT + "." + DATASET,
          PROJECT + "." + DATASET,
          ProvinceCityCount.FIELD_TRANSACTION_TYPE,
          transaction,
          ProvinceCityCount.FIELD_PROVINCE_CITY
      );
      other_cities = "Những tỉnh thành khác";
      ADDR = ProvinceCityCount.FIELD_PROVINCE_CITY;
    } else {
      query = String.format(
          "SELECT \n" +
              "  %s, \n" +
              "  SUM(count) as %s\n" +
              "FROM \n" +
              "  `%s.ADDR_TRANS_PROP_PRICE`\n" +
              "WHERE\n" +
              "  %s = \"%s\" AND \n" +
              "  %s = \"%s\" \n" +
              "GROUP BY\n" +
              "  %s ",
          ProvinceCityCount.FIELD_CITY_DISTRICT,
          ProvinceCityCount.FIELD_PROVINCE_CITY_COUNT,
          PROJECT + "." + DATASET,
          ProvinceCityCount.FIELD_PROVINCE_CITY,
          province.toLowerCase(),
          ProvinceCityCount.FIELD_TRANSACTION_TYPE,
          transaction,
          ProvinceCityCount.FIELD_CITY_DISTRICT
      );
      other_cities = "Những quận huyện khác";
      ADDR = ProvinceCityCount.FIELD_CITY_DISTRICT;
    }

    int total = 0;
    int total_other_cities = 0;

    try {
      TableResult result = bigQueryClient.query(query);

      for (FieldValueList row : result.iterateAll()) {
        String province_city = row.get(ADDR).getStringValue();
        int province_city_count = Integer.parseInt(row.get(ProvinceCityCount.FIELD_PROVINCE_CITY_COUNT).getStringValue());

        ProvinceCityCount provinceCityCount = new ProvinceCityCount(province_city, province_city_count);

//        if (max < province_city_count) {
//          max = province_city_count;
//        }

        total += province_city_count;

        provinceCityCounts.add(provinceCityCount);
      }

      int threshold =  total/25;

      for (ProvinceCityCount provinceCityCount: provinceCityCounts) {
        int province_city_count = provinceCityCount.get_province_city_count();

        if (provinceCityCount.get_province_city_count() >= threshold) {
          entities.add(provinceCityCount.toJsonObject());
        } else {
          total_other_cities += province_city_count;
        }
      }

      JsonObject item = new JsonObject()
          .put(ProvinceCityCount.FIELD_PROVINCE_CITY, other_cities)
          .put(ProvinceCityCount.FIELD_PROVINCE_CITY_COUNT, total_other_cities);
      entities.add(item);

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return entities;
  }

  @Override
  public List<MetropolisPriceArea> queryMetropolisPriceArea(String transaction) {
    List<MetropolisPriceArea> entities = new ArrayList<>();

    String query = String.format(
        "SELECT \n" +
        "  %s,\n" +
        "  %s,\n" +
        "  %s,\n" +
        "  %s,\n" +
        "  %s\n" +
        "FROM `%s.AVERAGE_PRICE_TOTAL_AREA_METROPOLIS`\n" +
        "WHERE %s = \"%s\"",
        MetropolisPriceArea.FIELD_PROVINCE_CITY,
        MetropolisPriceArea.FIELD_PROPERTY_TYPE,
        MetropolisPriceArea.FIELD_AVERAGE_PRICE,
        MetropolisPriceArea.FIELD_PROVINCE_CITY_COUNT,
        MetropolisPriceArea.FIELD_PRICE_UNIT,
        PROJECT + "." + DATASET,
        MetropolisPriceArea.FIELD_TRANSACTION_TYPE,
        transaction
    );

    try {
      TableResult result = bigQueryClient.query(query);

      for (FieldValueList row : result.iterateAll()) {
        MetropolisPriceArea item = new MetropolisPriceArea(
            row.get(MetropolisPriceArea.FIELD_PROVINCE_CITY).getStringValue().toUpperCase(),
            row.get(MetropolisPriceArea.FIELD_PROPERTY_TYPE).getStringValue(),
            row.get(MetropolisPriceArea.FIELD_AVERAGE_PRICE).getDoubleValue(),
            Integer.parseInt(row.get(MetropolisPriceArea.FIELD_PROVINCE_CITY_COUNT).getStringValue()),
            row.get(MetropolisPriceArea.FIELD_PRICE_UNIT).getStringValue()
        );

        entities.add(item);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return entities;
  }

  @Override
  public JsonArray queryProject(String transaction, String sort_type) {
    JsonArray entities = new JsonArray();

    String query;

    if (sort_type.equals("postCount")) {
      query = String.format(
          "SELECT \n" +
              "  %s, \n" +
              "  SUM(count) as %s, \n" +
              "  AVG(average_price) as %s \n" +
              "FROM \n" +
              "  `%s.TRANS_PROJECT_ADDR_FACT` \n" +
              "WHERE \n" +
              "  transaction_type = \"%s\" \n" +
              "GROUP BY \n" +
              "  %s \n" +
              "ORDER BY %s desc \n" +
              "LIMIT 20",
          Project.FIELD_PROJECT_NAME,
          Project.FIELD_POST_COUNT,
          Project.FIELD_AVERAGE_PRICE,
          PROJECT + "." + DATASET,
          transaction,
          Project.FIELD_PROJECT_NAME,
          Project.FIELD_POST_COUNT
      );
    } else {
      query = String.format(
          "SELECT \n" +
              "  %s, \n" +
              "  SUM(count) as %s, \n" +
              "  AVG(average_price) as %s \n" +
              "FROM \n" +
              "  `%s.TRANS_PROJECT_ADDR_FACT` \n" +
              "WHERE \n" +
              "  transaction_type = \"%s\" \n" +
              "GROUP BY \n" +
              "  %s \n" +
              "ORDER BY %s desc \n" +
              "LIMIT 20",
          Project.FIELD_PROJECT_NAME,
          Project.FIELD_POST_COUNT,
          Project.FIELD_AVERAGE_PRICE,
          PROJECT + "." + DATASET,
          transaction,
          Project.FIELD_PROJECT_NAME,
          Project.FIELD_AVERAGE_PRICE
      );
    }

    try {
      TableResult result = bigQueryClient.query(query);

      for (FieldValueList row : result.iterateAll()) {
        String project_name = row.get(Project.FIELD_PROJECT_NAME).getStringValue();
        int post_count = Integer.parseInt(row.get(Project.FIELD_POST_COUNT).getStringValue());
        double average_price = Double.parseDouble(row.get(Project.FIELD_AVERAGE_PRICE).getStringValue());
        JsonObject item = new JsonObject()
            .put(Project.FIELD_PROJECT_NAME, project_name)
            .put(Project.FIELD_POST_COUNT, post_count)
            .put(Project.FIELD_AVERAGE_PRICE, average_price);

        entities.add(item);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return entities;
  }

  @Override
  public JsonArray queryPriceArea(String transaction) {
    JsonArray entities = new JsonArray();

    String query = String.format(
        "SELECT \n" +
        "  addr.addr_province_city as %s,\n" +
        "  AVG(average_price) as %s, \n" +
        "  SUM(total_area) as %s " +
        "FROM\n" +
        "  `%s.TRANS_PROP_ADDR_FACT` as fact,\n" +
        "  `%s.ADDR_DIMENSION` as addr\n" +
        "WHERE \n" +
        "  transaction_type = \"%s\" AND\n" +
        "  fact.addr_id = addr.id  \n" +
        "GROUP BY \n" +
        "  %s",
        PriceArea.FIELD_PROVINCE_CITY,
        PriceArea.FIELD_AVERAGE_PRICE,
        PriceArea.FIELD_TOTAL_AREA,
        PROJECT + "." + DATASET,
        PROJECT + "." + DATASET,
        transaction,
        PriceArea.FIELD_PROVINCE_CITY
    );

    try {
      TableResult result = bigQueryClient.query(query);

      for (FieldValueList row : result.iterateAll()) {
        String province_city = WordUtils.capitalize(row.get(PriceArea.FIELD_PROVINCE_CITY).getStringValue());
        double average_price = Double.parseDouble(row.get(PriceArea.FIELD_AVERAGE_PRICE).getStringValue());
        double total_area = Double.parseDouble(row.get(PriceArea.FIELD_TOTAL_AREA).getStringValue());

        JsonObject record = new JsonObject()
            .put(PriceArea.FIELD_PROVINCE_CITY, province_city)
            .put(PriceArea.FIELD_AVERAGE_PRICE, average_price)
            .put(PriceArea.FIELD_TOTAL_AREA, total_area);
        entities.add(record);
      }

    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return entities;
  }

  @Override
  public JsonArray queryProjectProvince(String transaction, String sort_type, String province) {
    JsonArray entities = new JsonArray();

    String query;

    if (sort_type.equals("postCount")) {
      query = String.format(
          "SELECT \n" +
              "  %s,\n" +
              "  SUM(count) as %s,\n" +
              "  AVG(average_price) as %s\n" +
              "FROM \n" +
              "  `%s.TRANS_PROJECT_ADDR_FACT` as fact,\n" +
              "  `%s.ADDR_DIMENSION` as addr\n" +
              "WHERE\n" +
              "  fact.addr_id = addr.id AND\n" +
              "  transaction_type = \"%s\" AND\n" +
              "  addr.addr_province_city = \"%s\"\n" +
              "GROUP BY\n" +
              "  %s\n" +
              "ORDER BY %s DESC\n" +
              "LIMIT 20",
          Project.FIELD_PROJECT_NAME,
          Project.FIELD_POST_COUNT,
          Project.FIELD_AVERAGE_PRICE,
          PROJECT + "." + DATASET,
          PROJECT + "." + DATASET,
          transaction,
          province.toLowerCase(),
          Project.FIELD_PROJECT_NAME,
          Project.FIELD_POST_COUNT
      );
    } else {
      query = String.format(
          "SELECT \n" +
              "  %s,\n" +
              "  SUM(count) as %s,\n" +
              "  AVG(average_price) as %s\n" +
              "FROM \n" +
              "  `%s.TRANS_PROJECT_ADDR_FACT` as fact,\n" +
              "  `%s.ADDR_DIMENSION` as addr\n" +
              "WHERE\n" +
              "  fact.addr_id = addr.id AND\n" +
              "  transaction_type = \"%s\" AND\n" +
              "  addr.addr_province_city = \"%s\"\n" +
              "GROUP BY\n" +
              "  %s\n" +
              "ORDER BY %s DESC\n" +
              "LIMIT 20",
          Project.FIELD_PROJECT_NAME,
          Project.FIELD_POST_COUNT,
          Project.FIELD_AVERAGE_PRICE,
          PROJECT + "." + DATASET,
          PROJECT + "." + DATASET,
          transaction,
          province.toLowerCase(),
          Project.FIELD_PROJECT_NAME,
          Project.FIELD_AVERAGE_PRICE
      );
    }

    try {
      TableResult result = bigQueryClient.query(query);

      for (FieldValueList row : result.iterateAll()) {
        String project_name = row.get(Project.FIELD_PROJECT_NAME).getStringValue();
        int post_count = Integer.parseInt(row.get(Project.FIELD_POST_COUNT).getStringValue());
        double average_price = Double.parseDouble(row.get(Project.FIELD_AVERAGE_PRICE).getStringValue());
        JsonObject item = new JsonObject()
            .put(Project.FIELD_PROJECT_NAME, project_name)
            .put(Project.FIELD_POST_COUNT, post_count)
            .put(Project.FIELD_AVERAGE_PRICE, average_price);

        entities.add(item);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return entities;
  }

  @Override
  public JsonArray queryCityDistrictPriceArea(String transaction_type, String province) {
    JsonArray entities = new JsonArray();

    String city = province.toLowerCase();
    String limit_number;
    if (city.equals("hồ chí minh") || city.equals("hà nội")) {
      limit_number = "10";
    } else {
      limit_number = "5";
    }

    String query = String.format(
          "SELECT \n" +
          "  addr.addr_city_district as %s,\n" +
          "  AVG(average_price) as %s,\n" +
          "  SUM(fact.count) as district_count,\n" +
          "  SUM(fact.total_area) as %s\n" +
          "FROM\n" +
          "  `%s.TRANS_PROP_ADDR_FACT` as fact,\n" +
          "  `%s.ADDR_DIMENSION` as addr\n" +
          "WHERE \n" +
          "  transaction_type = \"%s\" AND\n" +
          "  fact.addr_id = addr.id AND\n" +
          "  addr.addr_province_city = \"%s\"\n" +
          "GROUP BY \n" +
          "  city_district\n" +
          "ORDER BY district_count DESC LIMIT %s",
        CityDistrictPriceArea.FIELD_CITY_DISTRICT,
        CityDistrictPriceArea.FIELD_AVERAGE_PRICE,
        CityDistrictPriceArea.FIELD_TOTAL_AREA,
        PROJECT + "." + DATASET,
        PROJECT + "." + DATASET,
        transaction_type,
        city,
        limit_number
      );

    try {
      TableResult result = bigQueryClient.query(query);

      for (FieldValueList row : result.iterateAll()) {
        String district = row.get(CityDistrictPriceArea.FIELD_CITY_DISTRICT).getStringValue();
        double average_price = Double.parseDouble(row.get(CityDistrictPriceArea.FIELD_AVERAGE_PRICE).getStringValue());
        double total_area = Double.parseDouble(row.get(CityDistrictPriceArea.FIELD_TOTAL_AREA).getStringValue());

        JsonObject item = new JsonObject()
            .put(CityDistrictPriceArea.FIELD_CITY_DISTRICT, district.toUpperCase())
            .put(CityDistrictPriceArea.FIELD_AVERAGE_PRICE, average_price)
            .put(CityDistrictPriceArea.FIELD_TOTAL_AREA, total_area);

        entities.add(item);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    return entities;
  }
}
