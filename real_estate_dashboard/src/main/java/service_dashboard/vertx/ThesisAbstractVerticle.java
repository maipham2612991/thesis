package service_dashboard.vertx;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ThesisAbstractVerticle extends AbstractVerticle {
  private static final Logger LOG = LoggerFactory.getLogger(ThesisAbstractVerticle.class);

  protected <T,O> void delegateToWorker(
      RoutingContext context,
      Class<T> workerClass,
      String workerApiName,
      O inputObject) {
    LOG.info("Sending to worker: {}/{} with input *{}", workerClass.getSimpleName(), workerApiName, inputObject);

    vertx.eventBus().<JsonObject>request(
      workerClass.getSimpleName() + "/" + workerApiName,
        inputObject,
        messageAsyncResult -> {
          if (messageAsyncResult.succeeded()) {
            context.response()
                .putHeader("Content-Type", "application/json")
                .setStatusCode(200)
                .end(messageAsyncResult.result().body().encode());
          } else {
            final HttpServerRequest request = context.request();
            LOG.error("Error handling request {}. statusCode[{}]. user-agent[{}]. app_version[{}]. "
                    + "user_phone[{}]. Params [{}]. Body [{}]",
                request.path(),
                context.statusCode(),
                request.headers().get("user-agent"),
                request.headers().get("app_version"),
                request.headers().get("user_phone"),
                request.params(),
                context.getBodyAsString(),
                messageAsyncResult.cause());
            JsonObject errorMess = new JsonObject()
                .put("error_message",
                    String.format("Error sending to worker %s/%s", workerClass.getSimpleName(), workerApiName));
            context.response()
                .setStatusCode(500)
                .putHeader("Content-Type", "application/json")
                .end(errorMess.encode());
          }
        }
    );
  }
}
