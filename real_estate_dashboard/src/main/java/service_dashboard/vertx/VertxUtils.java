package service_dashboard.vertx;

import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.CorsHandler;

import java.util.concurrent.TimeUnit;

public class VertxUtils {

  public static void configVertxToAllowCORS(Router router) {
    router.route().handler(BodyHandler.create());
    router.route().handler(CookieHandler.create());
    router.route().handler(CorsHandler.create(".*")
        .allowCredentials(true)
        .allowedMethod(HttpMethod.GET)
        .allowedMethod(HttpMethod.POST)
        .allowedMethod(HttpMethod.PUT)
        .allowedMethod(HttpMethod.DELETE)
        .allowedMethod(HttpMethod.OPTIONS)
        .allowedHeader("Access-Control-Request-Method")
        .allowedHeader("Access-Control-Allow-Credentials")
        .allowedHeader("Access-Control-Allow-Origin")
        .allowedHeader("Access-Control-Allow-Headers")
        .allowedHeader("Authorization")
        .allowedHeader("Content-Type"));
  }

  public static VertxOptions configVertxOption() {
    return new VertxOptions()
        .setMaxEventLoopExecuteTime(10)
        .setMaxEventLoopExecuteTimeUnit(TimeUnit.SECONDS)
        .setMaxWorkerExecuteTime(10)
        .setMaxWorkerExecuteTimeUnit(TimeUnit.SECONDS)
        .setBlockedThreadCheckInterval(5)
        .setBlockedThreadCheckIntervalUnit(TimeUnit.SECONDS);
  }

  public static HttpServerOptions getHttpServerOptions() {
    return new HttpServerOptions()
        .setMaxHeaderSize(8192 * 2)
        .setCompressionSupported(true)
        .setMaxInitialLineLength(4096 * 4);
  }

}
