## Presentation
Detail presentation can be seen in
https://docs.google.com/presentation/d/16lI5UH5ox9l2Im8oeq3v4P8yHKSrMBV3dCkd-ydDixU/edit?usp=sharing

## Microservice application for real estate
Three sevices including:
- Real estate dashboard: display trending, market share, etc..
- Price prediction: predict price of house, land given information about area, bathroom, bedroom, etc..
- Active real estate: houses/land on sale 
- User interface show result of all services

Architecture is shown in 'Untitled Diagram.png'
## Contributors:
- Khang Nguyen
- Mai Pham
- Nguyen Nguyen

## Data sources
Multiple real estate websites in Viet Nam
## Approaches
- Crawling real estate data from multiple websites (scheduled crawler daily)
- Call an external API to extract data more details from them
- Integrate and normalize all data into databases
- ETL in other ways to suitable for each service: ETL to BigQuery with Star Schema, ETL to relational database with 3NF for transactional service
- Implement all services and display all result in a web application
- We employ GCP for all infrastructure

Due to out of budget, we halted all services and the result can be seen in the slides


